(define-module (glicid packages admin)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages hurd)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages groff)
  #:use-module (gnu packages openldap)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages cyrus-sasl)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
)

(define-public sudo-with-ldap
  (package
    (name "sudo-with-ldap")
    (version "1.9.8p2")
    (source (origin
      (method url-fetch)
      (uri
        (list
          (string-append "https://www.sudo.ws/sudo/dist/sudo-" version ".tar.gz")
          (string-append "ftp://ftp.sudo.ws/pub/sudo/OLD/sudo-" version ".tar.gz")
        )
      )
      (sha256 (base32 "0b8gd15l2g22w4fhhz0gzmq5c8370klanmy2c1p3px6yly6qnfwy"))
      (modules '((guix build utils)))
      (snippet
        '(begin
          (delete-file-recursively "lib/zlib")
        )
      )
    ))
    (build-system gnu-build-system)
    (outputs (list "out"))
    (arguments
      `(#:configure-flags
        (list
          (string-append "--docdir=" (assoc-ref %outputs "out") "/share/doc/" ,name "-" ,version)
          "--with-logpath=/var/log/sudo.log"
          "--with-rundir=/var/run/sudo" ; must be cleaned up at boot time
          "--with-vardir=/var/db/sudo"
          "--with-iologdir=/var/log/sudo-io"
          "--enable-sasl"
          "--with-ldap"
          "--enable-openssl"
          "--with-nsswitch"
          "--with-pam-login"
          ;; 'visudo.c' expects _PATH_MV to be defined, but glibc doesn't provide it.
          (string-append "CPPFLAGS=-D_PATH_MV='\"" (assoc-ref %build-inputs "coreutils") "/bin/mv\"'")
        )
        ;; Avoid non-determinism; see <http://bugs.gnu.org/21918>.
        #:parallel-build? #f
        #:phases
        (modify-phases %standard-phases
          (add-before 'configure 'pre-configure
            (lambda _
              (substitute* "src/sudo_usage.h.in"
                ;; Do not capture 'configure' arguments since we would
                ;; unduly retain references, and also because the
                ;; CPPFLAGS above would close the string literal prematurely.
                (("@CONFIGURE_ARGS@") "\"\"")
              )
              (substitute* (find-files "." "Makefile\\.in")
                ;; Allow installation as non-root.
                (("-o [[:graph:]]+ -g [[:graph:]]+") "")
                ;; Don't try to create /etc/sudoers.
                (("^install: (.*)install-sudoers(.*)" _ before after) (string-append "install: " before after "\n"))
                ;; Don't try to create /run/sudo.
                (("\\$\\(DESTDIR\\)\\$\\(rundir\\)") "$(TMPDIR)/dummy")
                ;; Install example sudo{,_logsrvd}.conf to the right place.
                (("\\$\\(DESTDIR\\)\\$\\(sysconfdir\\)") "$(DESTDIR)/$(docdir)/examples")
                ;; Don't try to create /var/db/sudo.
                (("\\$\\(DESTDIR\\)\\$\\(vardir\\)") "$(TMPDIR)/dummy")
              )
              ;; ‘Checking existing [/etc/]sudoers file for syntax errors’ is
              ;; not the task of the build system, and fails.
              (substitute* "plugins/sudoers/Makefile.in"
                (("^pre-install:" match) (string-append match "\ndisabled-" match))
              )
            )
          )
        )
        ;; XXX: The 'testsudoers' test series expects user 'root' to exist, but
        ;; the chroot's /etc/passwd doesn't have it.  Turn off the tests.
        #:tests? #f
      )
    )
    (native-inputs
      (list groff)
    )
    (inputs
      `(("coreutils" ,coreutils)
      ,@(if (hurd-target?)
        '()
        `(("linux-pam" ,linux-pam))
      )
      ("zlib" ,zlib)
      ("openldap" ,openldap)
      ("openssl" ,openssl)
      ("cyrus-sasl" ,cyrus-sasl)
      )
    )
    (home-page "https://www.sudo.ws/")
    (synopsis "Run commands as root")
    (description
      "Sudo (su \"do\") allows a system administrator to delegate authority to
      give certain users (or groups of users) the ability to run some (or all)
      commands as root or another user while providing an audit trail of the
      commands and their arguments."
    )
    ;; See <http://www.sudo.ws/sudo/license.html>.
    (license license:x11)
  )
)
