(define-module (glicid packages algebra)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages commencement)
)

(define-public fftw-openmpi-with-fortran
  (package
    (inherit fftw-openmpi) 
    (name "fftw-openmpi-with-fortran")
    (version "3.3.10")
    (source (origin
      (method url-fetch)
      (uri (string-append "https://www.fftw.org/fftw-" version ".tar.gz"))
      (sha256 (base32 "0rv4w90b65b2kvjpj8g9bdkl4xqc42q20f5bzpxdrkajk1a35jan"))
    ))
    (inputs `(
      ("gfortran-toolchain", gfortran-toolchain)
      ,@(package-inputs fftw-openmpi)
    ))
  )
)
