(define-module (glicid packages benchmark)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module ((gnu packages benchmark) #:prefix gnu:)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages mpi)
  #:use-module (glicid packages mpi)
  #:use-module (glicid packages gcc)
  #:use-module (glicid utils)
)

(define-public intel-mpi-benchmarks/openmpi-2021.3
  (package
    (inherit gnu:intel-mpi-benchmarks/openmpi)
    (name "intel-mpi-benchmarks-upstream")
    (version "2021.3")
    (source 
      (origin
        (inherit (package-source gnu:intel-mpi-benchmarks/openmpi))
        (method git-fetch)
        (uri (git-reference
          (url "https://github.com/intel/mpi-benchmarks")
          (commit (string-append "IMB-v" version))
        ))
        (file-name (git-file-name name version))
        (sha256 (base32 "04kczch2hlfzbgk929vcxx480kc6raba8rbz246j7d26k1z1zh8h"))
      )
    )
  )
)

(define-public intel-mpi-benchmarks/openmpi-2021.3-libfabric-rdma
  (transform-package (
    (instead-of "openmpi" openmpi-glicid-libfabric-rdma)
    intel-mpi-benchmarks/openmpi-2021.3
  ) "fab+rdma")
)

(define intel-mpi-benchmark-transform-gcc-11
  (gcc11-instead-of-gcc intel-mpi-benchmarks/openmpi-2021.3-libfabric-rdma)
)

(define-public intel-mpi-benchmarks/openmpi-2021.3-gcc-11
  (package 
    (inherit intel-mpi-benchmark-transform-gcc-11)
    (version (string-append (package-version intel-mpi-benchmark-transform-gcc-11) "-gcc-11" ))
  )
)

(define-public intel-mpi-benchmarks/openmpi-2021.3-all-ccipl
  (transform-package (
    (instead-of "openmpi-upstream-glicid-libfabric-rdma" openmpi-glicid-libfabric-rdma-ucx-ccipl)
    intel-mpi-benchmarks/openmpi-2021.3-libfabric-rdma
  ) "ccipl")
)


(define-public stream-benchmarks
  (package
    (name "stream-benchmarks")
    (version "5.10-jh")
    (source (origin
      (method git-fetch)
      (uri
        (git-reference
          (url "https://github.com/jeffhammond/STREAM.git")
          (commit "HEAD")
        )
      )
      (sha256 (base32 "1b5ka2h6rhp2103app6p0vq29y7qixcli9w874hb33y05ggjin8m"))
      (file-name (string-append name "-" version "-checkout"))
    ))
    (build-system gnu-build-system)
    (arguments
      `(#:phases
         (modify-phases %standard-phases
           (delete 'configure) ; no configure 
           (delete 'check) ; no check
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
             ;; Le Makefile du paquet ne fournit pas de règle « install »
             ;; alors on le fait nous-mêmes.
               (let 
                 ((bin (string-append (assoc-ref outputs "out") "/bin")))
                 (install-file "stream_c.exe" bin)
                 (install-file "stream_f.exe" bin)
                 #t
               )
             )
           )
         )
       )
     )
     (inputs `(
       ("gfortran-toolchain" ,gfortran-toolchain)
     ))
     (synopsis "STREAM  benchmark")
     (description "STREAM benchmark")
     (home-page "https://")
     (license "")
  )
)

;;defined by gricad / PA Boutier

(define-public osu-benchmarks
  (package
    (name "osu-benchmarks")
    (version "5.8")
    (source (origin
      (method url-fetch)
      (uri (string-append "https://mvapich.cse.ohio-state.edu/download/mvapich/osu-micro-benchmarks-" version ".tgz"))
      (sha256 (base32 "19a4wg0msipibkxsi8i0c34d07512yfaj2k37dxg5541ysdw690f"))
    ))
    (build-system gnu-build-system)
    (arguments
      `(#:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'setenv
             (lambda _
               (setenv "CC" (which "mpicc"))
               (setenv "CXX" (which "mpic++"))
               #t
             )
           )
         )
      )
    )
    (propagated-inputs `(
      ("openmpi" ,openmpi)
    ))
    (synopsis "OSU micro benchmarks")
    (description "OSU micro benchmark for mpi")
    (home-page "https://mvapich.cse.ohio-state.edu/benchmarks/")
    (license license:gpl2+)
  )
)
