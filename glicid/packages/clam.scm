(define-module (glicid packages clam)

#:use-module (guix packages)
#:use-module (guix download)
#:use-module (gnu packages linux)
#:use-module (gnu packages virtualization)
#:use-module (gnu packages storage)
#:use-module (gnu packages gcc)
#:use-module (glicid packages glicid)
#:use-module (guix build-system python)
#:use-module ((guix licenses) #:prefix license:)
#:use-module (guix git-download)
#:use-module (gnu packages python)
#:use-module (guix build-system python)
#:use-module (gnu packages python-web)
#:use-module (gnu packages python-xyz)
#:use-module (gnu packages python-crypto)
#:use-module (gnu packages check)
#:use-module (guix build-system copy)
#:use-module (gnu packages django)
#:use-module (gnu packages time)
#:use-module (gnu packages databases)
)

(define-public python-pytest-runner-4.5.1
  (package
    (name "python-pytest-runner")
    (version "4.5.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "pytest-runner" version))
       (sha256
        (base32
         "1vzilbayx5mznsdm1r258m3616374p6kvhsbj4j6238j9djkvjyi"))))
    (build-system python-build-system)
    (arguments
     '(;; FIXME: The test suite requires 'python-flake8' and 'python-black',
       ;; but that introduces a circular dependency.
       #:tests? #f
       #:phases (modify-phases %standard-phases
                  (replace 'check
                    (lambda* (#:key tests? #:allow-other-keys)
                      (if tests?
                          (invoke "pytest" "-vv")
                          (format #t "test suite not run~%"))
                      #t)))))
    (native-inputs
     `(("python-setuptools-scm" ,python-setuptools-scm)))
    (home-page "https://github.com/pytest-dev/pytest-runner")
    (synopsis "Invoke py.test as a distutils command")
    (description
     "This package provides a @command{pytest-runner} command that
@file{setup.py} files can use to run tests.")
    (license license:expat)))

(define-public python-keycloak-client-0.2.3 
(package
  (name "python-keycloak-client")
  (version "0.2.3")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "python-keycloak-client" version))
      (sha256
        (base32
          "0gw6wj1rlacfpgbp4b2wyp68ccrh713438qjpv11x6bnlcnf52x3"))))
  (build-system python-build-system) ;; FIXME: Tests require specific "pytest", 
  ;; we don't have it for the moment.
  (arguments
     '(#:tests? #f))


  (propagated-inputs
    `(("python-jose" ,python-jose)
    ("python-pytest-runner-4.5.1", python-pytest-runner-4.5.1)
      ("python-requests" ,python-requests)))
  (native-inputs
    `(("python-bumpversion" ,python-bumpversion)
      ("python-pytest-runner-4.5.1", python-pytest-runner-4.5.1)
      ("python-twine" ,python-twine)))
  (home-page
    "https://github.com/Peter-Slump/python-keycloak-client")
  (synopsis "Install Python Keycloak client.")
  (description "Install Python Keycloak client.")
  (license license:expat))
  
)


(define-public glicid-django-keycloak
 (package
    (name "glicid-django-keycloak")
    (version "v0.1.2-bird")
    (source 
     (origin
      (method git-fetch)
      (uri 
       (git-reference
        (url "https://oauth2:glpat-RsX2GjsD2WrzXubiJeou@gitlab.univ-nantes.fr/CCIPL/stages/2021/django-keycloak.git")        (commit version)
       )
      )
      
      (file-name (git-file-name name version))
      (sha256
       (base32
        "0l380q6z504nndjl15cb5lkghpmm6f3rs4bgwy8sx5s12xz2sbck"
       )
      )
     )
    ) 

    (build-system python-build-system)
    ;; XXX: The git repository has no tags, and the PyPI releases do not
    ;; contain tests.
    (arguments '(#:tests? #f))
    (native-inputs `(("python-keycloak-client" , python-keycloak-client-0.2.3)
		     ("python-ecdsa", python-ecdsa)
    ))
    (home-page "https://blabla")
    (synopsis "blabla")
    (description
     "This package provides a single-file minifier for CSS, HTML, and JavaScript.")
    ;; XXX: The README just says "GNU GPL and GNU LGPL and MIT".  From
    ;; <https://github.com/juancarlospaco/css-html-js-minify/issues/9> it
    ;; looks like the user can choose a license.
    (license (list license:gpl3+ license:lgpl3+ license:expat))

 )
)


(define-public python-django-autocomplete-light
(package
  (name "python-django-autocomplete-light")
  (version "3.8.2")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "django-autocomplete-light" version))
      (sha256
        (base32
          "145snirk0a7bgcgwisn5fzmb0w4m88zf6r55m2biz3wsnmqymw15"))))
  (build-system python-build-system)
  (arguments '(#:tests? #f)) ; pb with django ?
  (native-inputs `(("python-django", python-django)))
  (propagated-inputs `(("python-six" ,python-six)))
  (home-page
    "http://django-autocomplete-light.rtfd.org")
  (synopsis "Fresh autocompletes for Django")
  (description "Fresh autocompletes for Django")
  (license license:expat))
)

(define-public python-django-icons

(package
  (name "python-django-icons")
  (version "4.0.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "django-icons" version))
      (sha256
        (base32
          "06r0d68qknqa7p44nss6wzdcawakyr7ildgyl6mp5md9xb8klbah"))))
  (build-system python-build-system)
   (arguments '(#:tests? #f)) ; django pb
  (propagated-inputs
    `(("python-django" ,python-django)
      ("python-importlib-metadata"
       ,python-importlib-metadata)))
  (home-page
    "https://github.com/zostera/django-icons")
  (synopsis "Icons for Django")
  (description "Icons for Django")
  (license #f))

)

(define-public python-django-admin-row-actions
(package
  (name "python-django-admin-row-actions")
  (version "0.0.5")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "django-admin-row-actions" version))
      (sha256
        (base32
          "0rz71a54lj73wfi56flfh5m5fi7ah1cl8fph1h8y386s0am8plpz"))))
  (build-system python-build-system)

  (native-inputs `(("python-django", python-django)))
  (arguments '(#:tests? #f)) ; iteration problem

  (propagated-inputs `(("python-six" ,python-six)))
  (home-page
    "https://github.com/DjangoAdminHackers/django-admin-row-actions")
  (synopsis "django admin row actions")
  (description "django admin row actions")
  (license #f))
)

(define-public python-django-bootstrap4
(package
  (name "python-django-bootstrap4")
  (version "3.0.1")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "django-bootstrap4" version))
      (sha256
        (base32
          "0azcalm95yh713fm1bmsqd73iz53fhmyajsg3fly6mmvffs7zjf5"))))
  (build-system python-build-system)
  (native-inputs `(("python-django", python-django)))
(arguments '(#:tests? #f)) ; gdal not found.
  (propagated-inputs
    `(("python-beautifulsoup4" ,python-beautifulsoup4)
      ("python-django" ,python-django)
      ("python-importlib-metadata"
       ,python-importlib-metadata)))
  (home-page
    "https://github.com/zostera/django-bootstrap4")
  (synopsis "Bootstrap 4 for Django")
  (description "Bootstrap 4 for Django")
  (license #f))



)

(define-public python-django-mathfilters
(package
  (name "python-django-mathfilters")
  (version "1.0.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "django-mathfilters" version))
      (sha256
        (base32
          "01sbnrznai6sz4w4qs7ld35039l7q5wh5zawwy1kd2gwdppr5f69"))))
  (build-system python-build-system)
    (native-inputs `(("python-django", python-django)))
;  (arguments '(#:tests? #f)) ; iteration problem
  (home-page
    "https://github.com/dbrgn/django-mathfilters")
  (synopsis
    "A set of simple math filters for Django")
  (description
    "A set of simple math filters for Django")
  (license license:expat))

)

(define-public python-tablib
(package
  (name "python-tablib")
  (version "3.0.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "tablib" version))
      (sha256
        (base32
          "03f1z6jq6rf67gwhbm9ma4rydm8h447a5nh5lcs5l8jg8l4aqg7q"))))
  (build-system python-build-system)
;  (arguments '(#:tests? #f)) ; need setuptools ?
  (native-inputs `(("python-setuptools-scm", python-setuptools-scm)))
  (home-page "https://tablib.readthedocs.io")
  (synopsis
    "Format agnostic tabular data library (XLS, JSON, YAML, CSV)")
  (description
    "Format agnostic tabular data library (XLS, JSON, YAML, CSV)")
  (license license:expat))


)

(define-public python-markuppy

(package
  (name "python-markuppy")
  (version "1.14")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "MarkupPy" version))
      (sha256
        (base32
          "0pqdmpxbr8iq22b4css2gz5z6s01ddpzyj25x27kgbs2lp0f5phs"))))
  (build-system python-build-system)
  (home-page
    "https://github.com/tylerbakke/MarkupPy")
  (synopsis "An HTML/XML generator")
  (description "An HTML/XML generator")
  (license license:expat))


)

(define-public python-xlwt
(package
  (name "python-xlwt")
  (version "1.3.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "xlwt" version))
      (sha256
        (base32
          "123c2pdamshkq75wwvck8fq0cjq1843xd3x9qaiz2a4vg9qi56f5"))))
  (build-system python-build-system)
  (arguments '(#:tests? #f)) ; code error
  (home-page "http://www.python-excel.org/")
  (synopsis   "Library to create spreadsheet files compatible with MS Excel 97/2000/XP/2003 XLS files, on any platform, with Python 2.6, 2.7, 3.3+")
  (description "Library to create spreadsheet files compatible with MS Excel 97/2000/XP/2003 XLS files, on any platform, with Python 2.6, 2.7, 3.3+")
  (license license:bsd-3))

)

(define-public python-django-import-export
(package
  (name "python-django-import-export")
  (version "2.5.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "django-import-export" version))
      (sha256
        (base32
          "0j0gsj18bkkmbn262qj2syla80vncvqn49bllwxvcgw0zhxh1763"))))
  (build-system python-build-system)
  (arguments '(#:tests? #f)) ; django error
  (native-inputs `(("python-markuppy", python-markuppy)
                   ("python-odfpy",python-odfpy)
                   ("python-xlwt",python-xlwt)
                   ("python-xlrd",python-xlrd)
                   ("python-pyyaml",python-pyyaml)
                   ("python-openpyxl",python-openpyxl)
  ))
  (propagated-inputs
    `(("python-diff-match-patch"
       ,python-diff-match-patch)
      ("python-django" ,python-django)
      ("python-tablib" ,python-tablib) 
       ))
  (home-page
    "https://github.com/django-import-export/django-import-export")
  (synopsis
    "Django application and library for importing and exporting data with included admin integration.")
  (description
    "Django application and library for importing and exporting data with included admin integration.")
  (license license:bsd-3))


)

(define-public python-django-timezone-field
(package
  (name "python-django-timezone-field")
  (version "4.1.2")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "django-timezone-field" version))
      (sha256
        (base32
          "1pxrs6mkayr2rqxj8q4wdfrdhw1dnzvwkacajdjy6q6ha8jcdyng"))))
  (build-system python-build-system)
  (arguments '(#:tests? #f)) ; needs rest-framework ?
  (propagated-inputs
    `(("python-django" ,python-django)
      ("python-pytz" ,python-pytz)))
  (home-page
    "http://github.com/mfogel/django-timezone-field/")
  (synopsis
    "A Django app providing database and form fields for pytz timezone objects.")
  (description
    "A Django app providing database and form fields for pytz timezone objects.")
  (license license:bsd-3))


)


(define-public python-crontab
(package
  (name "python-crontab")
  (version "2.5.1")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "python-crontab" version))
      (sha256
        (base32
          "0cccrqc10r8781ba81x8r2frs3pl2m4hkm599k5358ak0xr7xgjb"))))
  (build-system python-build-system)
  (arguments '(#:tests? #f)) ; AssertionError: 2 != 0 : Windows shell command not found!

  (propagated-inputs
    `(("python-dateutil" ,python-dateutil)))
  (home-page
    "https://gitlab.com/doctormo/python-crontab/")
  (synopsis "Python Crontab API")
  (description "Python Crontab API")
  (license #f))


)

(define-public python-click-repl

(package
  (name "python-click-repl")
  (version "0.1.6")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "click-repl" version))
      (sha256
        (base32
          "1mcmz95595nrp4r58spy1ac993db26hk4q97isghbmn4md99vwmr"))))
  (build-system python-build-system)
  (propagated-inputs
    `(("python-click" ,python-click)
      ("python-prompt-toolkit" ,python-prompt-toolkit)
      ("python-six" ,python-six)))
  (home-page
    "https://github.com/untitaker/click-repl")
  (synopsis "REPL plugin for Click")
  (description "REPL plugin for Click")
  (license license:expat))
)


(define-public python-click-didyoumean

(package
  (name "python-click-didyoumean")
  (version "0.0.3")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "click-didyoumean" version))
      (sha256
        (base32
          "1svaza5lpvdbmyrx5xi0riqzq4hb9wnlpqrg6r8zy14pbi42j8hi"))))
  (build-system python-build-system)
  (propagated-inputs
    `(("python-click" ,python-click)))
  (home-page
    "https://github.com/timofurrer/click-didyoumean")
  (synopsis
    "Enable git-like did-you-mean feature in click.")
  (description
    "Enable git-like did-you-mean feature in click.")
  (license #f))

)

(define-public python-pytest-celery

(package
  (name "python-pytest-celery")
  (version "0.0.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "pytest-celery" version))
      (sha256
        (base32
          "01pli108qqiiyrn8qsqqabcpazrzj27r7cji9wgglsk76by61l6g"))))
  (build-system python-build-system)
  (propagated-inputs
    `(("python-celery" ,python-celery-5.1.0)))
  (home-page
    "https://github.com/graingert/pytest-celery")
  (synopsis
    "pytest-celery a shim pytest plugin to enable celery.contrib.pytest")
  (description
    "pytest-celery a shim pytest plugin to enable celery.contrib.pytest")
  (license license:bsd-3))

)

(define-public python-celery-5.1.0

(package
  (name "python-celery")
  (version "5.1.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "celery" version))
      (sha256
        (base32
          "110ix1h9axnc5dwzyswi1cvypzbr38p1jb1msyf1ikvq8p063w35"))))
  (build-system python-build-system)
  (arguments '(#:tests? #f)) ; circular depedency
  (propagated-inputs
    `(("python-billiard" ,python-billiard)
      ("python-click" ,python-click)
      ("python-click-didyoumean"
       ,python-click-didyoumean)
      ("python-click-plugins" ,python-click-plugins)
      ("python-click-repl" ,python-click-repl)
      ("python-kombu" ,python-kombu)
      ("python-pytz" ,python-pytz)
      ("python-setuptools" ,python-setuptools)
      ("python-vine" ,python-vine)))
 ; (native-inputs
 ;   `(("python-pytest-celery" ,python-pytest-celery))) ;; circular depedency !!
  (home-page "http://celeryproject.org")
  (synopsis "Distributed Task Queue.")
  (description "Distributed Task Queue.")
  (license license:bsd-3))

)

(define-public python-django-celery-beat

(package
  (name "python-django-celery-beat")
  (version "2.2.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "django-celery-beat" version))
      (sha256
        (base32
          "0hx5ig1g0naagkmh39xbb64hhckd3b3pm12g9z03zig72pxkm8dq"))))
  (build-system python-build-system)
  (arguments '(#:tests? #f)) ; needs much more up to date python-vine

  ; (native-inputs `(("python-vine", python-vine)))
  (propagated-inputs
    `(("python-celery" ,python-celery-5.1.0)
      ("python-crontab" ,python-crontab)
      ("python-django" ,python-django)
      ("python-django-timezone-field"
       ,python-django-timezone-field)))
  (home-page
    "https://github.com/celery/django-celery-beat")
  (synopsis "Database-backed Periodic Tasks.")
  (description "Database-backed Periodic Tasks.")
  (license license:bsd-3))

)



(define-public glicid-clam
 (package
    (name "glicid-clam")
    (version "v0.0.1")
    (source 
     (origin
      (method git-fetch)
      (uri 
       (git-reference
        (url "https://oauth2:glpat-RsX2GjsD2WrzXubiJeou@gitlab.univ-nantes.fr/CCIPL/stages/2021/cluster_account_manager.git")        (commit version)
       )
      )
      
      (file-name (git-file-name name version))
      (sha256
       (base32
        "1x8q8wfl3p430dw8cawvfw4a4jjqw80gdg6zm90wmiad1f1lgd6r"
       )
      )
     )
    ) 

;    (build-system python-build-system)
     (build-system copy-build-system) 
    ;; XXX: The git repository has no tags, and the PyPI releases do not
    ;; contain tests.
    (arguments '(

;#:tests? #f
;                 #:use-setuptools? #f          ; uses custom distutils 'install' command

		  #:phases 
                   (modify-phases %standard-phases
                    (add-before 'reset-gzip-timestamps 'make-files-writable
                     (lambda* (#:key outputs #:allow-other-keys)
                      (let ((out (assoc-ref outputs "out")))
                       (for-each make-file-writable
                        (find-files out "\\.gz$"))
			#t))))))






;                )
;    )

    (propagated-inputs `(("python-keycloak-client" , python-keycloak-client-0.2.3)
		     ("python-ecdsa", python-ecdsa)
                     ("python-django", python-django)
                     ("python-celery", python-celery-5.1.0)
                     ("python-django-auth-ldap",python-django-auth-ldap)
                     ("python-django-autocomplete-light",python-django-autocomplete-light)
                     ("python-django-admin-row-actions", python-django-admin-row-actions)
                     ("python-django-bootstrap4",python-django-bootstrap4)
                     ("python-django-icons",python-django-icons)
                     ("python-django-mathfilters",python-django-mathfilters)
                     ("python-django-import-export",python-django-import-export)
                     ("python-django-celery-beat",python-django-celery-beat)
                     ("python-psycopg2",python-psycopg2)
                     ("python-magic",python-magic)

    ))
    (home-page "https://blabla")
    (synopsis "blabla")
    (description
     "This package provides a single-file minifier for CSS, HTML, and JavaScript.")
    ;; XXX: The README just says "GNU GPL and GNU LGPL and MIT".  From
    ;; <https://github.com/juancarlospaco/css-html-js-minify/issues/9> it
    ;; looks like the user can choose a license.
    (license (list license:gpl3+ license:lgpl3+ license:expat))

 )
)
