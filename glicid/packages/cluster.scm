(define-module (glicid packages cluster)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages networking)
  #:use-module ((gnu packages cluster) #:prefix gnu:)  
  #:use-module (glicid utils) 
)

(define-public keepalived-upstream-2.2.4
  (package
    (inherit gnu:keepalived)
    (name "keepalived-upstream")
    (version "2.2.4")
    (source (origin
      (method url-fetch)
      (uri (string-append "http://www.keepalived.org/software/keepalived-" version ".tar.gz"))
      (sha256 (base32 "1py1xdrxzdxn09yi8dx842rmhnc8lv7z09wmb2mfljylhy8dcf01" ))
    ))
  )
)

(define-public keepalived-upstream-2.2.7
  (package
    (inherit gnu:keepalived)
    (name "keepalived-upstream")
    (version "2.2.7")
    (source (origin
      (method url-fetch)
      (uri (string-append "http://www.keepalived.org/software/keepalived-" version ".tar.gz"))
      (sha256 (base32 "17flnzcs8hpj1g8nhhqn6bwbvpksyizcyzk2ah55cjhmfkc406f6" ))
    ))
  )
)


(define local:keepalived keepalived-upstream-2.2.7)
(define keepalived-latest (latest-version local:keepalived gnu:keepalived))

(define-public keepalived-glicid
  (package
    (inherit keepalived-latest)
    (name "keepalived-glicid")
    (version (string-append (package-version keepalived-latest) "-glicid" ))
    (arguments
      `(#:configure-flags
         (list
           (string-append "--enable-snmp")
           (string-append "--enable-snmp-checker")
           (string-append "--enable-snmp-rfc")
         )
       )   
    )
    (inputs `(
      ("net-snmp", net-snmp)
     ,@(package-inputs keepalived-latest)
    ))
  )
)
