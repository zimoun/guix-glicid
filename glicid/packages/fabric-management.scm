(define-module (glicid packages fabric-management)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module ((gnu packages fabric-management) #:prefix gnu:) 
  #:use-module (glicid packages linux)
  #:use-module (glicid utils)

)

(define-public ucx-upstream-1.11.2
  (package
    (inherit gnu:ucx)
    (name (string-append (package-name gnu:ucx) "-upstream" ))
    (version "1.11.2")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/openucx/ucx/releases/download/v" version "/ucx-" version ".tar.gz" ))
        (sha256 (base32 "1py62vjr0hgyqsdpr04jhn918i8ccn6ghjalwpcjpz24admgisyy"))
      )
    )
  )
)

(define-public ucx-upstream-1.12.0
  (package
    (inherit gnu:ucx)
    (name (string-append (package-name gnu:ucx) "-upstream" ))
    (version "1.12.0")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/openucx/ucx/releases/download/v" version "/ucx-" version ".tar.gz" ))
        (sha256 (base32 "1djxsakwjwnw21hhzsr02w6h2jd2k16bm4pah4iz6k8s5pg99sck"))
      )
    )
  )
)


(define local-ucx ucx-upstream-1.12.0)
(define-public ucx-latest (latest-version local-ucx gnu:ucx))



(define-public ucx-latest-glicid
  (transform-package
    (
      (instead-of "rdma-core" rdma-core-latest)
      ucx-latest
    ) "glicid"
  )
)
