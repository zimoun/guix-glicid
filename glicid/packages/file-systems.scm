(define-module (glicid packages file-systems)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages cyrus-sasl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages onc-rpc)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages kerberos)
  #:use-module (gnu packages nfs)
  #:use-module (gnu packages openldap)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages pkg-config)

)

(define-public autofs-glicid
  (package
    (name "autofs-glicid")
    (version "5.1.8")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://kernel.org/linux/daemons/autofs/"
                           "v" (version-major version) "/"
                           "autofs-" version ".tar.xz"))
       (sha256
	(base32 "1zf0fgf6kr9amxq5amlgsp1v13sizwl3wvx2xl7b4r2nhmci0gdk"))))
    (build-system gnu-build-system)
    (arguments
     `(#:configure-flags
       (list "--enable-ignore-busy"     ; during shutdown
             "--enable-sloppy-mount"    ; support mount(8) -s
             "--with-libtirpc"
             (string-append "--with-openldap="
                            (assoc-ref %build-inputs "openldap"))
             (string-append "--with-sasl="
                            (assoc-ref %build-inputs "cyrus-sasl"))
;             "HAVE_SSS_AUTOFS=1"        ; required to make sssldir click
;           (string-append "sssldir="
;                            (assoc-ref %build-inputs "sssd")
;                            "/lib/sssd/modules")
	     )
       #:tests? #f                      ; no test suite
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'fix-hard-coded-search-path
           (lambda _
             (substitute* "configure"
               (("^searchpath=\".*\"")
                "searchpath=\"$PATH\""))))
         (add-before 'configure 'fix-rpath
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out")))
               (substitute* "Makefile.rules"
                 (("^AUTOFS_LIB_LINK.*=" match)
                  (string-append match " -Wl,-rpath=" out "/lib"))))))
         (add-before 'install 'omit-obsolete-lookup_nis.so-link
           ;; Building lookup_yp.so depends on $(YPCLNT) but this doesn't,
           ;; leading to a make error.  Since it's broken, comment it out.
           (lambda _
             (substitute* "modules/Makefile"
               (("ln -fs lookup_yp.so" match)
                (string-append "# " match))))))))
  (native-inputs
     (list bison flex pkg-config rpcsvc-proto))
    (inputs
     (list cyrus-sasl
           e2fsprogs ; for e[234]fsck
           libtirpc
           libxml2 ; needed for LDAP, SASL
           mit-krb5 ; needed for LDAP, SASL
           nfs-utils ; for mount.nfs
           openldap
           openssl ; needed for SASL
 ;          sssd
           util-linux))     ; for mount, umount
    ;; XXX A directory index is the closest thing this has to a home page.
    (home-page "https://www.kernel.org/pub/linux/daemons/autofs/")
    (synopsis "Kernel-based automounter for Linux")
    (description
     "Autofs is a kernel-based automounter for use with the Linux autofs4
module.  It automatically mounts selected file systems when they are used and
unmounts them after a set period of inactivity.  This provides
centrally-managed, consistent file names for users and applications, even in a
large and/or frequently changing (network) environment.")
    ;; fedfs/ is GPL-2-only but not built.
 (license (list license:bsd-3        ; modules/cyrus-sasl.c
                   license:gpl2+))))    ; the rest
