(define-module (glicid packages gcc)
  #:use-module (guix packages)
  #:use-module (gnu packages commencement) ;; for make-gcc-toolchain
  #:use-module (gnu packages gcc) ;; for gcc-11
)

;; beware : access to internal functions… this is probably not the right way to do it …

(define custom-gcc (@@ (gnu packages gcc) custom-gcc) )
(define %generic-search-paths (@@ (gnu packages gcc) %generic-search-paths ))

;;

(define-public gfortran-11
  (hidden-package
    (custom-gcc gcc-11 "gfortran" '("fortran")
      %generic-search-paths
    )
  )
)

(define-public gfortran-toolchain-11
  (package 
    (inherit (make-gcc-toolchain gfortran-11))
    (synopsis "Complete GCC tool chain for fortran lang development")
    (description "This package provides a complete GCC tool chain for
                  fortran lang development to be installed in user profiles.  This includes
                  fortran, as well as libc (headers and binaries, plus debugging symbols
                  in the @code{debug} output), and binutils."
    )
  )
)
