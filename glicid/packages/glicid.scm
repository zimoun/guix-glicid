(define-module (glicid packages glicid)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
;  #:use-module (guix licenses)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages commencement)
  #:use-module (guix build-system python)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((guix utils) #:select (target-64bit?))
  #:use-module (gnu packages)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages freeipmi)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages parallel)
  #:use-module (gnu packages storage)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages gtk)  
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages readline)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages chemistry)
  #:use-module (gnu packages sphinx)
  #:use-module (srfi srfi-1)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages sssd)
  #:use-module (gnu packages fabric-management)
  #:use-module (gnu packages benchmark)
  #:use-module (gnu packages messaging)
  #:use-module (gnu packages web)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages cluster)  
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages fontutils)
  #:use-module (guix build-system gnu)
  #:use-module (gnu packages image)
  #:use-module (gnu packages maths)     
  #:use-module (guix utils)
  #:use-module (gnu packages cmake)
  #:use-module  (gnu packages pretty-print)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages cryptsetup)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages assembly)
  #:use-module (gnu packages authentication)
  #:use-module (gnu packages bdw-gc)
  #:use-module (gnu packages boost) 
  #:use-module (gnu packages compression)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages cryptsetup)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages disk)
  #:use-module (gnu packages gperf)
  #:use-module (gnu packages jemalloc)
  #:use-module (gnu packages linux)   
  #:use-module (gnu packages lua)     
  #:use-module (gnu packages ncurses) 
  #:use-module (gnu packages networking)
  #:use-module (gnu packages nss)
  #:use-module (gnu packages openldap)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages sphinx)
  #:use-module (gnu packages tls)   
  #:use-module (gnu packages web)   
;;  #:use-module (gnu packages glicid)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages virtualization)
;;  #:use-module (glicid packages storage)
  #:use-module (glicid packages mpi)
  #:use-module (glicid packages gcc)
  
)


;;;;;;;;


(define-public glicid-librdkafka-09
 (package 
  (inherit librdkafka) 
  (name "glicid-librdkafka-09")
  (version "0.9.2")
  (source 
   (origin
    (method url-fetch)
    (uri (string-append "https://github.com/edenhill/librdkafka/archive/v" version ".tar.gz"))
    (sha256 (base32 "0938yn1msaq8xcj5z7b3jcdy6rslh9kxmvz01r8rdcgbarlvchy2"))
   ))
))



(define-public glicid-x2go
 (package 
  (name "glicid-x2go")
  (version "4.1.0.3")
  (source 
   (origin
    (method url-fetch)
    (uri (string-append "https://code.x2go.org/releases/source/x2goserver/x2goserver-" version ".tar.gz"))
    (sha256 (base32 "1l6wd708kbipib4ldprfiihqmj4895nifg0bkws4x97majislxk7"))
   ))

    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("libtool" ,libtool)))

   (inputs
    `(("perl" , perl)))

   (build-system gnu-build-system)
   (synopsis "X2go")
   (description "X2go")
   (license "not checked")
   (home-page "https://wiki.x2go.org")
  )  
)


(define-public glicid-gromacs-openmpi
 (package
  (inherit gromacs)
  (name "glicid-gromacs-openmpi")
  (inputs `(("openmpi", openmpi)
   ;;         ("openssh", openssh) ;; only for tests... disabled now
                    ,@(package-inputs gromacs)))
  (build-system cmake-build-system)

   (arguments
       `(#:configure-flags
         (list "-DGMX_DEVELOPER_BUILD=on" ; Needed to run tests
               ;; Unbundling
               "-DGMX_USE_LMFIT=EXTERNAL"
               "-DGMX_BUILD_OWN_FFTW=off"
               "-DGMX_EXTERNAL_BLAS=on"
               "-DGMX_EXTERNAL_LAPACK=on"
               "-DGMX_EXTERNAL_TNG=on"
               "-DGMX_EXTERNAL_ZLIB=on"
               "-DGMX_EXTERNAL_TINYXML2=on"
               ;; special glicid
               "-DGMX_MPI=on"
               "-DCMAKE_C_COMPILER=mpicc"
               "-DCMAKE_CXX_COMPILER=mpicxx"
	       "-DGMX_DOUBLE=off"
	       "-DGMX_OPENMP=on"
               "-DGMX_SIMD=AVX2_256"
               (string-append "-DTinyXML2_DIR="
                              (assoc-ref %build-inputs "tinyxml2"))
               ;; Workaround for cmake/FindSphinx.cmake version parsing that does
               ;; not understand the guix-wrapped `sphinx-build --version' answer
               (string-append "-DSPHINX_EXECUTABLE_VERSION="
                              ,(package-version python-sphinx)))
         #:tests? #f
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'fixes
             (lambda* (#:key inputs #:allow-other-keys)
               ;; Still bundled: part of gromacs, source behind registration
               ;; but free software anyways
               ;;(delete-file-recursively "src/external/vmd_molfile")
               ;; Still bundled: threads-based OpenMPI-compatible fallback
               ;; designed to be bundled like that
               ;;(delete-file-recursively "src/external/thread_mpi")
               ;; Unbundling
               (delete-file-recursively "src/external/lmfit")
               (delete-file-recursively "src/external/clFFT")
               (delete-file-recursively "src/external/fftpack")
               (delete-file-recursively "src/external/build-fftw")
               (delete-file-recursively "src/external/tng_io")
               (delete-file-recursively "src/external/tinyxml2")
               (delete-file-recursively "src/external/googletest")
               (copy-recursively (assoc-ref inputs "googletest-source")
                                 "src/external/googletest")
               ;; This test warns about the build host hardware, disable
               (substitute* "src/gromacs/hardware/tests/hardwaretopology.cpp"
                 (("TEST\\(HardwareTopologyTest, HwlocExecute\\)")
                  "void __guix_disabled()"))
               #t)))))



  )

)


(define-public glicid-python-nbxmpp
 (package 
  (inherit python-nbxmpp) 
  (name "glicid-python-nbxmpp")
  (version "1.0.2")
  (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "nbxmpp" version))
       (sha256
        (base32
         "0vw5drr077w9ks4crnw6pwa4735ycyjdcm54knc3w4in4x5027wr"))))

))


(define-public glicid-specific-hdf5
  (package
    (inherit hdf5-1.12)
    (name "glicid-specific-hdf5")
;    (arguments
;     `(#:tests? #f)
;   )
      (native-inputs `(
       ("gcc",gcc-11)
       ("gfortran",gfortran-11)
      ,@(package-native-inputs hdf5-1.12)))
  )
)  


(define-public glicid-motif
  (package
    (name "glicid-motif")
    (version "2.3.8")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://sourceforge.net/projects/motif/files/Motif%202.3.8%20Source%20Code/motif-2.3.8.tar.gz/download"))

              (sha256 (base32 "1rxwkrhmj8sfg7dwmkhq885valwqbh26d79033q7vb7fcqv756w5"))
             )
    )
    (build-system gnu-build-system)

(arguments
     `(#:make-flags
;;	       #:make-flags
       (list (string-append "PREFIX=" %output)
;             (string-append "CC=" ,(cc-for-target))
             ;; Xft.h #includes <ft2build.h> without ‘freetype2/’.  The Makefile
             ;; works around this by hard-coding /usr/include & $PREFIX.
             (string-append "CPPFLAGS=-I"
                            (assoc-ref %build-inputs "freetype")
                            "/include/freetype2")
             "V=1")
 )
)

(inputs
      `(("libxext" ,libxext)
        ("libx11", libx11)
        ("freetype",freetype)
	("libxft",libxft)
        ("xbitmaps",xbitmaps)
        ("pkg-config" ,pkg-config)
	("flex" ,flex)
	("bison" ,bison)
        ("libxt" ,libxt)))

  (synopsis "Motif")
    (description "Motif ")
    (home-page "https://sourceforge.net/untested")
    (license " ")

 )
)

(define-public glicid-xmgrace
  (package
    (name "glicid-xmgrace")
    (version "5.1.25")
    (source (origin
              (method git-fetch)
              (uri (git-reference
              (url "https://oauth2:glpat-RsX2GjsD2WrzXubiJeou@gitlab.univ-nantes.fr/CCIPL/legacy_code_mirror/grace.git")
              (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256 (base32 "1wb8v0q8xa2akas0swpgdgw1s66i50k7kh1g2f894hnk91fzx56z"))
             )
    )
    (build-system gnu-build-system)

 (arguments
     `(
       #:phases
       (modify-phases %standard-phases
         (add-after 'configure 'patch-/bin/sh
           (lambda _
             (substitute* '("Make.conf")
              (("/bin/sh") (which "bash"))
             )
           ) 
         )
        )
       )
      ) 

    (inputs
      `(
        ("libx11", libx11)
        ("libxft",libxft)
        ("libxt",libxt)
	("libxpm",libxpm)
        ("libxext" ,libxext)
        ("autoconf", autoconf)
        ("glicid-motif" ,glicid-motif)
        ("fontconfig", fontconfig)
        ("gfortran", gfortran)
        ("libjpeg-turbo", libjpeg-turbo)
;;        ("libxmhtml", libxmhtml)
       )
    )


   (synopsis "Xmgrace")
   (description "xmgrace")
   (home-page "https://sourceforge.net/untested")
   (license " ")

 )
)

(use-modules (guix packages))
(use-modules (guix download))
(use-modules (gnu packages ruby))
(use-modules (guix build-system ruby))
;(use-modules (guix licenses))



(define-public ruby-asciidoctor-revealjs
 (package
  (name "ruby-asciidoctor-revealjs")
  (version "4.1.0")
  (source
    (origin
      (method url-fetch)
      (uri (rubygems-uri "asciidoctor-revealjs" version))
      (sha256
        (base32
          "03vmbcc3x059h17ry4qwk1p0yar9wgh87l2qssi307gy45cjw2mq"))))
  (build-system ruby-build-system)
  (arguments
 '(#:tests? #f)) 
  (propagated-inputs
    `(("ruby-asciidoctor" ,ruby-asciidoctor)
;      ("ruby-concurrent-ruby" ,ruby-concurrent-ruby)
       ("ruby-concurrent" ,ruby-concurrent)
       ("ruby-asciidoctor-kroki", ruby-asciidoctor-kroki)
      ("ruby-thread-safe" ,ruby-thread-safe)))
  (synopsis
    "Converts AsciiDoc documents into HTML5 presentations designed to be executed by the reveal.js presentation framework.")
  (description
    "Converts AsciiDoc documents into HTML5 presentations designed to be executed by the reveal.js presentation framework.")
  (home-page
    "https://github.com/asciidoctor/asciidoctor-reveal.js")
  (license license:expat))
)

(define-public ruby-asciidoctor-kroki
(package
  (name "ruby-asciidoctor-kroki")
  (version "0.4.0")
  (source
    (origin
      (method url-fetch)
      (uri (rubygems-uri "asciidoctor-kroki" version))
      (sha256
        (base32
          "13gx22xld4rbxxirnsxyrsajy9v666r8a4ngms71611af5afgk6w"))))
  (build-system ruby-build-system)
  (arguments
  '(#:tests? #f))  
  (propagated-inputs
    `(("ruby-asciidoctor" ,ruby-asciidoctor)))
  (synopsis
    "An extension for Asciidoctor to convert diagrams to images using https://kroki.io")
  (description
    "An extension for Asciidoctor to convert diagrams to images using https://kroki.io")
  (home-page
    "https://github.com/Mogztter/asciidoctor-kroki")
  (license license:expat)
)
)

;; python-glances is now upstreamed as glances in python-xyz

;;; glicid.scm ends here
