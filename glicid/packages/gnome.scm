(define-module (glicid packages gnome)
  #:use-module (guix packages)
  #:use-module (gnu packages sssd)
  #:use-module (gnu packages gnome)
)

(define-public glicid-terminator
  (package 
    (inherit terminator)
    (name "glicid-terminator")
    (propagated-inputs
      `(
         ("sssd" ,sssd)
         ,@(package-propagated-inputs terminator)
       )
    )
  )
)
