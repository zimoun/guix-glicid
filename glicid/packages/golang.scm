(define-module (glicid packages golang)
 #:use-module (guix build-system go)
 #:use-module (guix download)
 #:use-module (guix git-download)
 #:use-module ((guix licenses) #:prefix license:)
 #:use-module (guix packages)
)

(define-public go-github-com-netflix-go-expect-0.0.0-20220104043353-73e0943537d2
  (package
    (name "go-github-com-netflix-go-expect")
    (version "0.0.0-20220104043353-73e0943537d2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/Netflix/go-expect")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0zkvhnc4ii6ygvcsj54ng0kql26rnny7l3hy1w61g88mxjsww1b9"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/Netflix/go-expect"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify-1.6.1"
         ,go-github-com-stretchr-testify-1.6.1)
        ("go-github-com-creack-pty-1.1.17" ,go-github-com-creack-pty-1.1.17)))
    (home-page "https://github.com/Netflix/go-expect")
    (synopsis "go-expect")
    (description
      "Package expect provides an expect-like interface to automate control of
applications.  It is unlike expect in that it does not spawn or manage process
lifecycle.  This package only focuses on expecting output and sending input
through it's psuedoterminal.")
    (license license:asl2.0)))
(define-public go-github-com-protonmail-go-crypto-0.0.0-20220113124808-70ae35bab23f
  (package
    (name "go-github-com-protonmail-go-crypto")
    (version "0.0.0-20220113124808-70ae35bab23f")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/ProtonMail/go-crypto")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0dpalb7cd2xs0hji80zv378vf8gx4ph7b8xdd05kvbskyw67fb7n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/ProtonMail/go-crypto"))
    (propagated-inputs
      `(("go-golang-org-x-crypto-0.0.0-20210322153248-0c34fe9e7dc2"
         ,go-golang-org-x-crypto-0.0.0-20210322153248-0c34fe9e7dc2)))
    (home-page "https://github.com/ProtonMail/go-crypto")
    (synopsis #f)
    (description
      "This module is backwards compatible with x/crypto/openpgp, so you can simply
replace all imports of @code{golang.org/x/crypto/openpgp} with
@code{github.com/ProtonMail/go-crypto/openpgp}.")
    (license license:bsd-3)))
(define-public go-github-com-adigunhammedolalekan-registry-auth-0.0.0-20200730122110-8cde180a3a60
  (package
    (name "go-github-com-adigunhammedolalekan-registry-auth")
    (version "0.0.0-20200730122110-8cde180a3a60")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/adigunhammedolalekan/registry-auth")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0855a5c0ifi6q2ghb6gax0kwwra5j7pd2lplq4wp8r83dnv4vwj0"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "github.com/adigunhammedolalekan/registry-auth"))
    (propagated-inputs
      `(("go-github-com-sirupsen-logrus-1.4.2"
         ,go-github-com-sirupsen-logrus-1.4.2)
        ("go-github-com-gorilla-mux-1.7.4" ,go-github-com-gorilla-mux-1.7.4)
        ("go-github-com-docker-libtrust-0.0.0-20160708172513-aabc10ec26b7"
         ,go-github-com-docker-libtrust-0.0.0-20160708172513-aabc10ec26b7)
        ("go-github-com-docker-distribution-2.7.1+incompatible"
         ,go-github-com-docker-distribution-2.7.1+incompatible)))
    (home-page "https://github.com/adigunhammedolalekan/registry-auth")
    (synopsis "registry-auth")
    (description
      "a package to implements docker registry token authentication server as described
here
[https://github.com/docker/distribution/blob/1b9ab303a477ded9bdd3fc97e9119fa8f9e58fca/docs/spec/auth/index.md]")
    (license license:expat)))
(define-public go-github-com-apex-log-1.9.0
  (package
    (name "go-github-com-apex-log")
    (version "1.9.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/apex/log")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ibqkncnb8wcwilg2kyfyl5541g69rg551iy6m61q6iwdn5vfhi2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/apex/log"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2-2.2.2" ,go-gopkg-in-yaml-v2-2.2.2)
        ("go-gopkg-in-check-v1-1.0.0-20190902080502-41f04d3bba15"
         ,go-gopkg-in-check-v1-1.0.0-20190902080502-41f04d3bba15)
        ("go-golang-org-x-text-0.3.2" ,go-golang-org-x-text-0.3.2)
        ("go-golang-org-x-net-0.0.0-20190620200207-3b0461eec859"
         ,go-golang-org-x-net-0.0.0-20190620200207-3b0461eec859)
        ("go-github-com-tj-go-spin-1.1.0" ,go-github-com-tj-go-spin-1.1.0)
        ("go-github-com-tj-go-kinesis-0.0.0-20171128231115-08b17f58cb1b"
         ,go-github-com-tj-go-kinesis-0.0.0-20171128231115-08b17f58cb1b)
        ("go-github-com-tj-go-elastic-0.0.0-20171221160941-36157cbbebc2"
         ,go-github-com-tj-go-elastic-0.0.0-20171221160941-36157cbbebc2)
        ("go-github-com-tj-go-buffer-1.1.0" ,go-github-com-tj-go-buffer-1.1.0)
        ("go-github-com-tj-assert-0.0.3" ,go-github-com-tj-assert-0.0.3)
        ("go-github-com-stretchr-testify-1.6.1"
         ,go-github-com-stretchr-testify-1.6.1)
        ("go-github-com-smartystreets-gunit-1.0.0"
         ,go-github-com-smartystreets-gunit-1.0.0)
        ("go-github-com-smartystreets-go-aws-auth-0.0.0-20180515143844-0c1422d1fdb9"
         ,go-github-com-smartystreets-go-aws-auth-0.0.0-20180515143844-0c1422d1fdb9)
        ("go-github-com-rogpeppe-fastuuid-1.1.0"
         ,go-github-com-rogpeppe-fastuuid-1.1.0)
        ("go-github-com-pkg-errors-0.8.1" ,go-github-com-pkg-errors-0.8.1)
        ("go-github-com-mattn-go-colorable-0.1.2"
         ,go-github-com-mattn-go-colorable-0.1.2)
        ("go-github-com-kr-pretty-0.2.0" ,go-github-com-kr-pretty-0.2.0)
        ("go-github-com-jpillora-backoff-0.0.0-20180909062703-3050d21c67d7"
         ,go-github-com-jpillora-backoff-0.0.0-20180909062703-3050d21c67d7)
        ("go-github-com-google-uuid-1.1.1" ,go-github-com-google-uuid-1.1.1)
        ("go-github-com-golang-protobuf-1.3.1"
         ,go-github-com-golang-protobuf-1.3.1)
        ("go-github-com-go-logfmt-logfmt-0.4.0"
         ,go-github-com-go-logfmt-logfmt-0.4.0)
        ("go-github-com-fatih-color-1.7.0" ,go-github-com-fatih-color-1.7.0)
        ("go-github-com-aybabtme-rgbterm-0.0.0-20170906152045-cc83f3b3ce59"
         ,go-github-com-aybabtme-rgbterm-0.0.0-20170906152045-cc83f3b3ce59)
        ("go-github-com-aws-aws-sdk-go-1.20.6"
         ,go-github-com-aws-aws-sdk-go-1.20.6)
        ("go-github-com-aphistic-sweet-0.2.0"
         ,go-github-com-aphistic-sweet-0.2.0)
        ("go-github-com-aphistic-golf-0.0.0-20180712155816-02c07f170c5a"
         ,go-github-com-aphistic-golf-0.0.0-20180712155816-02c07f170c5a)
        ("go-github-com-apex-logs-1.0.0" ,go-github-com-apex-logs-1.0.0)))
    (home-page "https://github.com/apex/log")
    (synopsis "Handlers")
    (description
      "Package log implements a simple structured logging API designed with few
assumptions.  Designed for centralized logging solutions such as Kinesis which
require encoding and decoding before fanning-out to handlers.")
    (license license:expat)))
(define-public go-github-com-apptainer-container-key-client-0.7.2
  (package
    (name "go-github-com-apptainer-container-key-client")
    (version "0.7.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/apptainer/container-key-client")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0f4bb1k8pi37alzgzvjw23ynp54752qwgnr9gim3vc061qcjx377"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/apptainer/container-key-client"))
    (propagated-inputs
      `(("go-github-com-sylabs-json-resp-0.8.0"
         ,go-github-com-sylabs-json-resp-0.8.0)))
    (home-page "https://github.com/apptainer/container-key-client")
    (synopsis "Container Key Client")
    (description
      "This project provides a Go client to Apptainer for key storage and retrieval
using the HKP protocol.  Forked from
@url{https://github.com/sylabs/scs-key-client,sylabs/scs-key-client}.")
    (license license:bsd-3)))
(define-public go-github-com-apptainer-container-library-client-1.2.2
  (package
    (name "go-github-com-apptainer-container-library-client")
    (version "1.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/apptainer/container-library-client")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1f6pxzk0k9jf9612kjw6jlmrs8yzqqrvhjwfxwri1q1d4i410a0h"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "github.com/apptainer/container-library-client"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3-3.0.0-20200313102051-9f266ea9e77c"
         ,go-gopkg-in-yaml-v3-3.0.0-20200313102051-9f266ea9e77c)
        ("go-github-com-pmezard-go-difflib-1.0.0"
         ,go-github-com-pmezard-go-difflib-1.0.0)
        ("go-github-com-davecgh-go-spew-1.1.0"
         ,go-github-com-davecgh-go-spew-1.1.0)
        ("go-golang-org-x-sync-0.0.0-20200317015054-43a5402ce75a"
         ,go-golang-org-x-sync-0.0.0-20200317015054-43a5402ce75a)
        ("go-github-com-sylabs-json-resp-0.8.0"
         ,go-github-com-sylabs-json-resp-0.8.0)
        ("go-github-com-stretchr-testify-1.7.0"
         ,go-github-com-stretchr-testify-1.7.0)
        ("go-github-com-go-log-log-0.2.0" ,go-github-com-go-log-log-0.2.0)
        ("go-github-com-blang-semver-v4-4.0.0"
         ,go-github-com-blang-semver-v4-4.0.0)))
    (home-page "https://github.com/apptainer/container-library-client")
    (synopsis "Container Library Client")
    (description
      "This project provides a Go client to Apptainer for the container library.
Forked from
@url{https://github.com/sylabs/scs-library-client,sylabs/scs-library-client}.")
    (license license:bsd-3)))
(define-public go-github-com-apptainer-sif-v2-2.3.2
  (package
    (name "go-github-com-apptainer-sif-v2")
    (version "2.3.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/apptainer/sif")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "10cmvgyjxnfgw5661n1zbdr8zs29rx9izsr1bbnnyjr9kdqdhfg1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/apptainer/sif/v2"))
    (propagated-inputs
      `(("go-gopkg-in-warnings-v0-0.1.2" ,go-gopkg-in-warnings-v0-0.1.2)
        ("go-golang-org-x-sys-0.0.0-20211205182925-97ca703d548d"
         ,go-golang-org-x-sys-0.0.0-20211205182925-97ca703d548d)
        ("go-golang-org-x-net-0.0.0-20210813160813-60bc85c4be6d"
         ,go-golang-org-x-net-0.0.0-20210813160813-60bc85c4be6d)
        ("go-golang-org-x-crypto-0.0.0-20210817164053-32db794688a5"
         ,go-golang-org-x-crypto-0.0.0-20210817164053-32db794688a5)
        ("go-github-com-xanzy-ssh-agent-0.3.0"
         ,go-github-com-xanzy-ssh-agent-0.3.0)
        ("go-github-com-sergi-go-diff-1.1.0"
         ,go-github-com-sergi-go-diff-1.1.0)
        ("go-github-com-pmezard-go-difflib-1.0.0"
         ,go-github-com-pmezard-go-difflib-1.0.0)
        ("go-github-com-mitchellh-go-homedir-1.1.0"
         ,go-github-com-mitchellh-go-homedir-1.1.0)
        ("go-github-com-kevinburke-ssh-config-0.0.0-20201106050909-4977a11b4351"
         ,go-github-com-kevinburke-ssh-config-0.0.0-20201106050909-4977a11b4351)
        ("go-github-com-jbenet-go-context-0.0.0-20150711004518-d14ea06fba99"
         ,go-github-com-jbenet-go-context-0.0.0-20150711004518-d14ea06fba99)
        ("go-github-com-inconshreveable-mousetrap-1.0.0"
         ,go-github-com-inconshreveable-mousetrap-1.0.0)
        ("go-github-com-imdario-mergo-0.3.12"
         ,go-github-com-imdario-mergo-0.3.12)
        ("go-github-com-go-git-go-billy-v5-5.3.1"
         ,go-github-com-go-git-go-billy-v5-5.3.1)
        ("go-github-com-go-git-gcfg-1.5.0" ,go-github-com-go-git-gcfg-1.5.0)
        ("go-github-com-emirpasic-gods-1.12.0"
         ,go-github-com-emirpasic-gods-1.12.0)
        ("go-github-com-acomagu-bufpipe-1.0.3"
         ,go-github-com-acomagu-bufpipe-1.0.3)
        ("go-github-com-microsoft-go-winio-0.4.16"
         ,go-github-com-microsoft-go-winio-0.4.16)
        ("go-github-com-spf13-pflag-1.0.5" ,go-github-com-spf13-pflag-1.0.5)
        ("go-github-com-spf13-cobra-1.3.0" ,go-github-com-spf13-cobra-1.3.0)
        ("go-github-com-sebdah-goldie-v2-2.5.3"
         ,go-github-com-sebdah-goldie-v2-2.5.3)
        ("go-github-com-magefile-mage-1.12.1"
         ,go-github-com-magefile-mage-1.12.1)
        ("go-github-com-google-uuid-1.3.0" ,go-github-com-google-uuid-1.3.0)
        ("go-github-com-go-git-go-git-v5-5.4.2"
         ,go-github-com-go-git-go-git-v5-5.4.2)
        ("go-github-com-blang-semver-v4-4.0.0"
         ,go-github-com-blang-semver-v4-4.0.0)
        ("go-github-com-protonmail-go-crypto-0.0.0-20220113124808-70ae35bab23f"
         ,go-github-com-protonmail-go-crypto-0.0.0-20220113124808-70ae35bab23f)))
    (home-page "https://github.com/apptainer/sif")
    (synopsis "The Singularity Image Format (SIF)")
    (description
      "This module contains an open source implementation of the Singularity Image
Format (SIF) that makes it easy to create complete and encapsulated container
environments stored in a single file.")
    (license license:bsd-3)))
(define-public go-github-com-blang-semver-v4-4.0.0
  (package
    (name "go-github-com-blang-semver-v4")
    (version "4.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/blang/semver")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "14h9ys4n4kx9cbj42lkdf4i5k3nkll6sd62jcvl7cs565v6fiknz"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/blang/semver/v4"))
    (home-page "https://github.com/blang/semver")
    (synopsis #f)
    (description #f)
    (license license:expat)))
(define-public go-github-com-buger-jsonparser-1.1.1
  (package
    (name "go-github-com-buger-jsonparser")
    (version "1.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/buger/jsonparser")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0qv2lsh2biwxn927941gqiv5pqg7n4v58j0i536pjp7pr17pq7dp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/buger/jsonparser"))
    (home-page "https://github.com/buger/jsonparser")
    (synopsis
      "Alternative JSON parser for Go (10x times faster standard library)")
    (description
      "It does not require you to know the structure of the payload (eg.  create
structs), and allows accessing fields by providing the path to them.  It is up
to @strong{10 times faster} than standard @code{encoding/json} package
(depending on payload size and usage), @strong{allocates no memory}.  See
benchmarks below.")
    (license license:expat)))
(define-public go-github-com-cenkalti-backoff-v4-4.1.2
  (package
    (name "go-github-com-cenkalti-backoff-v4")
    (version "4.1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/cenkalti/backoff")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "08c28226q612i1pv83161y57qh16631vpc51ai9f76qfrzsy946z"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/cenkalti/backoff/v4"))
    (home-page "https://github.com/cenkalti/backoff")
    (synopsis "Exponential Backoff")
    (description
      "Package backoff implements backoff algorithms for retrying operations.")
    (license license:expat)))
(define-public go-github-com-containerd-cgroups-1.0.3
  (package
    (name "go-github-com-containerd-cgroups")
    (version "1.0.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/containerd/cgroups")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0mqsi2jm1f7xvs35kx6ijvadr6zycshc1xivq6xdiwrw24wb0m3r"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/containerd/cgroups"))
    (propagated-inputs
      `(("go-golang-org-x-sys-0.0.0-20210510120138-977fb7262007"
         ,go-golang-org-x-sys-0.0.0-20210510120138-977fb7262007)
        ("go-go-uber-org-goleak-1.1.12" ,go-go-uber-org-goleak-1.1.12)
        ("go-github-com-urfave-cli-1.22.2" ,go-github-com-urfave-cli-1.22.2)
        ("go-github-com-stretchr-testify-1.7.0"
         ,go-github-com-stretchr-testify-1.7.0)
        ("go-github-com-sirupsen-logrus-1.8.1"
         ,go-github-com-sirupsen-logrus-1.8.1)
        ("go-github-com-opencontainers-runtime-spec-1.0.2"
         ,go-github-com-opencontainers-runtime-spec-1.0.2)
        ("go-github-com-gogo-protobuf-1.3.2"
         ,go-github-com-gogo-protobuf-1.3.2)
        ("go-github-com-godbus-dbus-v5-5.0.4"
         ,go-github-com-godbus-dbus-v5-5.0.4)
        ("go-github-com-docker-go-units-0.4.0"
         ,go-github-com-docker-go-units-0.4.0)
        ("go-github-com-cpuguy83-go-md2man-v2-2.0.0"
         ,go-github-com-cpuguy83-go-md2man-v2-2.0.0)
        ("go-github-com-coreos-go-systemd-v22-22.3.2"
         ,go-github-com-coreos-go-systemd-v22-22.3.2)
        ("go-github-com-cilium-ebpf-0.4.0" ,go-github-com-cilium-ebpf-0.4.0)))
    (home-page "https://github.com/containerd/cgroups")
    (synopsis "cgroups")
    (description
      "Go package for creating, managing, inspecting, and destroying cgroups.  The
resources format for settings on the cgroup uses the OCI runtime-spec found
@url{https://github.com/opencontainers/runtime-spec,here}.")
    (license license:asl2.0)))
(define-public go-github-com-containerd-containerd-1.6.0
  (package
    (name "go-github-com-containerd-containerd")
    (version "1.6.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/containerd/containerd")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1wqmb124l8hjh7a053xjgh1kwblczg6q99l4vh6pi5zmnmdn67iv"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/containerd/containerd"))
    (propagated-inputs
      `(("go-google-golang-org-genproto-0.0.0-20200224152610-e50cd9704f63"
         ,go-google-golang-org-genproto-0.0.0-20200224152610-e50cd9704f63)
        ("go-github-com-urfave-cli-1.22.1" ,go-github-com-urfave-cli-1.22.1)
        ("go-github-com-gogo-googleapis-1.3.2"
         ,go-github-com-gogo-googleapis-1.3.2)
        ("go-k8s-io-utils-0.0.0-20210930125809-cb0fa318a74b"
         ,go-k8s-io-utils-0.0.0-20210930125809-cb0fa318a74b)
        ("go-k8s-io-klog-v2-2.30.0" ,go-k8s-io-klog-v2-2.30.0)
        ("go-k8s-io-cri-api-0.23.1" ,go-k8s-io-cri-api-0.23.1)
        ("go-k8s-io-component-base-0.22.5" ,go-k8s-io-component-base-0.22.5)
        ("go-k8s-io-client-go-0.22.5" ,go-k8s-io-client-go-0.22.5)
        ("go-k8s-io-apiserver-0.22.5" ,go-k8s-io-apiserver-0.22.5)
        ("go-k8s-io-apimachinery-0.22.5" ,go-k8s-io-apimachinery-0.22.5)
        ("go-k8s-io-api-0.22.5" ,go-k8s-io-api-0.22.5)
        ("go-gotest-tools-v3-3.0.3" ,go-gotest-tools-v3-3.0.3)
        ("go-google-golang-org-protobuf-1.27.1"
         ,go-google-golang-org-protobuf-1.27.1)
        ("go-google-golang-org-grpc-1.43.0" ,go-google-golang-org-grpc-1.43.0)
        ("go-golang-org-x-term-0.0.0-20210615171337-6886f2dfbf5b"
         ,go-golang-org-x-term-0.0.0-20210615171337-6886f2dfbf5b)
        ("go-golang-org-x-sys-0.0.0-20211216021012-1d35b9e2eb4e"
         ,go-golang-org-x-sys-0.0.0-20211216021012-1d35b9e2eb4e)
        ("go-golang-org-x-sync-0.0.0-20210220032951-036812b2e83c"
         ,go-golang-org-x-sync-0.0.0-20210220032951-036812b2e83c)
        ("go-golang-org-x-oauth2-0.0.0-20210819190943-2bc19b11175f"
         ,go-golang-org-x-oauth2-0.0.0-20210819190943-2bc19b11175f)
        ("go-golang-org-x-net-0.0.0-20211216030914-fe4d6282115f"
         ,go-golang-org-x-net-0.0.0-20211216030914-fe4d6282115f)
        ("go-golang-org-x-crypto-0.0.0-20210817164053-32db794688a5"
         ,go-golang-org-x-crypto-0.0.0-20210817164053-32db794688a5)
        ("go-go-opentelemetry-io-otel-trace-1.3.0"
         ,go-go-opentelemetry-io-otel-trace-1.3.0)
        ("go-go-opentelemetry-io-otel-sdk-1.3.0"
         ,go-go-opentelemetry-io-otel-sdk-1.3.0)
        ("go-go-opentelemetry-io-otel-exporters-otlp-otlptrace-otlptracehttp-1.3.0"
         ,go-go-opentelemetry-io-otel-exporters-otlp-otlptrace-otlptracehttp-1.3.0)
        ("go-go-opentelemetry-io-otel-exporters-otlp-otlptrace-otlptracegrpc-1.3.0"
         ,go-go-opentelemetry-io-otel-exporters-otlp-otlptrace-otlptracegrpc-1.3.0)
        ("go-go-opentelemetry-io-otel-exporters-otlp-otlptrace-1.3.0"
         ,go-go-opentelemetry-io-otel-exporters-otlp-otlptrace-1.3.0)
        ("go-go-opentelemetry-io-otel-1.3.0"
         ,go-go-opentelemetry-io-otel-1.3.0)
        ("go-go-opentelemetry-io-contrib-instrumentation-google-golang-org-grpc-otelgrpc-0.28.0"
         ,go-go-opentelemetry-io-contrib-instrumentation-google-golang-org-grpc-otelgrpc-0.28.0)
        ("go-go-etcd-io-bbolt-1.3.6" ,go-go-etcd-io-bbolt-1.3.6)
        ("go-github-com-vishvananda-netlink-1.1.1-0.20210330154013-f5de75959ad5"
         ,go-github-com-vishvananda-netlink-1.1.1-0.20210330154013-f5de75959ad5)
        ("go-github-com-tchap-go-patricia-2.2.6+incompatible"
         ,go-github-com-tchap-go-patricia-2.2.6+incompatible)
        ("go-github-com-stretchr-testify-1.7.0"
         ,go-github-com-stretchr-testify-1.7.0)
        ("go-github-com-sirupsen-logrus-1.8.1"
         ,go-github-com-sirupsen-logrus-1.8.1)
        ("go-github-com-satori-go-uuid-1.2.0"
         ,go-github-com-satori-go-uuid-1.2.0)
        ("go-github-com-prometheus-client-golang-1.11.0"
         ,go-github-com-prometheus-client-golang-1.11.0)
        ("go-github-com-pelletier-go-toml-1.9.3"
         ,go-github-com-pelletier-go-toml-1.9.3)
        ("go-github-com-opencontainers-selinux-1.10.0"
         ,go-github-com-opencontainers-selinux-1.10.0)
        ("go-github-com-opencontainers-runtime-spec-1.0.3-0.20210326190908-1c3f411f0417"
         ,go-github-com-opencontainers-runtime-spec-1.0.3-0.20210326190908-1c3f411f0417)
        ("go-github-com-opencontainers-runc-1.1.0"
         ,go-github-com-opencontainers-runc-1.1.0)
        ("go-github-com-opencontainers-image-spec-1.0.2-0.20211117181255-693428a734f5"
         ,go-github-com-opencontainers-image-spec-1.0.2-0.20211117181255-693428a734f5)
        ("go-github-com-opencontainers-go-digest-1.0.0"
         ,go-github-com-opencontainers-go-digest-1.0.0)
        ("go-github-com-moby-sys"
         ,go-github-com-moby-sys)
        ("go-github-com-moby-locker-1.0.1" ,go-github-com-moby-locker-1.0.1)
        ("go-github-com-klauspost-compress-1.11.13"
         ,go-github-com-klauspost-compress-1.11.13)
        ("go-github-com-json-iterator-go-1.1.12"
         ,go-github-com-json-iterator-go-1.1.12)
        ("go-github-com-intel-goresctrl-0.2.0"
         ,go-github-com-intel-goresctrl-0.2.0)
        ("go-github-com-imdario-mergo-0.3.12"
         ,go-github-com-imdario-mergo-0.3.12)
        ("go-github-com-hashicorp-go-multierror-1.1.1"
         ,go-github-com-hashicorp-go-multierror-1.1.1)
        ("go-github-com-grpc-ecosystem-go-grpc-prometheus-1.2.0"
         ,go-github-com-grpc-ecosystem-go-grpc-prometheus-1.2.0)
        ("go-github-com-grpc-ecosystem-go-grpc-middleware-1.3.0"
         ,go-github-com-grpc-ecosystem-go-grpc-middleware-1.3.0)
        ("go-github-com-google-uuid-1.2.0" ,go-github-com-google-uuid-1.2.0)
        ("go-github-com-google-go-cmp-0.5.6"
         ,go-github-com-google-go-cmp-0.5.6)
        ("go-github-com-gogo-protobuf-1.3.2"
         ,go-github-com-gogo-protobuf-1.3.2)
        ("go-github-com-go-logr-stdr-1.2.2" ,go-github-com-go-logr-stdr-1.2.2)
        ("go-github-com-fsnotify-fsnotify-1.4.9"
         ,go-github-com-fsnotify-fsnotify-1.4.9)
        ("go-github-com-emicklei-go-restful-2.9.5+incompatible"
         ,go-github-com-emicklei-go-restful-2.9.5+incompatible)
        ("go-github-com-docker-go-units-0.4.0"
         ,go-github-com-docker-go-units-0.4.0)
        ("go-github-com-docker-go-metrics-0.0.1"
         ,go-github-com-docker-go-metrics-0.0.1)
        ("go-github-com-docker-go-events-0.0.0-20190806004212-e31b211e4f1c"
         ,go-github-com-docker-go-events-0.0.0-20190806004212-e31b211e4f1c)
        ("go-github-com-davecgh-go-spew-1.1.1"
         ,go-github-com-davecgh-go-spew-1.1.1)
        ("go-github-com-coreos-go-systemd-v22-22.3.2"
         ,go-github-com-coreos-go-systemd-v22-22.3.2)
        ("go-github-com-containernetworking-plugins-1.0.1"
         ,go-github-com-containernetworking-plugins-1.0.1)
        ("go-github-com-containerd-zfs-1.0.0"
         ,go-github-com-containerd-zfs-1.0.0)
        ("go-github-com-containerd-typeurl-1.0.2"
         ,go-github-com-containerd-typeurl-1.0.2)
        ("go-github-com-containerd-ttrpc-1.1.0"
         ,go-github-com-containerd-ttrpc-1.1.0)
        ("go-github-com-containerd-nri-0.1.0"
         ,go-github-com-containerd-nri-0.1.0)
        ("go-github-com-containerd-imgcrypt-1.1.3"
         ,go-github-com-containerd-imgcrypt-1.1.3)
        ("go-github-com-containerd-go-runc-1.0.0"
         ,go-github-com-containerd-go-runc-1.0.0)
        ("go-github-com-containerd-go-cni-1.1.3"
         ,go-github-com-containerd-go-cni-1.1.3)
        ("go-github-com-containerd-fifo-1.0.0"
         ,go-github-com-containerd-fifo-1.0.0)
        ("go-github-com-containerd-continuity-0.2.2"
         ,go-github-com-containerd-continuity-0.2.2)
        ("go-github-com-containerd-console-1.0.3"
         ,go-github-com-containerd-console-1.0.3)
        ("go-github-com-containerd-cgroups-1.0.3"
         ,go-github-com-containerd-cgroups-1.0.3)
        ("go-github-com-containerd-btrfs-1.0.0"
         ,go-github-com-containerd-btrfs-1.0.0)
        ("go-github-com-containerd-aufs-1.0.0"
         ,go-github-com-containerd-aufs-1.0.0)
        ("go-github-com-microsoft-hcsshim-0.9.2"
         ,go-github-com-microsoft-hcsshim-0.9.2)
        ("go-github-com-microsoft-go-winio-0.5.1"
         ,go-github-com-microsoft-go-winio-0.5.1)
        ("go-github-com-adalogics-go-fuzz-headers-0.0.0-20210715213245-6c3934b029d8"
         ,go-github-com-adalogics-go-fuzz-headers-0.0.0-20210715213245-6c3934b029d8)
        ("go-cloud-google-com-go-0.81.0" ,go-cloud-google-com-go-0.81.0)))
    (home-page "https://github.com/containerd/containerd")
    (synopsis "Now Recruiting")
    (description
      "containerd is an industry-standard container runtime with an emphasis on
simplicity, robustness and portability.  It is available as a daemon for Linux
and Windows, which can manage the complete container lifecycle of its host
system: image transfer and storage, container execution and supervision,
low-level storage and network attachments, etc.")
    (license license:asl2.0)))
(define-public go-github-com-containernetworking-cni-1.0.1
  (package
    (name "go-github-com-containernetworking-cni")
    (version "1.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/containernetworking/cni")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0jzijcb96z26lchh6j4js3rl7c148mwlaiz3s6ww47kljnxmv05f"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/containernetworking/cni"))
    (propagated-inputs
      `(("go-github-com-onsi-gomega-1.10.1" ,go-github-com-onsi-gomega-1.10.1)
        ("go-github-com-onsi-ginkgo-1.13.0"
         ,go-github-com-onsi-ginkgo-1.13.0)))
    (home-page "https://github.com/containernetworking/cni")
    (synopsis "CNI - the Container Network Interface")
    (description
      "CNI (), a @url{https://cncf.io,Cloud Native Computing Foundation} project,
consists of a specification and libraries for writing plugins to configure
network interfaces in Linux containers, along with a number of supported
plugins.  CNI concerns itself only with network connectivity of containers and
removing allocated resources when the container is deleted.  Because of this
focus, CNI has a wide range of support and the specification is simple to
implement.")
    (license license:asl2.0)))
(define-public go-github-com-containernetworking-plugins-1.1.0
  (package
    (name "go-github-com-containernetworking-plugins")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/containernetworking/plugins")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "10c9sxi5nq88772ql35a3vy5m1wrdyji4imc4ysl27malcqxhiik"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/containernetworking/plugins"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2-2.4.0" ,go-gopkg-in-yaml-v2-2.4.0)
        ("go-gopkg-in-tomb-v1-1.0.0-20141024135613-dd632973f1e7"
         ,go-gopkg-in-tomb-v1-1.0.0-20141024135613-dd632973f1e7)
        ("go-golang-org-x-text-0.3.6" ,go-golang-org-x-text-0.3.6)
        ("go-golang-org-x-net-0.0.0-20210428140749-89ef3d95e781"
         ,go-golang-org-x-net-0.0.0-20210428140749-89ef3d95e781)
        ("go-go-opencensus-io-0.22.3" ,go-go-opencensus-io-0.22.3)
        ("go-github-com-vishvananda-netns-0.0.0-20210104183010-2eb08e3e575f"
         ,go-github-com-vishvananda-netns-0.0.0-20210104183010-2eb08e3e575f)
        ("go-github-com-sirupsen-logrus-1.8.1"
         ,go-github-com-sirupsen-logrus-1.8.1)
        ("go-github-com-pkg-errors-0.9.1" ,go-github-com-pkg-errors-0.9.1)
        ("go-github-com-nxadm-tail-1.4.8" ,go-github-com-nxadm-tail-1.4.8)
        ("go-github-com-golang-groupcache-0.0.0-20200121045136-8c9f03a8e57e"
         ,go-github-com-golang-groupcache-0.0.0-20200121045136-8c9f03a8e57e)
        ("go-github-com-gogo-protobuf-1.3.2"
         ,go-github-com-gogo-protobuf-1.3.2)
        ("go-github-com-fsnotify-fsnotify-1.4.9"
         ,go-github-com-fsnotify-fsnotify-1.4.9)
        ("go-github-com-containerd-cgroups-1.0.1"
         ,go-github-com-containerd-cgroups-1.0.1)
        ("go-github-com-microsoft-go-winio-0.4.17"
         ,go-github-com-microsoft-go-winio-0.4.17)
        ("go-golang-org-x-sys-0.0.0-20210809222454-d867a43fc93e"
         ,go-golang-org-x-sys-0.0.0-20210809222454-d867a43fc93e)
        ("go-github-com-vishvananda-netlink-1.1.1-0.20210330154013-f5de75959ad5"
         ,go-github-com-vishvananda-netlink-1.1.1-0.20210330154013-f5de75959ad5)
        ("go-github-com-safchain-ethtool-0.0.0-20210803160452-9aa261dae9b1"
         ,go-github-com-safchain-ethtool-0.0.0-20210803160452-9aa261dae9b1)
        ("go-github-com-onsi-gomega-1.15.0" ,go-github-com-onsi-gomega-1.15.0)
        ("go-github-com-onsi-ginkgo-1.16.4" ,go-github-com-onsi-ginkgo-1.16.4)
        ("go-github-com-networkplumbing-go-nft-0.2.0"
         ,go-github-com-networkplumbing-go-nft-0.2.0)
        ("go-github-com-mattn-go-shellwords-1.0.12"
         ,go-github-com-mattn-go-shellwords-1.0.12)
        ("go-github-com-godbus-dbus-v5-5.0.4"
         ,go-github-com-godbus-dbus-v5-5.0.4)
        ("go-github-com-d2g-dhcp4server-0.0.0-20181031114812-7d4a0a7f59a5"
         ,go-github-com-d2g-dhcp4server-0.0.0-20181031114812-7d4a0a7f59a5)
        ("go-github-com-d2g-dhcp4client-1.0.0"
         ,go-github-com-d2g-dhcp4client-1.0.0)
        ("go-github-com-d2g-dhcp4-0.0.0-20170904100407-a1d1b6c41b1c"
         ,go-github-com-d2g-dhcp4-0.0.0-20170904100407-a1d1b6c41b1c)
        ("go-github-com-coreos-go-systemd-v22-22.3.2"
         ,go-github-com-coreos-go-systemd-v22-22.3.2)
        ("go-github-com-coreos-go-iptables-0.6.0"
         ,go-github-com-coreos-go-iptables-0.6.0)
        ("go-github-com-containernetworking-cni-1.0.1"
         ,go-github-com-containernetworking-cni-1.0.1)
        ("go-github-com-buger-jsonparser-1.1.1"
         ,go-github-com-buger-jsonparser-1.1.1)
        ("go-github-com-alexflint-go-filemutex-1.1.0"
         ,go-github-com-alexflint-go-filemutex-1.1.0)
        ("go-github-com-microsoft-hcsshim-0.8.20"
         ,go-github-com-microsoft-hcsshim-0.8.20)))
    (home-page "https://github.com/containernetworking/plugins")
    (synopsis "Plugins")
    (description
      "Some CNI network plugins, maintained by the containernetworking team.  For more
information, see the @url{https://www.cni.dev,CNI website}.")
    (license license:asl2.0)))
(define-public go-github-com-containers-image-v5-5.19.1
  (package
    (name "go-github-com-containers-image-v5")
    (version "5.19.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/containers/image")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wyq3ns6yrzi9bff2kb8p3rppw652m4ffq8374sdjkn7f39z4inf"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/containers/image/v5"))
    (propagated-inputs
      `(("go-golang-org-x-term-0.0.0-20201126162022-7de9c90e9dd1"
         ,go-golang-org-x-term-0.0.0-20201126162022-7de9c90e9dd1)
        ("go-golang-org-x-sys-0.0.0-20220114195835-da31bd327af9"
         ,go-golang-org-x-sys-0.0.0-20220114195835-da31bd327af9)
        ("go-golang-org-x-sync-0.0.0-20210220032951-036812b2e83c"
         ,go-golang-org-x-sync-0.0.0-20210220032951-036812b2e83c)
        ("go-golang-org-x-net-0.0.0-20211112202133-69e39bad7dc2"
         ,go-golang-org-x-net-0.0.0-20211112202133-69e39bad7dc2)
        ("go-golang-org-x-crypto-0.0.0-20211215153901-e495a2d5b3d3"
         ,go-golang-org-x-crypto-0.0.0-20211215153901-e495a2d5b3d3)
        ("go-go-etcd-io-bbolt-1.3.6" ,go-go-etcd-io-bbolt-1.3.6)
        ("go-github-com-xeipuuv-gojsonschema-1.2.0"
         ,go-github-com-xeipuuv-gojsonschema-1.2.0)
        ("go-github-com-xeipuuv-gojsonpointer-0.0.0-20190809123943-df4f5c81cb3b"
         ,go-github-com-xeipuuv-gojsonpointer-0.0.0-20190809123943-df4f5c81cb3b)
        ("go-github-com-vbauerster-mpb-v7-7.3.2"
         ,go-github-com-vbauerster-mpb-v7-7.3.2)
        ("go-github-com-vbatts-tar-split-0.11.2"
         ,go-github-com-vbatts-tar-split-0.11.2)
        ("go-github-com-ulikunitz-xz-0.5.10"
         ,go-github-com-ulikunitz-xz-0.5.10)
        ("go-github-com-sylabs-sif-v2-2.3.1"
         ,go-github-com-sylabs-sif-v2-2.3.1)
        ("go-github-com-stretchr-testify-1.7.0"
         ,go-github-com-stretchr-testify-1.7.0)
        ("go-github-com-sirupsen-logrus-1.8.1"
         ,go-github-com-sirupsen-logrus-1.8.1)
        ("go-github-com-proglottis-gpgme-0.1.1"
         ,go-github-com-proglottis-gpgme-0.1.1)
        ("go-github-com-pkg-errors-0.9.1" ,go-github-com-pkg-errors-0.9.1)
        ("go-github-com-ostreedev-ostree-go-0.0.0-20190702140239-759a8c1ac913"
         ,go-github-com-ostreedev-ostree-go-0.0.0-20190702140239-759a8c1ac913)
        ("go-github-com-opencontainers-selinux-1.10.0"
         ,go-github-com-opencontainers-selinux-1.10.0)
        ("go-github-com-opencontainers-image-spec-1.0.3-0.20211202193544-a5463b7f9c84"
         ,go-github-com-opencontainers-image-spec-1.0.3-0.20211202193544-a5463b7f9c84)
        ("go-github-com-opencontainers-go-digest-1.0.0"
         ,go-github-com-opencontainers-go-digest-1.0.0)
        ("go-github-com-manifoldco-promptui-0.9.0"
         ,go-github-com-manifoldco-promptui-0.9.0)
        ("go-github-com-klauspost-pgzip-1.2.5"
         ,go-github-com-klauspost-pgzip-1.2.5)
        ("go-github-com-klauspost-compress-1.14.2"
         ,go-github-com-klauspost-compress-1.14.2)
        ("go-github-com-imdario-mergo-0.3.12"
         ,go-github-com-imdario-mergo-0.3.12)
        ("go-github-com-hashicorp-go-multierror-1.1.1"
         ,go-github-com-hashicorp-go-multierror-1.1.1)
        ("go-github-com-gorilla-mux-1.7.4" ,go-github-com-gorilla-mux-1.7.4)
        ("go-github-com-ghodss-yaml-1.0.0" ,go-github-com-ghodss-yaml-1.0.0)
        ("go-github-com-docker-libtrust-0.0.0-20160708172513-aabc10ec26b7"
         ,go-github-com-docker-libtrust-0.0.0-20160708172513-aabc10ec26b7)
        ("go-github-com-docker-go-connections-0.4.0"
         ,go-github-com-docker-go-connections-0.4.0)
        ("go-github-com-docker-docker-credential-helpers-0.6.4"
         ,go-github-com-docker-docker-credential-helpers-0.6.4)
        ("go-github-com-docker-docker-20.10.12+incompatible"
         ,go-github-com-docker-docker-20.10.12+incompatible)
        ("go-github-com-docker-distribution-2.7.1+incompatible"
         ,go-github-com-docker-distribution-2.7.1+incompatible)
        ("go-github-com-containers-storage-1.38.2"
         ,go-github-com-containers-storage-1.38.2)
        ("go-github-com-containers-ocicrypt-1.1.2"
         ,go-github-com-containers-ocicrypt-1.1.2)
        ("go-github-com-containers-libtrust-0.0.0-20190913040956-14b96171aa3b"
         ,go-github-com-containers-libtrust-0.0.0-20190913040956-14b96171aa3b)
        ("go-github-com-containerd-containerd-1.5.9"
         ,go-github-com-containerd-containerd-1.5.9)
        ("go-github-com-burntsushi-toml-1.0.0"
         ,go-github-com-burntsushi-toml-1.0.0)
        ("go-github-com-14rcole-gopopulate-0.0.0-20180821133914-b175b219e774"
         ,go-github-com-14rcole-gopopulate-0.0.0-20180821133914-b175b219e774)))
    (home-page "https://github.com/containers/image")
    (synopsis "")
    (description
      "The package image provides libraries and commands to interact with container
images.")
    (license license:asl2.0)))
(define-public go-github-com-creack-pty-1.1.17
  (package
    (name "go-github-com-creack-pty")
    (version "1.1.17")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/creack/pty")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "09lcq5bw1dwppxjia05sj4fy7gbk62vishfz1bgrbd1r06i57mjf"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/creack/pty"))
    (home-page "https://github.com/creack/pty")
    (synopsis "pty")
    (description
      "Package pty provides functions for working with Unix terminals.")
    (license license:expat)))
(define-public go-github-com-cyphar-filepath-securejoin-0.2.3
  (package
    (name "go-github-com-cyphar-filepath-securejoin")
    (version "0.2.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/cyphar/filepath-securejoin")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0w03d8hslwfhcnfqck21agzs7y0sc6gpwfz53md8fv26ishwxppy"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/cyphar/filepath-securejoin"))
    (home-page "https://github.com/cyphar/filepath-securejoin")
    (synopsis #f)
    (description
      "Package securejoin is an implementation of the hopefully-soon-to-be-included
SecureJoin helper that is meant to be part of the \"path/filepath\" package.  The
purpose of this project is to provide a PoC implementation to make the
SecureJoin proposal
(@url{https://github.com/golang/go/issues/20126,https://github.com/golang/go/issues/20126})
more tangible.")
    (license license:bsd-3)))
(define-public go-github-com-docker-docker-20.10.12+incompatible
  (package
    (name "go-github-com-docker-docker")
    (version "20.10.12+incompatible")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/moby/moby")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "17j820ij8p6w61jyg5g6y5dwv56dh1w178f6ia2i2dc9b8mz6b5a"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/docker/docker"))
    (home-page "https://github.com/docker/docker")
    (synopsis "The Moby Project")
    (description
      "Moby is an open-source project created by Docker to enable and accelerate
software containerization.")
    (license license:asl2.0)))
(define-public go-github-com-fatih-color-1.13.0
  (package
    (name "go-github-com-fatih-color")
    (version "1.13.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/fatih/color")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "029qkxsdpblhrpgbv4fcmqwkqnjhx08hwiqp19pd7zz6l8a373ay"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/fatih/color"))
    (propagated-inputs
      `(("go-github-com-mattn-go-isatty-0.0.14"
         ,go-github-com-mattn-go-isatty-0.0.14)
        ("go-github-com-mattn-go-colorable-0.1.9"
         ,go-github-com-mattn-go-colorable-0.1.9)))
    (home-page "https://github.com/fatih/color")
    (synopsis "color")
    (description
      "Package color is an ANSI color package to output colorized or SGR defined output
to the standard output.  The API can be used in several way, pick one that suits
you.")
    (license license:expat)))
(define-public go-github-com-go-log-log-0.2.0
  (package
    (name "go-github-com-go-log-log")
    (version "0.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-log/log")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1idnqv4yvkmdh3wcsgvhcpak9z6ix8dsifdjq5kfbbnskdq5rmvg"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-log/log"))
    (home-page "https://github.com/go-log/log")
    (synopsis "Log")
    (description "Package log provides a log interface")
    (license license:expat)))
(define-public go-github-com-google-uuid-1.3.0
  (package
    (name "go-github-com-google-uuid")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/google/uuid")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0imkw52m7fzrwsdj2rfrk3zbplqfbwncyv6hv89xw0vdw3jpk122"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/uuid"))
    (home-page "https://github.com/google/uuid")
    (synopsis "uuid")
    (description "Package uuid generates and inspects UUIDs.")
    (license license:bsd-3)))
(define-public go-github-com-opencontainers-go-digest-1.0.0
  (package
    (name "go-github-com-opencontainers-go-digest")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/opencontainers/go-digest")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0i5acjajvr6hi9zb7gxwifd8w28y884cv7cx36adj8lngj647xbi"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/opencontainers/go-digest"))
    (home-page "https://github.com/opencontainers/go-digest")
    (synopsis "go-digest")
    (description
      "Package digest provides a generalized type to opaquely represent message digests
and their operations within the registry.  The Digest type is designed to serve
as a flexible identifier in a content-addressable system.  More importantly, it
provides tools and wrappers to work with hash.Hash-based digests with little
effort.")
    (license (list license:asl2.0 license:cc-by-sa4.0))))
(define-public go-github-com-opencontainers-image-spec-1.0.3-0.20211202193544-a5463b7f9c84
  (package
    (name "go-github-com-opencontainers-image-spec")
    (version "1.0.3-0.20211202193544-a5463b7f9c84")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/opencontainers/image-spec")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "11js6467d7fhcvad4aw26d4p0r5b33vf41qhcf1bdwx0y7smd7s4"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/opencontainers/image-spec"))
    (propagated-inputs
      `(("go-github-com-xeipuuv-gojsonschema-1.2.0"
         ,go-github-com-xeipuuv-gojsonschema-1.2.0)
        ("go-github-com-xeipuuv-gojsonreference-0.0.0-20180127040603-bd5ef7bd5415"
         ,go-github-com-xeipuuv-gojsonreference-0.0.0-20180127040603-bd5ef7bd5415)
        ("go-github-com-russross-blackfriday-1.6.0"
         ,go-github-com-russross-blackfriday-1.6.0)
        ("go-github-com-pkg-errors-0.9.1" ,go-github-com-pkg-errors-0.9.1)
        ("go-github-com-opencontainers-go-digest-1.0.0"
         ,go-github-com-opencontainers-go-digest-1.0.0)))
    (home-page "https://github.com/opencontainers/image-spec")
    (synopsis "OCI Image Format Specification")
    (description
      "The OCI Image Format project creates and maintains the software shipping
container image format spec (OCI Image Format).")
    (license license:asl2.0)))
(define-public go-github-com-opencontainers-runtime-spec-1.0.3-0.20210326190908-1c3f411f0417
  (package
    (name "go-github-com-opencontainers-runtime-spec")
    (version "1.0.3-0.20210326190908-1c3f411f0417")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/opencontainers/runtime-spec")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1azk8z41ga8af35c966f06vrq9y9hwz6mkm59r3py5qaamzim3qq"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/opencontainers/runtime-spec"))
    (home-page "https://github.com/opencontainers/runtime-spec")
    (synopsis "Open Container Initiative Runtime Specification")
    (description
      "The @url{https://www.opencontainers.org,Open Container Initiative} develops
specifications for standards on Operating System process and application
containers.")
    (license license:asl2.0)))
(define-public go-github-com-opencontainers-runtime-tools-0.9.1-0.20210326182921-59cdde06764b
  (package
    (name "go-github-com-opencontainers-runtime-tools")
    (version "0.9.1-0.20210326182921-59cdde06764b")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/opencontainers/runtime-tools")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0f4zy6x1rrrg10bphwijic30m3r4mq934zbavnci6jyi41l1w2r0"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/opencontainers/runtime-tools"))
    (home-page "https://github.com/opencontainers/runtime-tools")
    (synopsis "oci-runtime-tool")
    (description
      "oci-runtime-tool is a collection of tools for working with the
@url{https://github.com/opencontainers/runtime-spec,OCI runtime specification}.
To build from source code, runtime-tools requires Go 1.10.x or above.")
    (license license:asl2.0)))
(define-public go-github-com-opencontainers-selinux-1.10.0
  (package
    (name "go-github-com-opencontainers-selinux")
    (version "1.10.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/opencontainers/selinux")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "118q1d9py9zhym4jmyiivasmllkdnkp1zyx4brc6n2cfmxfph6gc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/opencontainers/selinux"))
    (propagated-inputs
      `(("go-golang-org-x-sys-0.0.0-20191115151921-52ab43148777"
         ,go-golang-org-x-sys-0.0.0-20191115151921-52ab43148777)))
    (home-page "https://github.com/opencontainers/selinux")
    (synopsis "selinux")
    (description "Common SELinux package used across the container ecosystem.")
    (license license:asl2.0)))
(define-public go-github-com-opencontainers-umoci-0.4.7
  (package
    (name "go-github-com-opencontainers-umoci")
    (version "0.4.7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/opencontainers/umoci")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0in8kyi4jprvbm3zsl3risbjj8b0ma62yl3rq8rcvcgypx0mn7d4"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/opencontainers/umoci"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3-3.0.0-20200615113413-eeeca48fe776"
         ,go-gopkg-in-yaml-v3-3.0.0-20200615113413-eeeca48fe776)
        ("go-gopkg-in-check-v1-1.0.0-20200227125254-8fa46927fb4f"
         ,go-gopkg-in-check-v1-1.0.0-20200227125254-8fa46927fb4f)
        ("go-google-golang-org-protobuf-1.24.0"
         ,go-google-golang-org-protobuf-1.24.0)
        ("go-golang-org-x-sys-0.0.0-20200622214017-ed371f2e16b4"
         ,go-golang-org-x-sys-0.0.0-20200622214017-ed371f2e16b4)
        ("go-golang-org-x-crypto-0.0.0-20200604202706-70a84ac30bf9"
         ,go-golang-org-x-crypto-0.0.0-20200604202706-70a84ac30bf9)
        ("go-github-com-vbatts-go-mtree-0.5.0"
         ,go-github-com-vbatts-go-mtree-0.5.0)
        ("go-github-com-urfave-cli-1.22.4" ,go-github-com-urfave-cli-1.22.4)
        ("go-github-com-tj-assert-0.0.3" ,go-github-com-tj-assert-0.0.3)
        ("go-github-com-stretchr-testify-1.6.1"
         ,go-github-com-stretchr-testify-1.6.1)
        ("go-github-com-sirupsen-logrus-1.6.0"
         ,go-github-com-sirupsen-logrus-1.6.0)
        ("go-github-com-rootless-containers-proto-0.1.0"
         ,go-github-com-rootless-containers-proto-0.1.0)
        ("go-github-com-pkg-errors-0.9.1" ,go-github-com-pkg-errors-0.9.1)
        ("go-github-com-opencontainers-runtime-spec-1.0.2"
         ,go-github-com-opencontainers-runtime-spec-1.0.2)
        ("go-github-com-opencontainers-runc-1.0.0-rc90"
         ,go-github-com-opencontainers-runc-1.0.0-rc90)
        ("go-github-com-opencontainers-image-spec-1.0.1"
         ,go-github-com-opencontainers-image-spec-1.0.1)
        ("go-github-com-opencontainers-go-digest-1.0.0"
         ,go-github-com-opencontainers-go-digest-1.0.0)
        ("go-github-com-niemeyer-pretty-0.0.0-20200227124842-a10e7caefd8e"
         ,go-github-com-niemeyer-pretty-0.0.0-20200227124842-a10e7caefd8e)
        ("go-github-com-mohae-deepcopy-0.0.0-20170929034955-c48cc78d4826"
         ,go-github-com-mohae-deepcopy-0.0.0-20170929034955-c48cc78d4826)
        ("go-github-com-mattn-go-colorable-0.1.6"
         ,go-github-com-mattn-go-colorable-0.1.6)
        ("go-github-com-kr-text-0.2.0" ,go-github-com-kr-text-0.2.0)
        ("go-github-com-klauspost-pgzip-1.2.4"
         ,go-github-com-klauspost-pgzip-1.2.4)
        ("go-github-com-klauspost-compress-1.11.3"
         ,go-github-com-klauspost-compress-1.11.3)
        ("go-github-com-google-go-cmp-0.5.0"
         ,go-github-com-google-go-cmp-0.5.0)
        ("go-github-com-golang-protobuf-1.4.2"
         ,go-github-com-golang-protobuf-1.4.2)
        ("go-github-com-docker-go-units-0.4.0"
         ,go-github-com-docker-go-units-0.4.0)
        ("go-github-com-cyphar-filepath-securejoin-0.2.2"
         ,go-github-com-cyphar-filepath-securejoin-0.2.2)
        ("go-github-com-cpuguy83-go-md2man-v2-2.0.0"
         ,go-github-com-cpuguy83-go-md2man-v2-2.0.0)
        ("go-github-com-apex-log-1.4.0" ,go-github-com-apex-log-1.4.0)
        ("go-github-com-adamkorcz-go-fuzz-headers-0.0.0-20210312213058-32f4d319f0d2"
         ,go-github-com-adamkorcz-go-fuzz-headers-0.0.0-20210312213058-32f4d319f0d2)))
    (home-page "https://github.com/opencontainers/umoci")
    (synopsis "Install")
    (description
      "@strong{u}moci @strong{m}odifies @strong{O}pen @strong{C}ontainer
@strong{i}mages.")
    (license license:asl2.0)))
(define-public go-github-com-pelletier-go-toml-1.9.4
  (package
    (name "go-github-com-pelletier-go-toml")
    (version "1.9.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/pelletier/go-toml")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0f2d9m19dadl18i2baf57xygvn9vprm6wb8chvpx8kipx94nchyl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/pelletier/go-toml"))
    (home-page "https://github.com/pelletier/go-toml")
    (synopsis "go-toml")
    (description "Package toml is a TOML parser and manipulation library.")
    (license license:expat)))
(define-public go-github-com-pkg-errors-0.9.1
  (package
    (name "go-github-com-pkg-errors")
    (version "0.9.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/pkg/errors")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1761pybhc2kqr6v5fm8faj08x9bql8427yqg6vnfv6nhrasx1mwq"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/pkg/errors"))
    (home-page "https://github.com/pkg/errors")
    (synopsis "errors")
    (description "Package errors provides simple error handling primitives.")
    (license license:bsd-2)))
(define-public go-github-com-seccomp-containers-golang-0.6.0
  (package
    (name "go-github-com-seccomp-containers-golang")
    (version "0.6.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/seccomp/containers-golang")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0clrwdnmr131a65h66br57bsnr17i1s3kq9cc7ljrdaym57y6cfz"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/seccomp/containers-golang"))
    (propagated-inputs
      `(("go-golang-org-x-sys-0.0.0-20200720211630-cb9d2d5c5666"
         ,go-golang-org-x-sys-0.0.0-20200720211630-cb9d2d5c5666)
        ("go-github-com-xeipuuv-gojsonschema-1.2.0"
         ,go-github-com-xeipuuv-gojsonschema-1.2.0)
        ("go-github-com-syndtr-gocapability-0.0.0-20180916011248-d98352740cb2"
         ,go-github-com-syndtr-gocapability-0.0.0-20180916011248-d98352740cb2)
        ("go-github-com-sirupsen-logrus-1.6.0"
         ,go-github-com-sirupsen-logrus-1.6.0)
        ("go-github-com-seccomp-libseccomp-golang-0.9.1"
         ,go-github-com-seccomp-libseccomp-golang-0.9.1)
        ("go-github-com-opencontainers-selinux-1.6.0"
         ,go-github-com-opencontainers-selinux-1.6.0)
        ("go-github-com-opencontainers-runtime-tools-0.9.0"
         ,go-github-com-opencontainers-runtime-tools-0.9.0)
        ("go-github-com-opencontainers-runtime-spec-1.0.3-0.20200710190001-3e4195d92445"
         ,go-github-com-opencontainers-runtime-spec-1.0.3-0.20200710190001-3e4195d92445)
        ("go-github-com-hashicorp-go-multierror-1.1.0"
         ,go-github-com-hashicorp-go-multierror-1.1.0)
        ("go-github-com-blang-semver-3.5.1+incompatible"
         ,go-github-com-blang-semver-3.5.1+incompatible)))
    (home-page "https://github.com/seccomp/containers-golang")
    (synopsis "containers-golang")
    (description
      "@code{containers-golang} is a set of Go libraries used by container runtimes to
generate and load seccomp mappings into the kernel.")
    (license license:asl2.0)))
(define-public go-github-com-seccomp-libseccomp-golang-0.9.2-0.20210429002308-3879420cc921
  (package
    (name "go-github-com-seccomp-libseccomp-golang")
    (version "0.9.2-0.20210429002308-3879420cc921")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/seccomp/libseccomp-golang")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ypr3j50cd9lnj9szfyh45kh0cf7swm7cv2svz1575mldd9m1xr9"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/seccomp/libseccomp-golang"))
    (home-page "https://github.com/seccomp/libseccomp-golang")
    (synopsis #f)
    (description
      "Package seccomp provides bindings for libseccomp, a library wrapping the Linux
seccomp syscall.  Seccomp enables an application to restrict system call use for
itself and its children.")
    (license license:bsd-2)))
(define-public go-github-com-spf13-cobra-1.3.0
  (package
    (name "go-github-com-spf13-cobra")
    (version "1.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/spf13/cobra")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0j3kj6yxrl2aigixapjl6bi2gmghrj52763wbd7jc079f38wz94n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/spf13/cobra"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2-2.4.0" ,go-gopkg-in-yaml-v2-2.4.0)
        ("go-github-com-spf13-viper-1.10.0" ,go-github-com-spf13-viper-1.10.0)
        ("go-github-com-spf13-pflag-1.0.5" ,go-github-com-spf13-pflag-1.0.5)
        ("go-github-com-inconshreveable-mousetrap-1.0.0"
         ,go-github-com-inconshreveable-mousetrap-1.0.0)
        ("go-github-com-cpuguy83-go-md2man-v2-2.0.1"
         ,go-github-com-cpuguy83-go-md2man-v2-2.0.1)))
    (home-page "https://github.com/spf13/cobra")
    (synopsis "Overview")
    (description
      "Package cobra is a commander providing a simple interface to create powerful
modern CLI interfaces.  In addition to providing an interface, Cobra
simultaneously provides a controller to organize your application code.")
    (license license:asl2.0)))
(define-public go-github-com-spf13-pflag-1.0.5
  (package
    (name "go-github-com-spf13-pflag")
    (version "1.0.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/spf13/pflag")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0gpmacngd0gpslnbkzi263f5ishigzgh6pbdv9hp092rnjl4nd31"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/spf13/pflag"))
    (home-page "https://github.com/spf13/pflag")
    (synopsis "Description")
    (description
      "Package pflag is a drop-in replacement for Go's flag package, implementing
POSIX/GNU-style --flags.")
    (license license:bsd-3)))
(define-public go-github-com-sylabs-json-resp-0.8.0
  (package
    (name "go-github-com-sylabs-json-resp")
    (version "0.8.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/sylabs/json-resp")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ygr4mavaj1pp78hkkphhn3ydd923n3klwnx8x7v4ajqzx4cx4id"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/sylabs/json-resp"))
    (home-page "https://github.com/sylabs/json-resp")
    (synopsis "JSON Response")
    (description
      "The @code{json-resp} package contains a small set of functions that are used to
marshall and unmarshall response data and errors in JSON format.")
    (license license:bsd-3)))
(define-public go-github-com-urfave-cli-1.22.5
  (package
    (name "go-github-com-urfave-cli")
    (version "1.22.5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/urfave/cli")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1n8lk58j7bdsp47yh5bcpsw38lhkbyzcy3203ffkawc2rvkf1pp7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/urfave/cli"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v2-2.2.2" ,go-gopkg-in-yaml-v2-2.2.2)
        ("go-github-com-cpuguy83-go-md2man-v2-2.0.0-20190314233015-f79a8a8ca69d"
         ,go-github-com-cpuguy83-go-md2man-v2-2.0.0-20190314233015-f79a8a8ca69d)
        ("go-github-com-burntsushi-toml-0.3.1"
         ,go-github-com-burntsushi-toml-0.3.1)))
    (home-page "https://github.com/urfave/cli")
    (synopsis "cli")
    (description
      "Package cli provides a minimal framework for creating and organizing command
line Go applications.  cli is designed to be easy to understand and write, the
most simple cli application can be written as follows:")
    (license license:expat)))
(define-public go-github-com-vbauerster-mpb-v7-7.4.1
  (package
    (name "go-github-com-vbauerster-mpb-v7")
    (version "7.4.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/vbauerster/mpb")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ii1r84qjfp2spvj6nb8xy5b3nsfw17jmwgp55zkaz9n9lfj75w2"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/vbauerster/mpb/v7"))
    (propagated-inputs
      `(("go-golang-org-x-sys-0.0.0-20220209214540-3681064d5158"
         ,go-golang-org-x-sys-0.0.0-20220209214540-3681064d5158)
        ("go-github-com-mattn-go-runewidth-0.0.13"
         ,go-github-com-mattn-go-runewidth-0.0.13)
        ("go-github-com-acarl005-stripansi-0.0.0-20180116102854-5a71ef0e047d"
         ,go-github-com-acarl005-stripansi-0.0.0-20180116102854-5a71ef0e047d)
        ("go-github-com-vividcortex-ewma-1.2.0"
         ,go-github-com-vividcortex-ewma-1.2.0)))
    (home-page "https://github.com/vbauerster/mpb")
    (synopsis "Multi Progress Bar")
    (description
      "Package mpb is a library for rendering progress bars in terminal applications.")
    (license license:unlicense)))
(define-public go-github-com-xeipuuv-gojsonpointer-0.0.0-20190905194746-02993c407bfb
  (package
    (name "go-github-com-xeipuuv-gojsonpointer")
    (version "0.0.0-20190905194746-02993c407bfb")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/xeipuuv/gojsonpointer")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0y7gmpgsm8c12ax4a0ij9srmd9d424iq224n172ckwfqf37amvzy"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/xeipuuv/gojsonpointer"))
    (home-page "https://github.com/xeipuuv/gojsonpointer")
    (synopsis "gojsonpointer")
    (description "An implementation of JSON Pointer - Go language")
    (license license:asl2.0)))
(define-public go-github-com-yvasiyarov-go-metrics-0.0.0-20150112132944-c25f46c4b940
  (package
    (name "go-github-com-yvasiyarov-go-metrics")
    (version "0.0.0-20150112132944-c25f46c4b940")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/yvasiyarov/go-metrics")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0p1hx4ml4gzgyj8n0fg5lv8bq092zwg00n59lq8v8sphjc1h8i8d"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/yvasiyarov/go-metrics"))
    (home-page "https://github.com/yvasiyarov/go-metrics")
    (synopsis "go-metrics")
    (description "Go port of Coda Hale's Metrics library")
    (license license:expat)))
(define-public go-github-com-yvasiyarov-newrelic-platform-go-0.0.0-20160601141957-9c099fbc30e9
  (package
    (name "go-github-com-yvasiyarov-newrelic-platform-go")
    (version "0.0.0-20160601141957-9c099fbc30e9")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/yvasiyarov/newrelic_platform_go")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0n6jap27qc7b7c37ck1gv9biq5nfl7y06jk0cqgc517yfnb4f07x"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/yvasiyarov/newrelic_platform_go"))
    (home-page "https://github.com/yvasiyarov/newrelic_platform_go")
    (synopsis "New Relic Platform Agent SDK for Go(golang)")
    (description
      "Package newrelic_platform_go is New Relic Platform Agent SDK for Go language.")
    (license license:bsd-2)))
(define-public go-golang-org-x-sys-0.0.0-20220209214540-3681064d5158
  (package
    (name "go-golang-org-x-sys")
    (version "0.0.0-20220209214540-3681064d5158")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/sys")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0cw9rzb4rxmr2si28pjqx3wv5r119c83kd08yy1h7nf725bzrs89"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/sys"))
    (home-page "https://golang.org/x/sys")
    (synopsis "sys")
    (description
      "This repository holds supplemental Go packages for low-level interactions with
the operating system.")
    (license license:bsd-3)))
(define-public go-golang-org-x-term-0.0.0-20210916214954-140adaaadfaf
  (package
    (name "go-golang-org-x-term")
    (version "0.0.0-20210916214954-140adaaadfaf")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/term")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1rcdx2q92pzpmdyzdlzpq9flc3ibfi3m1rni7w4qlmlq569s6wh3"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/term"))
    (propagated-inputs
      `(("go-golang-org-x-sys-0.0.0-20210615035016-665e8c7367d1"
         ,go-golang-org-x-sys-0.0.0-20210615035016-665e8c7367d1)))
    (home-page "https://golang.org/x/term")
    (synopsis "Go terminal/console support")
    (description
      "Package term provides support functions for dealing with terminals, as commonly
found on UNIX systems.")
    (license license:bsd-3)))
(define-public go-gopkg-in-yaml-v2-2.4.0
  (package
    (name "go-gopkg-in-yaml-v2")
    (version "2.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/yaml.v2")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1pbmrpj7gcws34g8vwna4i2nhm9p6235piww36436xhyaa10cldr"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "gopkg.in/yaml.v2" #:unpack-path "gopkg.in/yaml.v2"))
    (propagated-inputs
      `(("go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405"
         ,go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405)))
    (home-page "https://gopkg.in/yaml.v2")
    (synopsis "YAML support for the Go language")
    (description "Package yaml implements YAML support for the Go language.")
    (license license:asl2.0)))
(define-public go-gotest-tools-v3-3.1.0
  (package
    (name "go-gotest-tools-v3")
    (version "3.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/gotestyourself/gotest.tools")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0jk2grk2q19k453m76hn2bsq9i73yk63drlfjg1imvhmmcl88872"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "gotest.tools/v3" #:unpack-path "gotest.tools/v3"))
    (propagated-inputs
      `(("go-golang-org-x-tools-0.1.0" ,go-golang-org-x-tools-0.1.0)
        ("go-golang-org-x-sys-0.0.0-20210119212857-b64e53b001e4"
         ,go-golang-org-x-sys-0.0.0-20210119212857-b64e53b001e4)
        ("go-github-com-spf13-pflag-1.0.3" ,go-github-com-spf13-pflag-1.0.3)
        ("go-github-com-pkg-errors-0.9.1" ,go-github-com-pkg-errors-0.9.1)
        ("go-github-com-google-go-cmp-0.5.5"
         ,go-github-com-google-go-cmp-0.5.5)))
    (home-page "https://gotest.tools/v3")
    (synopsis "gotest.tools")
    (description
      "Package gotesttools is a collection of packages to augment `testing` and support
common patterns.")
    (license license:asl2.0)))
(define-public go-mvdan-cc-sh-v3-3.4.3-0.20220202175809-113ed667a8a7
  (package
    (name "go-mvdan-cc-sh-v3")
    (version "3.4.3-0.20220202175809-113ed667a8a7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/mvdan/sh")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1k5113qqm20x58838k8f0ry3v1hinahjf2yl8y4450922yy0mq2k"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "mvdan.cc/sh/v3" #:unpack-path "mvdan.cc/sh/v3"))
    (propagated-inputs
      `(("go-mvdan-cc-editorconfig-0.2.0" ,go-mvdan-cc-editorconfig-0.2.0)
        ("go-golang-org-x-term-0.0.0-20210916214954-140adaaadfaf"
         ,go-golang-org-x-term-0.0.0-20210916214954-140adaaadfaf)
        ("go-golang-org-x-sys-0.0.0-20210925032602-92d5a993a665"
         ,go-golang-org-x-sys-0.0.0-20210925032602-92d5a993a665)
        ("go-golang-org-x-sync-0.0.0-20210220032951-036812b2e83c"
         ,go-golang-org-x-sync-0.0.0-20210220032951-036812b2e83c)
        ("go-github-com-rogpeppe-go-internal-1.8.1-0.20210923151022-86f73c517451"
         ,go-github-com-rogpeppe-go-internal-1.8.1-0.20210923151022-86f73c517451)
        ("go-github-com-pkg-diff-0.0.0-20210226163009-20ebb0f2a09e"
         ,go-github-com-pkg-diff-0.0.0-20210226163009-20ebb0f2a09e)
        ("go-github-com-kr-pretty-0.3.0" ,go-github-com-kr-pretty-0.3.0)
        ("go-github-com-google-renameio-1.0.1"
         ,go-github-com-google-renameio-1.0.1)
        ("go-github-com-frankban-quicktest-1.13.1"
         ,go-github-com-frankban-quicktest-1.13.1)
        ("go-github-com-creack-pty-1.1.15" ,go-github-com-creack-pty-1.1.15)))
    (home-page "https://mvdan.cc/sh/v3")
    (synopsis "sh")
    (description
      "This package provides a shell parser, formatter, and interpreter.  Supports
@url{https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html,POSIX
Shell}, @url{https://www.gnu.org/software/bash/,Bash}, and
@url{http://www.mirbsd.org/mksh.htm,mksh}.  Requires Go 1.16 or later.")
    (license license:bsd-3)))
(define-public go-oras-land-oras-go-1.1.0
  (package
    (name "go-oras-land-oras-go")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/oras-project/oras-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1mbn57dgpr7m2f7cw5r9s8kka44khrgqca84np6wnjiyzxgr1khk"))))
    (build-system go-build-system)
    (arguments '(#:import-path "oras.land/oras-go"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3-3.0.0-20210107192922-496545a6307b"
         ,go-gopkg-in-yaml-v3-3.0.0-20210107192922-496545a6307b)
        ("go-gopkg-in-yaml-v2-2.4.0" ,go-gopkg-in-yaml-v2-2.4.0)
        ("go-gopkg-in-check-v1-1.0.0-20200227125254-8fa46927fb4f"
         ,go-gopkg-in-check-v1-1.0.0-20200227125254-8fa46927fb4f)
        ("go-google-golang-org-protobuf-1.27.1"
         ,go-google-golang-org-protobuf-1.27.1)
        ("go-google-golang-org-grpc-1.43.0" ,go-google-golang-org-grpc-1.43.0)
        ("go-google-golang-org-genproto-0.0.0-20220107163113-42d7afdf6368"
         ,go-google-golang-org-genproto-0.0.0-20220107163113-42d7afdf6368)
        ("go-golang-org-x-text-0.3.7" ,go-golang-org-x-text-0.3.7)
        ("go-golang-org-x-sys-0.0.0-20211216021012-1d35b9e2eb4e"
         ,go-golang-org-x-sys-0.0.0-20211216021012-1d35b9e2eb4e)
        ("go-golang-org-x-net-0.0.0-20220107192237-5cfca573fb4d"
         ,go-golang-org-x-net-0.0.0-20220107192237-5cfca573fb4d)
        ("go-github-com-yvasiyarov-newrelic-platform-go-0.0.0-20140908184405-b21fdbd4370f"
         ,go-github-com-yvasiyarov-newrelic-platform-go-0.0.0-20140908184405-b21fdbd4370f)
        ("go-github-com-yvasiyarov-gorelic-0.0.0-20141212073537-a9bba5b9ab50"
         ,go-github-com-yvasiyarov-gorelic-0.0.0-20141212073537-a9bba5b9ab50)
        ("go-github-com-yvasiyarov-go-metrics-0.0.0-20140926110328-57bccd1ccd43"
         ,go-github-com-yvasiyarov-go-metrics-0.0.0-20140926110328-57bccd1ccd43)
        ("go-github-com-spf13-pflag-1.0.5" ,go-github-com-spf13-pflag-1.0.5)
        ("go-github-com-prometheus-procfs-0.6.0"
         ,go-github-com-prometheus-procfs-0.6.0)
        ("go-github-com-prometheus-common-0.10.0"
         ,go-github-com-prometheus-common-0.10.0)
        ("go-github-com-prometheus-client-model-0.2.0"
         ,go-github-com-prometheus-client-model-0.2.0)
        ("go-github-com-prometheus-client-golang-1.7.1"
         ,go-github-com-prometheus-client-golang-1.7.1)
        ("go-github-com-pmezard-go-difflib-1.0.0"
         ,go-github-com-pmezard-go-difflib-1.0.0)
        ("go-github-com-niemeyer-pretty-0.0.0-20200227124842-a10e7caefd8e"
         ,go-github-com-niemeyer-pretty-0.0.0-20200227124842-a10e7caefd8e)
        ("go-github-com-morikuni-aec-1.0.0" ,go-github-com-morikuni-aec-1.0.0)
        ("go-github-com-moby-term-0.0.0-20200312100748-672ec06f55cd"
         ,go-github-com-moby-term-0.0.0-20200312100748-672ec06f55cd)
        ("go-github-com-moby-sys"
         ,go-github-com-moby-sys)
        ("go-github-com-moby-locker-1.0.1" ,go-github-com-moby-locker-1.0.1)
        ("go-github-com-matttproud-golang-protobuf-extensions-1.0.2-0.20181231171920-c182affec369"
         ,go-github-com-matttproud-golang-protobuf-extensions-1.0.2-0.20181231171920-c182affec369)
        ("go-github-com-kr-text-0.2.0" ,go-github-com-kr-text-0.2.0)
        ("go-github-com-klauspost-compress-1.13.6"
         ,go-github-com-klauspost-compress-1.13.6)
        ("go-github-com-inconshreveable-mousetrap-1.0.0"
         ,go-github-com-inconshreveable-mousetrap-1.0.0)
        ("go-github-com-gorilla-mux-1.8.0" ,go-github-com-gorilla-mux-1.8.0)
        ("go-github-com-gorilla-handlers-1.5.1"
         ,go-github-com-gorilla-handlers-1.5.1)
        ("go-github-com-google-go-cmp-0.5.6"
         ,go-github-com-google-go-cmp-0.5.6)
        ("go-github-com-gomodule-redigo-1.8.2"
         ,go-github-com-gomodule-redigo-1.8.2)
        ("go-github-com-golang-protobuf-1.5.2"
         ,go-github-com-golang-protobuf-1.5.2)
        ("go-github-com-golang-groupcache-0.0.0-20210331224755-41bb18bfe9da"
         ,go-github-com-golang-groupcache-0.0.0-20210331224755-41bb18bfe9da)
        ("go-github-com-gogo-protobuf-1.3.2"
         ,go-github-com-gogo-protobuf-1.3.2)
        ("go-github-com-felixge-httpsnoop-1.0.1"
         ,go-github-com-felixge-httpsnoop-1.0.1)
        ("go-github-com-docker-libtrust-0.0.0-20150114040149-fa567046d9b1"
         ,go-github-com-docker-libtrust-0.0.0-20150114040149-fa567046d9b1)
        ("go-github-com-docker-go-units-0.4.0"
         ,go-github-com-docker-go-units-0.4.0)
        ("go-github-com-docker-go-metrics-0.0.1"
         ,go-github-com-docker-go-metrics-0.0.1)
        ("go-github-com-docker-go-events-0.0.0-20190806004212-e31b211e4f1c"
         ,go-github-com-docker-go-events-0.0.0-20190806004212-e31b211e4f1c)
        ("go-github-com-docker-go-connections-0.4.0"
         ,go-github-com-docker-go-connections-0.4.0)
        ("go-github-com-docker-docker-credential-helpers-0.6.4"
         ,go-github-com-docker-docker-credential-helpers-0.6.4)
        ("go-github-com-docker-distribution-2.7.1+incompatible"
         ,go-github-com-docker-distribution-2.7.1+incompatible)
        ("go-github-com-davecgh-go-spew-1.1.1"
         ,go-github-com-davecgh-go-spew-1.1.1)
        ("go-github-com-containerd-cgroups-1.0.2"
         ,go-github-com-containerd-cgroups-1.0.2)
        ("go-github-com-cespare-xxhash-v2-2.1.1"
         ,go-github-com-cespare-xxhash-v2-2.1.1)
        ("go-github-com-bugsnag-panicwrap-0.0.0-20151223152923-e2c28503fcd0"
         ,go-github-com-bugsnag-panicwrap-0.0.0-20151223152923-e2c28503fcd0)
        ("go-github-com-bugsnag-osext-0.0.0-20130617224835-0dd3f918b21b"
         ,go-github-com-bugsnag-osext-0.0.0-20130617224835-0dd3f918b21b)
        ("go-github-com-bugsnag-bugsnag-go-0.0.0-20141110184014-b1d153021fcd"
         ,go-github-com-bugsnag-bugsnag-go-0.0.0-20141110184014-b1d153021fcd)
        ("go-github-com-bshuster-repo-logrus-logstash-hook-1.0.0"
         ,go-github-com-bshuster-repo-logrus-logstash-hook-1.0.0)
        ("go-github-com-beorn7-perks-1.0.1" ,go-github-com-beorn7-perks-1.0.1)
        ("go-github-com-shopify-logrus-bugsnag-0.0.0-20171204204709-577dee27f20d"
         ,go-github-com-shopify-logrus-bugsnag-0.0.0-20171204204709-577dee27f20d)
        ("go-github-com-microsoft-hcsshim-0.9.1"
         ,go-github-com-microsoft-hcsshim-0.9.1)
        ("go-github-com-microsoft-go-winio-0.5.1"
         ,go-github-com-microsoft-go-winio-0.5.1)
        ("go-github-com-azure-go-ansiterm-0.0.0-20170929234023-d6e3b3328b78"
         ,go-github-com-azure-go-ansiterm-0.0.0-20170929234023-d6e3b3328b78)
        ("go-golang-org-x-sync-0.0.0-20210220032951-036812b2e83c"
         ,go-golang-org-x-sync-0.0.0-20210220032951-036812b2e83c)
        ("go-golang-org-x-crypto-0.0.0-20211117183948-ae814b36b871"
         ,go-golang-org-x-crypto-0.0.0-20211117183948-ae814b36b871)
        ("go-github-com-stretchr-testify-1.7.0"
         ,go-github-com-stretchr-testify-1.7.0)
        ("go-github-com-spf13-cobra-1.2.1" ,go-github-com-spf13-cobra-1.2.1)
        ("go-github-com-sirupsen-logrus-1.8.1"
         ,go-github-com-sirupsen-logrus-1.8.1)
        ("go-github-com-pkg-errors-0.9.1" ,go-github-com-pkg-errors-0.9.1)
        ("go-github-com-phayes-freeport-0.0.0-20180830031419-95f893ade6f2"
         ,go-github-com-phayes-freeport-0.0.0-20180830031419-95f893ade6f2)
        ("go-github-com-opencontainers-image-spec-1.0.2"
         ,go-github-com-opencontainers-image-spec-1.0.2)
        ("go-github-com-opencontainers-go-digest-1.0.0"
         ,go-github-com-opencontainers-go-digest-1.0.0)
        ("go-github-com-docker-docker-20.10.11+incompatible"
         ,go-github-com-docker-docker-20.10.11+incompatible)
        ("go-github-com-docker-cli-20.10.11+incompatible"
         ,go-github-com-docker-cli-20.10.11+incompatible)
        ("go-github-com-distribution-distribution-v3-3.0.0-20211118083504-a29a3c99a684"
         ,go-github-com-distribution-distribution-v3-3.0.0-20211118083504-a29a3c99a684)
        ("go-github-com-containerd-containerd-1.5.9"
         ,go-github-com-containerd-containerd-1.5.9)))
    (home-page "https://oras.land/oras-go")
    (synopsis "ORAS Go library")
    (description
      "Documentation for the ORAS Go library is located on the project website:
@url{https://oras.land/client_libraries/go/,oras.land/client_libraries/go}")
    (license license:asl2.0)))
(define-public go-github-com-opencontainers-image-spec-1.0.2-0.20211117181255-693428a734f5
  (package
    (name "go-github-com-opencontainers-image-spec")
    (version "1.0.2-0.20211117181255-693428a734f5")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/opencontainers/image-spec")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1w9fmamky30idjbqs78qhds6d5b9gdy6wzcxx6fhmpm2159dv62b"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/opencontainers/image-spec"))
    (home-page "https://github.com/opencontainers/image-spec")
    (synopsis "OCI Image Format Specification")
    (description
      "The OCI Image Format project creates and maintains the software shipping
container image format spec (OCI Image Format).")
    (license license:asl2.0)))
(define-public go-github-com-moby-sys-0.0.0-20220308220145-03355939d693
  (package
    (name "go-github-com-moby-sys")
    (version "0.0.0-20220308220145-03355939d693")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/moby/sys")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ns4bav4n0k93nwz898dmcrz9zm4fa08sxyzhi7wqj4f8mpz4fam"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/moby/sys"))
    (home-page "https://github.com/moby/sys")
    (synopsis #f)
    (description #f)
    (license license:asl2.0)))
(define-public go-github-com-yvasiyarov-gorelic-0.0.6
  (package
    (name "go-github-com-yvasiyarov-gorelic")
    (version "0.0.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/yvasiyarov/gorelic")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0210d8z7l945611b5ynzjhnypqfdyr490ndrap13s4k5z1jgqjnb"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/yvasiyarov/gorelic"))
    (home-page "https://github.com/yvasiyarov/gorelic")
    (synopsis "GoRelic is deprecated in favour of")
    (description
      "Package gorelic is an New Relic agent implementation for Go runtime.  It collect
a lot of metrics about Go scheduler, garbage collector and memory allocator and
send them to NewRelic.")
    (license license:bsd-2)))
(define-public go-github-com-stretchr-testify-1.6.1
  (package
    (name "go-github-com-stretchr-testify")
    (version "1.6.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/stretchr/testify")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1yhiqqzjvi63pf01rgzx68gqkkvjx03fvl5wk30br5l6s81s090l"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/stretchr/testify"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3-3.0.0-20200313102051-9f266ea9e77c"
         ,go-gopkg-in-yaml-v3-3.0.0-20200313102051-9f266ea9e77c)
        ("go-github-com-stretchr-objx-0.1.0"
         ,go-github-com-stretchr-objx-0.1.0)
        ("go-github-com-pmezard-go-difflib-1.0.0"
         ,go-github-com-pmezard-go-difflib-1.0.0)
        ("go-github-com-davecgh-go-spew-1.1.0"
         ,go-github-com-davecgh-go-spew-1.1.0)))
    (home-page "https://github.com/stretchr/testify")
    (synopsis "Testify - Thou Shalt Write Tests")
    (description
      "** We are working on testify v2 and would love to hear what you'd like to see in
it, have your say here: @url{https://cutt.ly/testify,https://cutt.ly/testify} **
Package testify is a set of packages that provide many tools for testifying that
your code will behave as you intend.")
    (license license:expat)))
(define-public go-github-com-stretchr-testify-1.7.0
  (package
    (name "go-github-com-stretchr-testify")
    (version "1.7.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/stretchr/testify")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0ixgjsvafr3513pz3r6pmgk074s2dxkll0dadvl25gkf30rkmh10"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/stretchr/testify"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3-3.0.0-20200313102051-9f266ea9e77c"
         ,go-gopkg-in-yaml-v3-3.0.0-20200313102051-9f266ea9e77c)
        ("go-github-com-stretchr-objx-0.1.0"
         ,go-github-com-stretchr-objx-0.1.0)
        ("go-github-com-pmezard-go-difflib-1.0.0"
         ,go-github-com-pmezard-go-difflib-1.0.0)
        ("go-github-com-davecgh-go-spew-1.1.0"
         ,go-github-com-davecgh-go-spew-1.1.0)))
    (home-page "https://github.com/stretchr/testify")
    (synopsis "Testify - Thou Shalt Write Tests")
    (description
      "** We are working on testify v2 and would love to hear what you'd like to see in
it, have your say here: @url{https://cutt.ly/testify,https://cutt.ly/testify} **
Package testify is a set of packages that provide many tools for testifying that
your code will behave as you intend.")
    (license license:expat)))
(define-public go-gopkg-in-yaml-v3-3.0.0-20200313102051-9f266ea9e77c
  (package
    (name "go-gopkg-in-yaml-v3")
    (version "3.0.0-20200313102051-9f266ea9e77c")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/yaml.v3")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1bbai3lzb50m0x2vwsdbagrbhvfylj9k1m32hgbqwldqx4p9ay35"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "gopkg.in/yaml.v3" #:unpack-path "gopkg.in/yaml.v3"))
    (propagated-inputs
      `(("go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405"
         ,go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405)))
    (home-page "https://gopkg.in/yaml.v3")
    (synopsis "YAML support for the Go language")
    (description "Package yaml implements YAML support for the Go language.")
    (license license:expat)))
(define-public go-github-com-stretchr-objx-0.1.0
  (package
    (name "go-github-com-stretchr-objx")
    (version "0.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/stretchr/objx")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19ynspzjdynbi85xw06mh8ad5j0qa1vryvxjgvbnyrr8rbm4vd8w"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/stretchr/objx"))
    (home-page "https://github.com/stretchr/objx")
    (synopsis "Objx")
    (description
      "Objx - Go package for dealing with maps, slices, JSON and other data.")
    (license license:expat)))
(define-public go-github-com-pmezard-go-difflib-1.0.0
  (package
    (name "go-github-com-pmezard-go-difflib")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/pmezard/go-difflib")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0c1cn55m4rypmscgf0rrb88pn58j3ysvc2d0432dp3c6fqg6cnzw"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/pmezard/go-difflib"))
    (home-page "https://github.com/pmezard/go-difflib")
    (synopsis "go-difflib")
    (description
      "Go-difflib is a partial port of python 3 difflib package.  Its main goal was to
make unified and context diff available in pure Go, mostly for testing purposes.")
    (license license:bsd-3)))
(define-public go-github-com-davecgh-go-spew-1.1.0
  (package
    (name "go-github-com-davecgh-go-spew")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/davecgh/go-spew")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0d4jfmak5p6lb7n2r6yvf5p1zcw0l8j74kn55ghvr7zr7b7axm6c"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/davecgh/go-spew"))
    (home-page "https://github.com/davecgh/go-spew")
    (synopsis "go-spew")
    (description
      "Go-spew implements a deep pretty printer for Go data structures to aid in
debugging.  A comprehensive suite of tests with 100% test coverage is provided
to ensure proper functionality.  See @code{test_coverage.txt} for the gocov
coverage report.  Go-spew is licensed under the liberal ISC license, so it may
be used in open source or commercial projects.")
    (license license:isc)))
(define-public go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405
  (package
    (name "go-gopkg-in-check-v1")
    (version "0.0.0-20161208181325-20d25e280405")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/check.v1")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0k1m83ji9l1a7ng8a7v40psbymxasmssbrrhpdv2wl4rhs0nc3np"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "gopkg.in/check.v1" #:unpack-path "gopkg.in/check.v1"))
    (home-page "https://gopkg.in/check.v1")
    (synopsis "Instructions")
    (description
      "Package check is a rich testing extension for Go's testing package.")
    (license license:bsd-2)))
(define-public go-golang-org-x-crypto-0.0.0-20210322153248-0c34fe9e7dc2
  (package
    (name "go-golang-org-x-crypto")
    (version "0.0.0-20210322153248-0c34fe9e7dc2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/crypto")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "021szs1czc0xg4chpays7i05dvkax9s8mns314ab01r887kchqxq"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/crypto"))
    (propagated-inputs
      `(("go-golang-org-x-term-0.0.0-20201126162022-7de9c90e9dd1"
         ,go-golang-org-x-term-0.0.0-20201126162022-7de9c90e9dd1)
        ("go-golang-org-x-sys-0.0.0-20201119102817-f84b799fce68"
         ,go-golang-org-x-sys-0.0.0-20201119102817-f84b799fce68)
        ("go-golang-org-x-net-0.0.0-20210226172049-e18ecbb05110"
         ,go-golang-org-x-net-0.0.0-20210226172049-e18ecbb05110)))
    (home-page "https://golang.org/x/crypto")
    (synopsis "Go Cryptography")
    (description
      "This repository holds supplementary Go cryptography libraries.")
    (license license:bsd-3)))
(define-public go-golang-org-x-term-0.0.0-20201126162022-7de9c90e9dd1
  (package
    (name "go-golang-org-x-term")
    (version "0.0.0-20201126162022-7de9c90e9dd1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/term")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ba252xmv6qsvf1w1gcy98mngrj0vd4inbjw0lsklqvva65nljna"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/term"))
    (propagated-inputs
      `(("go-golang-org-x-sys-0.0.0-20201119102817-f84b799fce68"
         ,go-golang-org-x-sys-0.0.0-20201119102817-f84b799fce68)))
    (home-page "https://golang.org/x/term")
    (synopsis "Go terminal/console support")
    (description
      "Package term provides support functions for dealing with terminals, as commonly
found on UNIX systems.")
    (license license:bsd-3)))
(define-public go-golang-org-x-sys-0.0.0-20201119102817-f84b799fce68
  (package
    (name "go-golang-org-x-sys")
    (version "0.0.0-20201119102817-f84b799fce68")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/sys")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1dvhqian5byzkcg1bnqzygqa6ccc6krc2q7j12pp1vhl3y7znnsg"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/sys"))
    (home-page "https://golang.org/x/sys")
    (synopsis "sys")
    (description
      "This repository holds supplemental Go packages for low-level interactions with
the operating system.")
    (license license:bsd-3)))
(define-public go-golang-org-x-net-0.0.0-20210226172049-e18ecbb05110
  (package
    (name "go-golang-org-x-net")
    (version "0.0.0-20210226172049-e18ecbb05110")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/net")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1vlq8mdscp7yfaa1lmyv03y5m4c2d67ydg2q1i6smkrxghn3zn3q"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/net"))
    (propagated-inputs
      `(("go-golang-org-x-text-0.3.3" ,go-golang-org-x-text-0.3.3)
        ("go-golang-org-x-term-0.0.0-20201126162022-7de9c90e9dd1"
         ,go-golang-org-x-term-0.0.0-20201126162022-7de9c90e9dd1)
        ("go-golang-org-x-sys-0.0.0-20201119102817-f84b799fce68"
         ,go-golang-org-x-sys-0.0.0-20201119102817-f84b799fce68)))
    (home-page "https://golang.org/x/net")
    (synopsis "Go Networking")
    (description
      "This repository holds supplementary Go networking libraries.")
    (license license:bsd-3)))
(define-public go-golang-org-x-text-0.3.3
  (package
    (name "go-golang-org-x-text")
    (version "0.3.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/text")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "19pihqm3phyndmiw6i42pdv6z1rbvlqlsnhsyqf9gsnn0qnmqqlh"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/text"))
    (propagated-inputs
      `(("go-golang-org-x-tools-0.0.0-20180917221912-90fa682c2a6e"
         ,go-golang-org-x-tools-0.0.0-20180917221912-90fa682c2a6e)))
    (home-page "https://golang.org/x/text")
    (synopsis "Go Text")
    (description
      "text is a repository of text-related packages related to internationalization
(i18n) and localization (l10n), such as character encodings, text
transformations, and locale-specific text handling.")
    (license license:bsd-3)))
(define-public go-golang-org-x-tools-0.0.0-20180917221912-90fa682c2a6e
  (package
    (name "go-golang-org-x-tools")
    (version "0.0.0-20180917221912-90fa682c2a6e")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/tools")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "03ic2xsy51jw9749wl7gszdbz99iijbd2bckgygl6cm9w5m364ak"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/tools"))
    (home-page "https://golang.org/x/tools")
    (synopsis "Go Tools")
    (description
      "This subrepository holds the source for various packages and tools that support
the Go programming language.")
    (license license:bsd-3)))
(define-public go-github-com-sirupsen-logrus-1.4.2
  (package
    (name "go-github-com-sirupsen-logrus")
    (version "1.4.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/sirupsen/logrus")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "087k2lxrr9p9dh68yw71d05h5g9p5v26zbwd6j7lghinjfaw334x"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/sirupsen/logrus"))
    (propagated-inputs
      `(("go-golang-org-x-sys-0.0.0-20190422165155-953cdadca894"
         ,go-golang-org-x-sys-0.0.0-20190422165155-953cdadca894)
        ("go-github-com-stretchr-testify-1.2.2"
         ,go-github-com-stretchr-testify-1.2.2)
        ("go-github-com-stretchr-objx-0.1.1"
         ,go-github-com-stretchr-objx-0.1.1)
        ("go-github-com-pmezard-go-difflib-1.0.0"
         ,go-github-com-pmezard-go-difflib-1.0.0)
        ("go-github-com-konsorten-go-windows-terminal-sequences-1.0.1"
         ,go-github-com-konsorten-go-windows-terminal-sequences-1.0.1)
        ("go-github-com-davecgh-go-spew-1.1.1"
         ,go-github-com-davecgh-go-spew-1.1.1)))
    (home-page "https://github.com/sirupsen/logrus")
    (synopsis "Logrus")
    (description
      "Package logrus is a structured logger for Go, completely API compatible with the
standard library logger.")
    (license license:expat)))
(define-public go-github-com-gorilla-mux-1.7.4
  (package
    (name "go-github-com-gorilla-mux")
    (version "1.7.4")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/gorilla/mux")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1d0sy1paa055ic84sp3766s9pa24q008hf77dc842vrgvn8p3wmh"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/gorilla/mux"))
    (home-page "https://github.com/gorilla/mux")
    (synopsis "gorilla/mux")
    (description "Package mux implements a request router and dispatcher.")
    (license license:bsd-3)))
(define-public go-github-com-docker-libtrust-0.0.0-20160708172513-aabc10ec26b7
  (package
    (name "go-github-com-docker-libtrust")
    (version "0.0.0-20160708172513-aabc10ec26b7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/docker/libtrust")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1lwslbggzc2b0c4wxl5pn6i2nfgz5jz8f7s7vnid9mrlsk59h7s1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/docker/libtrust"))
    (home-page "https://github.com/docker/libtrust")
    (synopsis "libtrust")
    (description
      "Package libtrust provides an interface for managing authentication and
authorization using public key cryptography.  Authentication is handled using
the identity attached to the public key and verified through TLS x509
certificates, a key challenge, or signature.  Authorization and access control
is managed through a trust graph distributed between both remote trust servers
and locally cached and managed data.")
    (license license:asl2.0)))
(define-public go-github-com-docker-distribution-2.7.1+incompatible
  (package
    (name "go-github-com-docker-distribution")
    (version "2.7.1+incompatible")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/distribution/distribution")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1nx8b5a68rn81alp8wkkw6qd5v32mgf0fk23mxm60zdf63qk1nzw"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/docker/distribution"))
    (home-page "https://github.com/docker/distribution")
    (synopsis "Distribution")
    (description
      "Package distribution will define the interfaces for the components of docker
distribution.  The goal is to allow users to reliably package, ship and store
content related to docker images.")
    (license license:asl2.0)))
(define-public go-golang-org-x-sys-0.0.0-20190422165155-953cdadca894
  (package
    (name "go-golang-org-x-sys")
    (version "0.0.0-20190422165155-953cdadca894")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/sys")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0gkha4whk8xkcv3isigbs250akag99isxnd3v9xmy0kl3g88hxy1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/sys"))
    (home-page "https://golang.org/x/sys")
    (synopsis "sys")
    (description
      "This repository holds supplemental Go packages for low-level interactions with
the operating system.")
    (license license:bsd-3)))
(define-public go-github-com-stretchr-testify-1.2.2
  (package
    (name "go-github-com-stretchr-testify")
    (version "1.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/stretchr/testify")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0dlszlshlxbmmfxj5hlwgv3r22x0y1af45gn1vd198nvvs3pnvfs"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/stretchr/testify"))
    (home-page "https://github.com/stretchr/testify")
    (synopsis "Testify - Thou Shalt Write Tests")
    (description
      "** We are working on testify v2 and would love to hear what you'd like to see in
it, have your say here: @url{https://cutt.ly/testify,https://cutt.ly/testify} **
Package testify is a set of packages that provide many tools for testifying that
your code will behave as you intend.")
    (license license:expat)))
(define-public go-github-com-stretchr-objx-0.1.1
  (package
    (name "go-github-com-stretchr-objx")
    (version "0.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/stretchr/objx")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0iph0qmpyqg4kwv8jsx6a56a7hhqq8swrazv40ycxk9rzr0s8yls"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/stretchr/objx"))
    (home-page "https://github.com/stretchr/objx")
    (synopsis "Objx")
    (description
      "Objx - Go package for dealing with maps, slices, JSON and other data.")
    (license license:expat)))
(define-public go-github-com-konsorten-go-windows-terminal-sequences-1.0.1
  (package
    (name "go-github-com-konsorten-go-windows-terminal-sequences")
    (version "1.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/konsorten/go-windows-terminal-sequences")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1lchgf27n276vma6iyxa0v1xds68n2g8lih5lavqnx5x6q5pw2ip"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "github.com/konsorten/go-windows-terminal-sequences"))
    (home-page "https://github.com/konsorten/go-windows-terminal-sequences")
    (synopsis "Windows Terminal Sequences")
    (description
      "This library allow for enabling Windows terminal color support for Go.")
    (license license:expat)))
(define-public go-github-com-davecgh-go-spew-1.1.1
  (package
    (name "go-github-com-davecgh-go-spew")
    (version "1.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/davecgh/go-spew")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0hka6hmyvp701adzag2g26cxdj47g21x6jz4sc6jjz1mn59d474y"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/davecgh/go-spew"))
    (home-page "https://github.com/davecgh/go-spew")
    (synopsis "go-spew")
    (description
      "Go-spew implements a deep pretty printer for Go data structures to aid in
debugging.  A comprehensive suite of tests with 100% test coverage is provided
to ensure proper functionality.  See @code{test_coverage.txt} for the gocov
coverage report.  Go-spew is licensed under the liberal ISC license, so it may
be used in open source or commercial projects.")
    (license license:isc)))
(define-public go-gopkg-in-yaml-v2-2.2.2
  (package
    (name "go-gopkg-in-yaml-v2")
    (version "2.2.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/yaml.v2")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "01wj12jzsdqlnidpyjssmj0r4yavlqy7dwrg7adqd8dicjc4ncsa"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "gopkg.in/yaml.v2" #:unpack-path "gopkg.in/yaml.v2"))
    (propagated-inputs
      `(("go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405"
         ,go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405)))
    (home-page "https://gopkg.in/yaml.v2")
    (synopsis "YAML support for the Go language")
    (description "Package yaml implements YAML support for the Go language.")
    (license license:asl2.0)))
(define-public go-gopkg-in-check-v1-1.0.0-20190902080502-41f04d3bba15
  (package
    (name "go-gopkg-in-check-v1")
    (version "1.0.0-20190902080502-41f04d3bba15")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/check.v1")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0vfk9czmlxmp6wndq8k17rhnjxal764mxfhrccza7nwlia760pjy"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "gopkg.in/check.v1" #:unpack-path "gopkg.in/check.v1"))
    (home-page "https://gopkg.in/check.v1")
    (synopsis "Instructions")
    (description
      "Package check is a rich testing extension for Go's testing package.")
    (license license:bsd-2)))
(define-public go-golang-org-x-text-0.3.2
  (package
    (name "go-golang-org-x-text")
    (version "0.3.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/text")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0flv9idw0jm5nm8lx25xqanbkqgfiym6619w575p7nrdh0riqwqh"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/text"))
    (propagated-inputs
      `(("go-golang-org-x-tools-0.0.0-20180917221912-90fa682c2a6e"
         ,go-golang-org-x-tools-0.0.0-20180917221912-90fa682c2a6e)))
    (home-page "https://golang.org/x/text")
    (synopsis "Go Text")
    (description
      "text is a repository of text-related packages related to internationalization
(i18n) and localization (l10n), such as character encodings, text
transformations, and locale-specific text handling.")
    (license license:bsd-3)))
(define-public go-golang-org-x-net-0.0.0-20190620200207-3b0461eec859
  (package
    (name "go-golang-org-x-net")
    (version "0.0.0-20190620200207-3b0461eec859")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/net")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0l00c8l0a8xnv6qdpwfzxxsr58jggacgzdrwiprrfx2xqm37b6d5"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/net"))
    (propagated-inputs
      `(("go-golang-org-x-text-0.3.0" ,go-golang-org-x-text-0.3.0)
        ("go-golang-org-x-sys-0.0.0-20190215142949-d0b11bdaac8a"
         ,go-golang-org-x-sys-0.0.0-20190215142949-d0b11bdaac8a)
        ("go-golang-org-x-crypto-0.0.0-20190308221718-c2843e01d9a2"
         ,go-golang-org-x-crypto-0.0.0-20190308221718-c2843e01d9a2)))
    (home-page "https://golang.org/x/net")
    (synopsis "Go Networking")
    (description
      "This repository holds supplementary Go networking libraries.")
    (license license:bsd-3)))
(define-public go-github-com-tj-go-spin-1.1.0
  (package
    (name "go-github-com-tj-go-spin")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tj/go-spin")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "11xr67991m5pwsy1dira3iwd0sr55vmn1cyjwmlqziw4bwpym64s"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tj/go-spin"))
    (home-page "https://github.com/tj/go-spin")
    (synopsis #f)
    (description #f)
    (license #f)))
(define-public go-github-com-tj-go-kinesis-0.0.0-20171128231115-08b17f58cb1b
  (package
    (name "go-github-com-tj-go-kinesis")
    (version "0.0.0-20171128231115-08b17f58cb1b")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tj/go-kinesis")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "108c6p5j6rhhc2cnc2v5368yfsw73y6lzlvz02vpvvjph8rhmld4"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tj/go-kinesis"))
    (home-page "https://github.com/tj/go-kinesis")
    (synopsis "go-kinesis")
    (description
      "Package kinesis implements a batch producer built on top of the official AWS
SDK.")
    (license license:expat)))
(define-public go-github-com-tj-go-elastic-0.0.0-20171221160941-36157cbbebc2
  (package
    (name "go-github-com-tj-go-elastic")
    (version "0.0.0-20171221160941-36157cbbebc2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tj/go-elastic")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1r94vc4hbfvqvjz74n4mvsw4dy3vbyzlivb90kyn8vn76a4wqk69"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tj/go-elastic"))
    (home-page "https://github.com/tj/go-elastic")
    (synopsis "go-elastic")
    (description
      "Package elastic provides an Elasticsearch client with AWS sigv4 support.")
    (license license:expat)))
(define-public go-github-com-tj-go-buffer-1.1.0
  (package
    (name "go-github-com-tj-go-buffer")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tj/go-buffer")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1qdxpa069pjjbzh217aadpzz1aq1n6h32lhp60yjcpi7hr4q8fxx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tj/go-buffer"))
    (propagated-inputs
      `(("go-github-com-tj-assert-0.0.3" ,go-github-com-tj-assert-0.0.3)))
    (home-page "https://github.com/tj/go-buffer")
    (synopsis "Buffer")
    (description
      "Package buffer provides a generic buffer or batching mechanism for flushing
entries at a given size or interval, useful for cases such as batching log
events.")
    (license license:expat)))
(define-public go-github-com-tj-assert-0.0.3
  (package
    (name "go-github-com-tj-assert")
    (version "0.0.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/tj/assert")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1j5swk3fjq1h5fpqkipddz2ccnbidr7qrpm5dpdaflg9q5jnc673"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/tj/assert"))
    (propagated-inputs
      `(("go-gopkg-in-yaml-v3-3.0.0-20200605160147-a5ece683394c"
         ,go-gopkg-in-yaml-v3-3.0.0-20200605160147-a5ece683394c)
        ("go-github-com-stretchr-testify-1.6.1"
         ,go-github-com-stretchr-testify-1.6.1)
        ("go-github-com-davecgh-go-spew-1.1.1"
         ,go-github-com-davecgh-go-spew-1.1.1)))
    (home-page "https://github.com/tj/assert")
    (synopsis "assert")
    (description
      "Package assert implements the same assertions as the `assert` package but stops
test execution when a test fails.")
    (license license:expat)))
(define-public go-github-com-smartystreets-gunit-1.0.0
  (package
    (name "go-github-com-smartystreets-gunit")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/smartystreets/gunit")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "11d09rl5qqpj36a311pjj8w5jkj83d5b8gdcsx8f1zn4j924sm5d"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/smartystreets/gunit"))
    (propagated-inputs
      `(("go-github-com-smartystreets-assertions-1.0.0"
         ,go-github-com-smartystreets-assertions-1.0.0)))
    (home-page "https://github.com/smartystreets/gunit")
    (synopsis "gunit")
    (description
      "Package gunit provides \"testing\" package hooks and convenience functions for
writing tests in an xUnit style.  See the README file and the examples folder
for examples.")
    (license license:expat)))
(define-public go-github-com-smartystreets-go-aws-auth-0.0.0-20180515143844-0c1422d1fdb9
  (package
    (name "go-github-com-smartystreets-go-aws-auth")
    (version "0.0.0-20180515143844-0c1422d1fdb9")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/smarty-archives/go-aws-auth")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0krfdpgn3gfii1z9fi8ydfw0wwfqyvp6w3rji7w92m528zkjl93d"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/smartystreets/go-aws-auth"))
    (home-page "https://github.com/smartystreets/go-aws-auth")
    (synopsis "go-aws-auth")
    (description
      "Package awsauth implements AWS request signing using Signed Signature Version 2,
Signed Signature Version 3, and Signed Signature Version 4.  Supports S3 and
STS.")
    (license license:expat)))
(define-public go-github-com-rogpeppe-fastuuid-1.1.0
  (package
    (name "go-github-com-rogpeppe-fastuuid")
    (version "1.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/rogpeppe/fastuuid")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1jlif5pxyz3md02ij93kd2y2j1zz0ajc9k8azvn83vv6l3r5c1zg"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/rogpeppe/fastuuid"))
    (home-page "https://github.com/rogpeppe/fastuuid")
    (synopsis "fastuuid")
    (description
      "Package fastuuid provides fast UUID generation of 192 bit universally unique
identifiers.")
    (license license:bsd-3)))
(define-public go-github-com-pkg-errors-0.8.1
  (package
    (name "go-github-com-pkg-errors")
    (version "0.8.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/pkg/errors")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0g5qcb4d4fd96midz0zdk8b9kz8xkzwfa8kr1cliqbg8sxsy5vd1"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/pkg/errors"))
    (home-page "https://github.com/pkg/errors")
    (synopsis "errors")
    (description "Package errors provides simple error handling primitives.")
    (license license:bsd-2)))
(define-public go-github-com-mattn-go-colorable-0.1.2
  (package
    (name "go-github-com-mattn-go-colorable")
    (version "0.1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/mattn/go-colorable")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0512jm3wmzkkn7d99x9wflyqf48n5ri3npy1fqkq6l6adc5mni3n"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/mattn/go-colorable"))
    (propagated-inputs
      `(("go-github-com-mattn-go-isatty-0.0.8"
         ,go-github-com-mattn-go-isatty-0.0.8)))
    (home-page "https://github.com/mattn/go-colorable")
    (synopsis "go-colorable")
    (description "Colorable writer for windows.")
    (license license:expat)))
(define-public go-github-com-kr-pretty-0.2.0
  (package
    (name "go-github-com-kr-pretty")
    (version "0.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/kr/pretty")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1ywbfzz1h3a3qd8rpkiqwi1dm4w8ls9ijb4x1b7567grns9f0vnp"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/kr/pretty"))
    (propagated-inputs
      `(("go-github-com-kr-text-0.1.0" ,go-github-com-kr-text-0.1.0)))
    (home-page "https://github.com/kr/pretty")
    (synopsis #f)
    (description
      "Package pretty provides pretty-printing for Go values.  This is useful during
debugging, to avoid wrapping long output lines in the terminal.")
    (license license:expat)))
(define-public go-github-com-jpillora-backoff-0.0.0-20180909062703-3050d21c67d7
  (package
    (name "go-github-com-jpillora-backoff")
    (version "0.0.0-20180909062703-3050d21c67d7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/jpillora/backoff")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1nxapdx9xg5gwiscfhq7m0w14hj4gaxb4avmgf1mx9zd3jnw9jxv"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/jpillora/backoff"))
    (home-page "https://github.com/jpillora/backoff")
    (synopsis "Backoff")
    (description
      "Package backoff provides an exponential-backoff implementation.")
    (license license:expat)))
(define-public go-github-com-google-uuid-1.1.1
  (package
    (name "go-github-com-google-uuid")
    (version "1.1.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/google/uuid")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0hfxcf9frkb57k6q0rdkrmnfs78ms21r1qfk9fhlqga2yh5xg8zb"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/google/uuid"))
    (home-page "https://github.com/google/uuid")
    (synopsis "uuid")
    (description "Package uuid generates and inspects UUIDs.")
    (license license:bsd-3)))
(define-public go-github-com-golang-protobuf-1.3.1
  (package
    (name "go-github-com-golang-protobuf")
    (version "1.3.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/golang/protobuf")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "15am4s4646qy6iv0g3kkqq52rzykqjhm4bf08dk0fy2r58knpsyl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/golang/protobuf"))
    (home-page "https://github.com/golang/protobuf")
    (synopsis "Go support for Protocol Buffers")
    (description
      "This module (@url{https://pkg.go.dev/mod/github.com/golang/protobuf,(code
github.com/golang/protobuf)}) contains Go bindings for protocol buffers.")
    (license license:bsd-3)))
(define-public go-github-com-go-logfmt-logfmt-0.4.0
  (package
    (name "go-github-com-go-logfmt-logfmt")
    (version "0.4.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/go-logfmt/logfmt")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "06smxc112xmixz78nyvk3b2hmc7wasf2sl5vxj1xz62kqcq9lzm9"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/go-logfmt/logfmt"))
    (propagated-inputs
      `(("go-github-com-kr-logfmt-0.0.0-20140226030751-b84e30acd515"
         ,go-github-com-kr-logfmt-0.0.0-20140226030751-b84e30acd515)))
    (home-page "https://github.com/go-logfmt/logfmt")
    (synopsis "logfmt")
    (description
      "Package logfmt implements utilities to marshal and unmarshal data in the logfmt
format.  The logfmt format records key/value pairs in a way that balances
readability for humans and simplicity of computer parsing.  It is most commonly
used as a more human friendly alternative to JSON for structured logging.")
    (license license:expat)))
(define-public go-github-com-fatih-color-1.7.0
  (package
    (name "go-github-com-fatih-color")
    (version "1.7.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/fatih/color")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0v8msvg38r8d1iiq2i5r4xyfx0invhc941kjrsg5gzwvagv55inv"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/fatih/color"))
    (home-page "https://github.com/fatih/color")
    (synopsis "color")
    (description
      "Package color is an ANSI color package to output colorized or SGR defined output
to the standard output.  The API can be used in several way, pick one that suits
you.")
    (license license:expat)))
(define-public go-github-com-aybabtme-rgbterm-0.0.0-20170906152045-cc83f3b3ce59
  (package
    (name "go-github-com-aybabtme-rgbterm")
    (version "0.0.0-20170906152045-cc83f3b3ce59")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/aybabtme/rgbterm")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0wvmxvjn64968ikvnxrflb1x8rlcwzpfl53fzbxff2axbx9lq50q"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/aybabtme/rgbterm"))
    (home-page "https://github.com/aybabtme/rgbterm")
    (synopsis "RGB terminal")
    (description
      "Package rgbterm colorizes bytes and strings using RGB colors, for a full range
of pretty terminal strings.")
    (license license:expat)))
(define-public go-github-com-aws-aws-sdk-go-1.20.6
  (package
    (name "go-github-com-aws-aws-sdk-go")
    (version "1.20.6")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/aws/aws-sdk-go")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "016vy5s9finyfgh08j7rvn2pnwymdgj2ydr59w57pysnri87mdwx"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/aws/aws-sdk-go"))
    (propagated-inputs
      `(("go-github-com-jmespath-go-jmespath-0.0.0-20180206201540-c2b33e8439af"
         ,go-github-com-jmespath-go-jmespath-0.0.0-20180206201540-c2b33e8439af)))
    (home-page "https://github.com/aws/aws-sdk-go")
    (synopsis "AWS SDK for Go")
    (description
      "Package sdk is the official AWS SDK for the Go programming language.")
    (license license:asl2.0)))
(define-public go-github-com-aphistic-sweet-0.2.0
  (package
    (name "go-github-com-aphistic-sweet")
    (version "0.2.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/aphistic/sweet")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1bb4qagfxf6byqn2yx0vq24xfvisz3ah4w6bvqclc8cklvfngw43"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/aphistic/sweet"))
    (propagated-inputs
      `(("go-golang-org-x-crypto-0.0.0-20190426145343-a29dc8fdc734"
         ,go-golang-org-x-crypto-0.0.0-20190426145343-a29dc8fdc734)
        ("go-github-com-sergi-go-diff-1.0.0"
         ,go-github-com-sergi-go-diff-1.0.0)
        ("go-github-com-onsi-gomega-1.5.0" ,go-github-com-onsi-gomega-1.5.0)
        ("go-github-com-mgutz-ansi-0.0.0-20170206155736-9520e82c474b"
         ,go-github-com-mgutz-ansi-0.0.0-20170206155736-9520e82c474b)
        ("go-github-com-mattn-go-colorable-0.1.1"
         ,go-github-com-mattn-go-colorable-0.1.1)))
    (home-page "https://github.com/aphistic/sweet")
    (synopsis "sweet")
    (description
      "Sweet is a pluggable test runner capable of hooking into standard Go tests.  It
attempts to provide access to the standard Go test tool as close as possible
while adding support for test suites and plugins that can hook into test results
to add additional functionality.")
    (license license:expat)))
(define-public go-github-com-aphistic-golf-0.0.0-20180712155816-02c07f170c5a
  (package
    (name "go-github-com-aphistic-golf")
    (version "0.0.0-20180712155816-02c07f170c5a")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/aphistic/golf")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1qixab9bb29wqbr4nc5j3g25hq1j7am93f181rkj7a4qacncx763"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/aphistic/golf"))
    (home-page "https://github.com/aphistic/golf")
    (synopsis "golf")
    (description
      "This package provides logging capabilities using the GELF
(@url{https://www.graylog.org/resources/gelf-2/,https://www.graylog.org/resources/gelf-2/})
log format")
    (license license:expat)))
(define-public go-github-com-apex-logs-1.0.0
  (package
    (name "go-github-com-apex-logs")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/apex/logs")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1kysjp3s8718p02ngpr60gnjjspv0a69biksfjqa1540svrbi850"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/apex/logs"))
    (propagated-inputs
      `(("go-github-com-tj-assert-0.0.0-20171129193455-018094318fb0"
         ,go-github-com-tj-assert-0.0.0-20171129193455-018094318fb0)
        ("go-github-com-stretchr-testify-1.3.0"
         ,go-github-com-stretchr-testify-1.3.0)))
    (home-page "https://github.com/apex/logs")
    (synopsis "Example")
    (description "Go client for @url{https://apex.sh/logs/,Apex Logs}.")
    (license license:expat)))
(define-public go-golang-org-x-text-0.3.0
  (package
    (name "go-golang-org-x-text")
    (version "0.3.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/text")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0r6x6zjzhr8ksqlpiwm5gdd7s209kwk5p4lw54xjvz10cs3qlq19"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/text"))
    (home-page "https://golang.org/x/text")
    (synopsis "Go Text")
    (description
      "text is a repository of text-related packages related to internationalization
(i18n) and localization (l10n), such as character encodings, text
transformations, and locale-specific text handling.")
    (license license:bsd-3)))
(define-public go-golang-org-x-sys-0.0.0-20190215142949-d0b11bdaac8a
  (package
    (name "go-golang-org-x-sys")
    (version "0.0.0-20190215142949-d0b11bdaac8a")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/sys")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "18yfsmw622l7gc5sqriv5qmck6903vvhivpzp8i3xfy3z33dybdl"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/sys"))
    (home-page "https://golang.org/x/sys")
    (synopsis "sys")
    (description
      "This repository holds supplemental Go packages for low-level interactions with
the operating system.")
    (license license:bsd-3)))
(define-public go-golang-org-x-crypto-0.0.0-20190308221718-c2843e01d9a2
  (package
    (name "go-golang-org-x-crypto")
    (version "0.0.0-20190308221718-c2843e01d9a2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://go.googlesource.com/crypto")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "01xgxbj5r79nmisdvpq48zfy8pzaaj90bn6ngd4nf33j9ar1dp8r"))))
    (build-system go-build-system)
    (arguments '(#:import-path "golang.org/x/crypto"))
    (propagated-inputs
      `(("go-golang-org-x-sys-0.0.0-20190215142949-d0b11bdaac8a"
         ,go-golang-org-x-sys-0.0.0-20190215142949-d0b11bdaac8a)))
    (home-page "https://golang.org/x/crypto")
    (synopsis "Go Cryptography")
    (description
      "This repository holds supplementary Go cryptography libraries.")
    (license license:bsd-3)))
(define-public go-gopkg-in-yaml-v3-3.0.0-20200605160147-a5ece683394c
  (package
    (name "go-gopkg-in-yaml-v3")
    (version "3.0.0-20200605160147-a5ece683394c")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://gopkg.in/yaml.v3")
               (commit (go-version->git-ref version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "03cm7c3p5fh0aa8vnxv5010dgqqr268ivb480ix1salxx3396w68"))))
    (build-system go-build-system)
    (arguments
      '(#:import-path "gopkg.in/yaml.v3" #:unpack-path "gopkg.in/yaml.v3"))
    (propagated-inputs
      `(("go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405"
         ,go-gopkg-in-check-v1-0.0.0-20161208181325-20d25e280405)))
    (home-page "https://gopkg.in/yaml.v3")
    (synopsis "YAML support for the Go language")
    (description "Package yaml implements YAML support for the Go language.")
    (license unknown-license!)))
