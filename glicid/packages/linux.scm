(define-module (glicid packages linux)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (gnu packages backup)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cryptsetup)
  #:use-module (gnu packages golang)
  #:use-module ((gnu packages linux) #:prefix gnu:)
  #:use-module (gnu packages linux) ;; yes : redundant FIXIT
  #:use-module (gnu system uuid)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages python)
  #:use-module (gnu packages tls)
  #:use-module (glicid packages golang)
  #:use-module (glicid utils)
)

(define-public rdma-core-upstream-37.1
  (package
    (inherit gnu:rdma-core)
    (name (string-append (package-name gnu:rdma-core) "-upstream" ))
    (version "37.1")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/linux-rdma/rdma-core/releases/download/v" version "/rdma-core-" version ".tar.gz"))
        (sha256 (base32 "1hjwagf5x48vgshy5s01qjlzjr8kmxpflfcvh8pgj3zbj82zzxiz"))
      )
    )
  )
)

(define-public rdma-core-upstream-38.0
  (package
    (inherit gnu:rdma-core)
    (name (string-append (package-name gnu:rdma-core) "-upstream" ))
    (version "38.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/linux-rdma/rdma-core/releases/download/v" version "/rdma-core-" version ".tar.gz"))
        (sha256 (base32 "043vybwx9kz4mbbmnj0jzkzsw02vzhkkjc5j3yjdiiqkmsgwr3cs"))
      )
    )
  )
)

(define-public rdma-core-upstream-39.0
  (package
    (inherit gnu:rdma-core)
    (name (string-append (package-name gnu:rdma-core) "-upstream" ))
    (version "39.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/linux-rdma/rdma-core/releases/download/v" version "/rdma-core-" version ".tar.gz"))
        (sha256 (base32 "0y13px3qgyh3szywjhikw183y54iym9sa60aw0sf51p3kzgg1spn"))
      )
    )   
  )
)

(define local-rdma-core rdma-core-upstream-39.0)

(define-public rdma-core-latest (latest-version local-rdma-core gnu:rdma-core))

(define-public libfabric-upstream-1.13.1
  (package
    (inherit gnu:libfabric)
    (version "1.13.1")
    (name (string-append (package-name gnu:libfabric) "-upstream" ))
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/ofiwg/libfabric/releases/download/v" version "/libfabric-" version ".tar.bz2"))
        (sha256 (base32 "03nkhqjjyw3hwhxrn7rg30qc1gzc1r7p7lymyz5s96m3qhwfsvlf"))
      )
    )
  )
)

(define-public libfabric-upstream-1.14.0
  (package
    (inherit gnu:libfabric)
    (version "1.14.0")
    (name (string-append (package-name gnu:libfabric) "-upstream" ))
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/ofiwg/libfabric/releases/download/v" version "/libfabric-" version ".tar.bz2"))
        (sha256 (base32 "16klkzkg04wb699mqpi8mn2r8sqzj35zalynbdazyg4ghj4169pw"))
      )
    )
  )
)

(define-public libfabric-opx-beta-upstream-1.14.0-c

(let ((commit "e127cd074bbfd8c50b9f3cfb5b8e24162f8076a4"))
  (package
    (inherit gnu:libfabric)
    (version "1.14.0c")
    (name (string-append (package-name gnu:libfabric) "-opx-beta-upstream" ))

    (source
      (origin

       (method git-fetch)
        (uri (git-reference
          (url "https://github.com/ofiwg/libfabric.git")
            (commit commit)
         ))
        (file-name (git-file-name name version))
        (sha256 (base32 "0fh665l2xcl59zhbn63y5bm8v9s652gk5s8r094qb4if0bp3in04"))

      )
    )
     (inputs `(
      ("autoconf", autoconf)
      ("automake", automake)
      ("libtool", libtool)
      ("libuuid" , util-linux "lib")
      ("numactl" , gnu:numactl)
      ("psm2" , gnu:psm2)
      ,@(package-inputs gnu:libfabric)))
  
  )
 )   
)


(define-public libfabric-opx-beta-upstream-1.14.0-d

(let ((commit "e127cd074bbfd8c50b9f3cfb5b8e24162f8076a4"))
  (package
    (inherit gnu:libfabric)
    (version "1.14.0d")
    (name (string-append (package-name gnu:libfabric) "-opx-beta-upstream" ))

    (source
      (origin

       (method git-fetch)
        (uri (git-reference
          (url "https://github.com/ofiwg/libfabric.git")
            (commit commit)
         ))
        (file-name (git-file-name name version))
        (sha256 (base32 "0fh665l2xcl59zhbn63y5bm8v9s652gk5s8r094qb4if0bp3in04"))

      )
    )
     (inputs `(
      ("autoconf", autoconf)
      ("automake", automake)
      ("libtool", libtool)
      ("libuuid" , util-linux "lib")
      ("numactl" , gnu:numactl)
   ;   ("psm2" , gnu:psm2) ; intended : no psm2
      ,@(package-inputs gnu:libfabric)))

  )
 )   
)



;(define local-libfabric libfabric-opx-beta-upstream-1.14.0-c)
(define local-libfabric libfabric-opx-beta-upstream-1.14.0-d)

(define-public libfabric-latest (latest-version local-libfabric gnu:libfabric))

(define-public apptainer
  (package
    (name "apptainer")
    (version "1.0.1")
    (source 
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://github.com/apptainer/apptainer/")
          (commit "a308dd1a664a0bf01ff74b39c69d07442c7fac4f")
        ))
        (sha256 (base32 "0k2fkwlvp72vh07a7m1sdzj1f3zml9y5mligzhnscs7zzlv5s28j"))
        (modules '((srfi srfi-26)))
        (snippet
          '(call-with-output-file "VERSION" (cut display version <>))
        )
      )
    )
    (home-page "https://apptainer.org/")
    (synopsis "THE CONTAINER SYSTEM FOR SECURE HIGH PERFORMANCE COMPUTING")
    (description "Apptainer/Singularity is the most widely used container system for HPC.
                  It is designed to execute applications at bare-metal performance while
                  being secure, portable, and 100% reproducible. Apptainer is an 
                  open-source project with a friendly community of developers and users.
                  The user base continues to expand, with Apptainer/Singularity now used
                  across industry and academia in many areas of work."
    )
    (license license:bsd-3)
    (build-system gnu-build-system)
    (inputs `(
      ("libarchive", libarchive)
      ("python", python-wrapper)
      ("zlib", zlib)
      ("squashfs-tools", squashfs-tools)
      ("openssl", openssl)
      ("libseccomp", gnu:libseccomp)
      ("cryptsetup", cryptsetup)
      ("go", go-1.17)
      ("pkg-config", pkg-config)
      ("go-github-com-netflix-go-expect-0.0.0-20220104043353-73e0943537d2", go-github-com-netflix-go-expect-0.0.0-20220104043353-73e0943537d2)
      ("go-github-com-protonmail-go-crypto-0.0.0-20220113124808-70ae35bab23f", go-github-com-protonmail-go-crypto-0.0.0-20220113124808-70ae35bab23f)
      ("go-github-com-adigunhammedolalekan-registry-auth-0.0.0-20200730122110-8cde180a3a60", go-github-com-adigunhammedolalekan-registry-auth-0.0.0-20200730122110-8cde180a3a60)
      ("go-github-com-apex-log-1.9.0", go-github-com-apex-log-1.9.0)
      ("go-github-com-apptainer-container-key-client-0.7.2", go-github-com-apptainer-container-key-client-0.7.2)
      ("go-github-com-apptainer-container-library-client-1.2.2", go-github-com-apptainer-container-library-client-1.2.2)
      ("go-github-com-apptainer-sif-v2-2.3.2", go-github-com-apptainer-sif-v2-2.3.2)
      ("go-github-com-blang-semver-v4-4.0.0", go-github-com-blang-semver-v4-4.0.0)
      ("go-github-com-buger-jsonparser-1.1.1", go-github-com-buger-jsonparser-1.1.1)
      ("go-github-com-cenkalti-backoff-v4-4.1.2", go-github-com-cenkalti-backoff-v4-4.1.2)
      ("go-github-com-containerd-cgroups-1.0.3", go-github-com-containerd-cgroups-1.0.3)
      ("go-github-com-containerd-containerd-1.6.0", go-github-com-containerd-containerd-1.6.0)
      ("go-github-com-containernetworking-cni-1.0.1", go-github-com-containernetworking-cni-1.0.1)
      ("go-github-com-containernetworking-plugins-1.1.0", go-github-com-containernetworking-plugins-1.1.0)
      ("go-github-com-containers-image-v5-5.19.1", go-github-com-containers-image-v5-5.19.1)
      ("go-github-com-creack-pty-1.1.17", go-github-com-creack-pty-1.1.17)
      ("go-github-com-cyphar-filepath-securejoin-0.2.3", go-github-com-cyphar-filepath-securejoin-0.2.3)
      ("go-github-com-docker-docker-20.10.12+incompatible", go-github-com-docker-docker-20.10.12+incompatible)
      ("go-github-com-fatih-color-1.13.0", go-github-com-fatih-color-1.13.0)
      ("go-github-com-go-log-log-0.2.0", go-github-com-go-log-log-0.2.0)
      ("go-github-com-google-uuid-1.3.0", go-github-com-google-uuid-1.3.0)
      ("go-github-com-opencontainers-go-digest-1.0.0", go-github-com-opencontainers-go-digest-1.0.0)
      ("go-github-com-opencontainers-image-spec-1.0.3-0.20211202193544-a5463b7f9c84", go-github-com-opencontainers-image-spec-1.0.3-0.20211202193544-a5463b7f9c84)
      ("go-github-com-opencontainers-runtime-spec-1.0.3-0.20210326190908-1c3f411f0417", go-github-com-opencontainers-runtime-spec-1.0.3-0.20210326190908-1c3f411f0417)
      ("go-github-com-opencontainers-runtime-tools-0.9.1-0.20210326182921-59cdde06764b", go-github-com-opencontainers-runtime-tools-0.9.1-0.20210326182921-59cdde06764b)
      ("go-github-com-opencontainers-selinux-1.10.0", go-github-com-opencontainers-selinux-1.10.0)
      ("go-github-com-opencontainers-umoci-0.4.7", go-github-com-opencontainers-umoci-0.4.7)
      ("go-github-com-pelletier-go-toml-1.9.4", go-github-com-pelletier-go-toml-1.9.4)
      ("go-github-com-pkg-errors-0.9.1", go-github-com-pkg-errors-0.9.1)
      ("go-github-com-seccomp-containers-golang-0.6.0", go-github-com-seccomp-containers-golang-0.6.0)
      ("go-github-com-seccomp-libseccomp-golang-0.9.2-0.20210429002308-3879420cc921", go-github-com-seccomp-libseccomp-golang-0.9.2-0.20210429002308-3879420cc921)
      ("go-github-com-spf13-cobra-1.3.0", go-github-com-spf13-cobra-1.3.0)
      ("go-github-com-spf13-pflag-1.0.5", go-github-com-spf13-pflag-1.0.5)
      ("go-github-com-sylabs-json-resp-0.8.0", go-github-com-sylabs-json-resp-0.8.0)
      ("go-github-com-vbauerster-mpb-v7-7.4.1", go-github-com-vbauerster-mpb-v7-7.4.1)
      ("go-golang-org-x-sys-0.0.0-20220209214540-3681064d5158", go-golang-org-x-sys-0.0.0-20220209214540-3681064d5158)
      ("go-golang-org-x-term-0.0.0-20210916214954-140adaaadfaf", go-golang-org-x-term-0.0.0-20210916214954-140adaaadfaf)
      ("go-gopkg-in-yaml-v2-2.4.0", go-gopkg-in-yaml-v2-2.4.0)
      ("go-gotest-tools-v3-3.1.0", go-gotest-tools-v3-3.1.0)
      ("go-mvdan-cc-sh-v3-3.4.3-0.20220202175809-113ed667a8a7", go-mvdan-cc-sh-v3-3.4.3-0.20220202175809-113ed667a8a7)
      ("go-oras-land-oras-go-1.1.0", go-oras-land-oras-go-1.1.0)
      ("go-github-com-opencontainers-image-spec-1.0.2-0.20211117181255-693428a734f5", go-github-com-opencontainers-image-spec-1.0.2-0.20211117181255-693428a734f5)
      ("go-github-com-moby-sys-0.0.0-20220308220145-03355939d693", go-github-com-moby-sys-0.0.0-20220308220145-03355939d693)
      ("go-github-com-urfave-cli-1.22.5", go-github-com-urfave-cli-1.22.5)
      ("go-github-com-xeipuuv-gojsonpointer-0.0.0-20190905194746-02993c407bfb", go-github-com-xeipuuv-gojsonpointer-0.0.0-20190905194746-02993c407bfb)
      ("go-github-com-yvasiyarov-go-metrics-0.0.0-20150112132944-c25f46c4b940", go-github-com-yvasiyarov-go-metrics-0.0.0-20150112132944-c25f46c4b940)
      ("go-github-com-yvasiyarov-gorelic-0.0.6", go-github-com-yvasiyarov-gorelic-0.0.6)
      ("go-github-com-yvasiyarov-newrelic-platform-go-0.0.0-20160601141957-9c099fbc30e9", go-github-com-yvasiyarov-newrelic-platform-go-0.0.0-20160601141957-9c099fbc30e9)
    ))
    (arguments
      `(#:phases
        (modify-phases %standard-phases
          (replace 'configure
            (lambda* (#:key inputs native-inputs propagated-inputs configure-flags #:allow-other-keys)
              (begin
                (invoke "./mconfig" "--localstatedir=/var")
              )
            )
          )
          (replace 'build
            (lambda* (#:key inputs native-inputs propagated-inputs configure-flags #:allow-other-keys)
              (begin
                (invoke "make" "-C" "builddir")
              )
            )
          )
          (replace 'install
            (lambda* (#:key inputs native-inputs propagated-inputs configure-flags #:allow-other-keys)
              (begin
                (invoke "make" "-C" "builddir" "install")
              )
            )
          )
        )
      )
    )
  )
)

