(define-module (glicid packages maths)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((gnu packages maths) #:prefix gnu:)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages mpi)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (gnu packages)
  #:use-module (glicid utils)
)

(define-public scalapack-upstream
  (package
    (inherit gnu:scalapack) 
    (name "scalapack-upstream")
    (version "2.1.0-new-upstream")
    (source
      (origin
        (method git-fetch)
        (uri
          (git-reference
            (url "https://oauth2:glpat-RsX2GjsD2WrzXubiJeou@gitlab.univ-nantes.fr/CCIPL/legacy_code_mirror/scalapack.git")
            (commit "myv2.0.2")
          )
        )
        (file-name "scalapack-univ-myv2")
        (patches (search-patches "scalapack-blacs-mpi-deprecations.patch"))
        (sha256 (base32 "1ccic46psf2hl9wsyflvkn5rxg8k17q578m9mzimvm9brbggf0na" ))
      )
    )
    (arguments
      `(#:configure-flags `("-DBUILD_SHARED_LIBS:BOOL=YES")
        #:phases (modify-phases %standard-phases
          (add-before 'check 'mpi-setup
            ,%openmpi-setup
          )
        )
       )
    )
  )
)
(define local:scalapack scalapack-upstream)
(define-public scalapack-latest (latest-version local:scalapack gnu:scalapack))

(define-public openblas-upstream-0.3.15
  (package
    (inherit gnu:openblas)
    (name "openblas-upstream")
    (version "0.3.15")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/xianyi/OpenBLAS/releases/download/v" version "/OpenBLAS-" version ".tar.gz" ))
        (sha256 (base32 "1gjbkrsh6n28hdp2ciyjigc8vg764d2r0jbzl63v753mjzn9va9h"))
      )
    )
  )
)


(define local:openblas openblas-upstream-0.3.15)
(define-public scalapack-latest (latest-version local:openblas gnu:openblas))

