(define-module (glicid packages mpi)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module ((gnu packages mpi) #:prefix gnu:)
  #:use-module (glicid packages linux)
  #:use-module (glicid packages fabric-management)
  #:use-module (glicid utils)
  #:use-module (guix utils)
  #:use-module (glicid packages parallel)
  #:use-module (gnu packages)
)


(define-public openmpi-upstream-4.1.2
  (package
    (inherit gnu:openmpi)
    (name "openmpi-upstream")
    (version "4.1.2")
    (source
     (origin
      (method url-fetch)
      (uri (string-append "https://www.open-mpi.org/software/ompi/v"
                          (version-major+minor version)
                          "/downloads/openmpi-" version ".tar.bz2"))
      (sha256
       (base32 "09xmlr4mfs02kwcf5cmdgkcdjj81fjwjmpa3rz2k28f3gz7wfy4v"))
      (patches (search-patches "openmpi-mtl-priorities.patch"))))

  )
)

(define local-openmpi openmpi-upstream-4.1.2)

(define-public openmpi-latest (latest-version local-openmpi gnu:openmpi))


(define-public openmpi-glicid
  (transform-package
    (
      (instead-of "slurm" slurm-glicid) openmpi-latest
    ) "glicid"
  )
)

(define-public openmpi-glicid-libfabric
  (transform-package
    (
      (instead-of "libfabric" libfabric-latest)
      openmpi-glicid
    ) "libfabric"
  )
)

(define-public openmpi-glicid-rdma
  (transform-package
    (
      (instead-of "rdma-core" rdma-core-latest)
      openmpi-glicid
    ) "rdma-core"
  )
)

(define-public openmpi-glicid-ucx
  (transform-package
    (
      (instead-of "ucx" ucx-latest-glicid) openmpi-glicid
    ) "ucx"
  )
)

(define-public openmpi-glicid-libfabric-rdma
  (transform-package
    (
      (instead-of "rdma-core" rdma-core-latest)
      openmpi-glicid-libfabric
    ) "rdma"
  )
)

(define-public openmpi-glicid-libfabric-ucx
  (transform-package
    (
      (instead-of "ucx" ucx-latest-glicid)
      openmpi-glicid-libfabric
    ) "ucx"
  )
)

(define-public openmpi-glicid-libfabric-rdma-ucx
  (transform-package
    (
      (instead-of "ucx" ucx-latest-glicid)
      openmpi-glicid-libfabric-rdma
    ) "ucx"
  )
)

(define-public openmpi-glicid-libfabric-rdma-ucx-ccipl
  (transform-package
    (
      (instead-of "slurm-glicid" slurm-ccipl)
      openmpi-glicid-libfabric-rdma-ucx
    ) "ccipl"
  )
)  


(define openmpi-glicid-transform-gcc-11 (gcc11-instead-of-gcc openmpi-glicid))

(define-public openmpi-glicid-gcc-11
  (package
    (inherit openmpi-glicid-transform-gcc-11)
    (name (string-append (package-name openmpi-glicid-transform-gcc-11) "-gcc-11" ))
  )
)


;openmpi-glicid-libfabric-rdma-ucx-ccipl
