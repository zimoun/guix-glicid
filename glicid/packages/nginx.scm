(define-module (glicid packages nginx)
  #:use-module (ice-9 match)
  #:use-module (guix)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:select (asl2.0))
  #:use-module (guix utils)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages geo)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages tls)
  #:use-module (glicid packages openldap)
)

(define-public nginx-ldap-auth-module
  (package
    (inherit nginx)
    (name "nginx-ldap-auth-module")
    (version "83c059b73566c2ee9cbda920d91b66657cf120b7")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
          (url "https://github.com/kvspb/nginx-auth-ldap")
            (commit version)
          )
        )
        (file-name (git-file-name "nginx-ldap-auth-module" version))
        (sha256 (base32 "023zmdir7w92dnb508ggskkc7kmd7k71hc597sb7i4xfgpwxzq1s" ))
      )
    )
    (synopsis "LDAP Authentication module for nginx")
    (description "LDAP module for nginx which supports authentication against multiple LDAP servers.")
    (build-system gnu-build-system)
    (inputs
     `(("nginx-sources" ,(package-source nginx))
       ("openldap", openldap)
       ,@(package-inputs nginx)))
    (arguments
      (substitute-keyword-arguments
        `(#:configure-flags '("--add-dynamic-module=.")
           #:make-flags '("modules")
           ,@(package-arguments nginx)
         )
        ((#:phases phases)
          `(modify-phases ,phases
           (add-after 'unpack 'unpack-nginx-sources
             (lambda* (#:key inputs native-inputs #:allow-other-keys)
               (begin
                 ;; The nginx source code is part of the module’s source.
                 (format #t "decompressing nginx source code~%")
                 (let ((tar (assoc-ref inputs "tar")) (nginx-srcs (assoc-ref inputs "nginx-sources")))
                      (invoke (string-append tar "/bin/tar") "xvf" nginx-srcs "--strip-components=1")
                 )
                 #t
                )
             )
           )
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let ((modules-dir (string-append (assoc-ref outputs "out") "/etc/nginx/modules")))
                    (install-file "objs/ngx_http_auth_ldap_module.so" modules-dir)
               #t
               )
             )
           )
           (delete 'fix-root-dirs)
           (delete 'install-man-page)
          )
        )
      )
    )
  )
)

(define-public modsecurity
  (package
    (name "modsecurity")
    (version "3.0.5")
    (source
      (origin
        (method url-fetch)
        (uri (list
          (string-append "https://github.com/SpiderLabs/ModSecurity/releases/download/v" version "/modsecurity-v" version ".tar.gz")
        ))
        (sha256 (base32 "1sarp7bjvkkdlpky5j9axfi0qmb177vw2vn2s10c8fcdg9dgj6vm" ))
      )
    )
    (synopsis "modsecurity module for nginx")
    (description "modsecurity module for nginx.")
    (license asl2.0)
    (home-page "https://github.com/SpiderLabs/ModSecurity")
    (build-system gnu-build-system)
    (inputs `(
      ("curl", curl)
      ("libxml2", libxml2)
      ("libxslt", libxslt)
      ("openssl", openssl)
      ("pcre", pcre)
      ("pcre:bin", pcre "bin")
      ("zlib", zlib)
      ("pkg-config", pkg-config)
      ("nginx-sources", (package-source nginx))
    ))
    (arguments `(
       #:configure-flags (list
         (string-append "--with-curl=" (assoc-ref %build-inputs "curl"))
         (string-append "--with-pcre=" (assoc-ref %build-inputs "pcre:bin") "/bin/pcre-config")
         (string-append "--with-libxml=" (assoc-ref %build-inputs "libxml2"))
         "--enable-standalone-module"
       )
    ))
  )
)

(define-public nginx-modsecurity-module
  (package
    (inherit nginx)
    (name "nginx-modsecurity-module")
    (version "1.0.2")
    (source
      (origin
        (method url-fetch)
        (uri (list
          (string-append "https://github.com/SpiderLabs/ModSecurity-nginx/releases/download/v" version "/modsecurity-nginx-v" version ".tar.gz")
        ))
        (sha256 (base32 "0clcny8276d3j73ws661lh983vqq9m29rw7qb7gn132ha066d9j1" ))
      )
    )
    (synopsis "ModSecurity module for nginx")
    (description "ModSecurity module for nginx.")
    (build-system gnu-build-system)
    (inputs
     `(("nginx-sources" ,(package-source nginx))
       ("modsecurity", modsecurity)
       ,@(package-inputs nginx)))
    (arguments
      (substitute-keyword-arguments
        `(#:configure-flags '("--add-dynamic-module=.")
           #:make-flags '("modules")
           ,@(package-arguments nginx)
         )
        ((#:phases phases)
          `(modify-phases ,phases
           (add-after 'unpack 'unpack-nginx-sources
             (lambda* (#:key inputs native-inputs #:allow-other-keys)
               (begin
                 ;; The nginx source code is part of the module’s source.
                 (format #t "decompressing nginx source code~%")
                 (let ((tar (assoc-ref inputs "tar")) (nginx-srcs (assoc-ref inputs "nginx-sources")))
                      (invoke (string-append tar "/bin/tar") "xvf" nginx-srcs "--strip-components=1")
                 )
                 #t
                )
             )
           )
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let ((modules-dir (string-append (assoc-ref outputs "out") "/etc/nginx/modules")))
                    (install-file "objs/ngx_http_modsecurity_module.so" modules-dir)
               #t
               )
             )
           )
           (delete 'fix-root-dirs)
           (delete 'install-man-page)
          )
        )
      )
    )
  )
)

(define-public nginx-geoip2-module
  (package
    (inherit nginx)
    (name "nginx-geoip2-module")
    (version "3.3")
    (source
      (origin
        (method url-fetch)
        (uri (list
          (string-append "https://github.com/leev/ngx_http_geoip2_module/archive/refs/tags/" version ".tar.gz")
        ))
        (sha256 (base32 "19vqzwr6gaqkh3b7zbxc61f87d044ykw9l39i2hi7qrkr0w88ds1" ))
      )
    )
    (synopsis "GeoIP2 module for nginx")
    (description "GeoIP2 module for nginx.")
    (build-system gnu-build-system)
    (inputs
     `(("nginx-sources" ,(package-source nginx))
       ("libmaxminddb", libmaxminddb)
       ,@(package-inputs nginx)))
    (arguments
      (substitute-keyword-arguments
        `(#:configure-flags '("--add-dynamic-module=.")
           #:make-flags '("modules")
           ,@(package-arguments nginx)
         )
        ((#:phases phases)
          `(modify-phases ,phases
           (add-after 'unpack 'unpack-nginx-sources
             (lambda* (#:key inputs native-inputs #:allow-other-keys)
               (begin
                 ;; The nginx source code is part of the module’s source.
                 (format #t "decompressing nginx source code~%")
                 (let ((tar (assoc-ref inputs "tar")) (nginx-srcs (assoc-ref inputs "nginx-sources")))
                      (invoke (string-append tar "/bin/tar") "xvf" nginx-srcs "--strip-components=1")
                 )
                 #t
                )
             )
           )
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let ((modules-dir (string-append (assoc-ref outputs "out") "/etc/nginx/modules")))
                    (install-file "objs/ngx_http_geoip2_module.so" modules-dir)
                    (install-file "objs/ngx_stream_geoip2_module.so" modules-dir)
               #t
               )
             )
           )
           (delete 'fix-root-dirs)
           (delete 'install-man-page)
          )
        )
      )
    )
  )
)

