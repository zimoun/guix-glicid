(define-module (glicid packages openldap)
 #:use-module (guix build-system gnu)
 #:use-module (guix download)
 #:use-module ((guix licenses) #:prefix license:)
 #:use-module (guix packages)
 #:use-module (guix utils)
 #:use-module (gnu packages autotools)
 #:use-module (gnu packages compression)
 #:use-module (gnu packages cyrus-sasl)
 #:use-module (gnu packages databases)
 #:use-module (gnu packages dbm)
 #:use-module (gnu packages gnupg)
 #:use-module (gnu packages groff)
 #:use-module (gnu packages kerberos)
 #:use-module (gnu packages libevent)
 #:use-module (gnu packages linux)
 #:use-module ((gnu packages openldap) #:prefix gnu:)
 #:use-module (gnu packages password-utils)
 #:use-module (gnu packages perl)
 #:use-module (gnu packages python)
 #:use-module (gnu packages pkg-config)
 #:use-module (gnu packages tls)
)

(define-public openldap
  (let* ((openldap-minimal gnu:openldap-2.6))
    (package
      (inherit openldap-minimal)
      (name (string-append (package-name openldap-minimal) "-more"))
      (arguments
       (substitute-keyword-arguments (package-arguments openldap-minimal)
         ((#:configure-flags flags)
          `(append (list
                    "--enable-aci"
                    "--enable-argon2"
                    "--enable-backends=mod"
                    "--enable-balancer"
                    "--enable-cleartext"
                    "--enable-crypt"
                    "--enable-debug"
                    "--enable-dynacl"
                    "--enable-modules"
                    "--enable-ipv6"
                    "--enable-local"
                    "--enable-overlays=mod"
                    "--enable-rlookups"
                    "--enable-shared"
                    "--enable-slapd"
                    "--enable-slapi"
                    "--enable-spasswd"
                    "--enable-syslog"
                    "--with-tls=openssl")
                   ,flags))))
      (inputs (modify-inputs (package-inputs openldap-minimal)
                (delete "gnutls")
                (append argon2
                        libevent
                        libltdl
                        lz4
                        openssl
                        perl
                        snappy
                        unixodbc
                        wiredtiger)))
      (native-inputs (modify-inputs (package-native-inputs openldap-minimal)
                       (append pkg-config))))))

(define (make-openldap version checksum)
  (package
    (inherit openldap)
    (version version)
    (source (origin
              (method url-fetch)
              ;; See <http://www.openldap.org/software/download/> for a list of
             ;; mirrors.
             (uri (list (string-append
                         "ftp://mirror.switch.ch/mirror/OpenLDAP/"
                         "openldap-release/openldap-" version ".tgz")
                        (string-append
                         "https://www.openldap.org/software/download/OpenLDAP/"
                         "openldap-release/openldap-" version ".tgz")
                        (string-append
                         "ftp://ftp.dti.ad.jp/pub/net/OpenLDAP/"
                         "openldap-release/openldap-" version ".tgz")))
              (sha256 (base32 checksum))))))

(define-public openldap-2.5.7
  (make-openldap
   "2.5.7"
   "1ayr76sa5hjwldqzis5v71sbp88hd3hysc00gw1raqn33c05g5za"))

(define-public openldap-2.5.9
  (make-openldap
   "2.5.9"
   "17pvwrj27jybbmjqpv0p7kd2qa4i6jnp134lz7cxa0sqrbs153n0"))

(define-public openldap-2.6.0
  (make-openldap
   "2.6.0"
   "0kqswk8pxgnlibh0kz6py3a2x3yh9pfk6pyr2nx9lgjpmh75h75p"))

(define-public nss-pam-ldapd
  (package
    (name "nss-pam-ldapd")
    (version "0.9.12")
    (source (origin
      (method url-fetch)
      (uri (string-append "https://arthurdejong.org/nss-pam-ldapd/nss-pam-ldapd-" version ".tar.gz"))
      (sha256 (base32 "050fzcmxmf6y15dlcffc4gxr3wkk7fliqqwhlwqzbjwk8vkn3mn6"))
    ))
    (build-system gnu-build-system)
    (arguments
     `(
        #:configure-flags
        (list (string-append
          "--with-pam-seclib-dir=" (assoc-ref %outputs "out") "/lib/security/")
          "--with-ldap-conf-file=/etc/nslcd.conf"
        )
        #:phases
        (modify-phases %standard-phases
          (add-after 'unpack 'override-nslcd.conf-install-path
            (lambda* (#:key outputs #:allow-other-keys)
              (substitute* "Makefile.in"
                (
                  ("\\$\\(DESTDIR\\)\\$\\(NSLCD_CONF_PATH\\)")
                  (string-append (assoc-ref outputs "out") "/etc/nslcd.conf.example")
                )
              )
            )
          )
        )
      )
    )
    (inputs `(
      ("linux-pam", linux-pam)
      ("mit-krb5", mit-krb5)
      ("openldap", openldap)
      ("python", python)
    ))
    (home-page "https://arthurdejong.org/nss-pam-ldapd")
    (synopsis "NSS and PAM modules for LDAP")
    (description "nss-pam-ldapd provides a @dfn{Name Service Switch} (NSS)
module that allows your LDAP server to provide user account, group, host name,
alias, netgroup, and basically any other information that you would normally
get from @file{/etc} flat files or NIS.  It also provides a @dfn{Pluggable
Authentication Module} (PAM) to do identity and authentication management with
an LDAP server.")
    (license license:lgpl2.1+)
  )
)
