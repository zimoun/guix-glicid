(define-module (glicid packages parallel)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages parallel)
  #:use-module (gnu packages gtk)  
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages mpi)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages databases)
)

(define-public openpmix-3.1.5
  (package
    (name "openpmix")
    (version "3.1.5")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/openpmix/openpmix/releases/download/v" version "/pmix-" version ".tar.bz2"))
        (sha256 (base32 "1xswdkfcrw123ghcr9gmrb852nzjbl4i6qwrid3xyma42yal34w8"))
      )
    )
    (build-system gnu-build-system)
    (arguments
      `(#:configure-flags
        (list
          (string-append "--with-hwloc=" (assoc-ref %build-inputs "hwloc"))
        )
       )
    )
    (synopsis "MPIX lib")
    (description"MPIX. More to come. FIXIT")
    (home-page "https://www.gnu.org/software/hello/")
    (license license:gpl3+)
    (inputs `(
      ("libevent", libevent)
      ("hwloc" ,hwloc-2 "lib")
      ("perl", perl)
    ))
  )
)

(define-public openpmix-4.1.0
  (package
    (inherit openpmix-3.1.5)
    (version "4.1.0")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append "https://github.com/openpmix/openpmix/releases/download/v" version "/pmix-" version ".tar.bz2"))
        (sha256 (base32 "0bl4gkh87csm3yh418wvrih35mg6swavcxj78gyb7gr1qsk0apql"))
      )
    )
    (inputs `(
      ("python", python)
      ("zlib", zlib)
      ("pandoc", pandoc)
      ,@(package-inputs openpmix-3.1.5)
    ))
  )
)

(define-public openpmix openpmix-4.1.0)

(define-public slurm-20.02-glicid
  (package 
    (inherit slurm-20.02)
    (name "slurm-glicid")
    (version "20.02.7")
    (source 
      (origin
        (inherit (package-source slurm-20.02))
        (method url-fetch)
        (uri (string-append "https://download.schedmd.com/slurm/slurm-" version ".tar.bz2"))
        (sha256 (base32 "1khlv69q41chgkcs1i7l651hvyx8sz3j9yhjbgky3gpqrgrmz1h6"))
      )
    )
    (inputs `(
      ("gtk+-2", gtk+-2)
      ("mariadb:dev", mariadb "dev")
      ,@(package-inputs slurm-20.02)
    ))
  )
)

(define-public slurm-20.11-glicid
  (package 
    (inherit slurm-20.02-glicid)
    (name "slurm-glicid")
    (version "20.11.7")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append "https://download.schedmd.com/slurm/slurm-" version ".tar.bz2"))
        (sha256 (base32 "1fdjihg1x7ks5l77yjv14a4mg6r0v8c3zk1dcxkhrhq3n4dc9nbs"))
      )
    )
  )
)

(define-public slurm-glicid slurm-20.11-glicid)
(define-public slurm-ccipl slurm-20.02-glicid)
