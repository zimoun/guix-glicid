(define-module (glicid packages perl)
 #:use-module (guix build-system perl)
 #:use-module (guix download)
 #:use-module (guix packages)
 #:use-module (guix utils)
)

(define-public perl-extutils-embed
  (package
    (name "perl-extutils-embed")
    (version "1.14")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "mirror://cpan/authors/id/D/DO/DOUGM/ExtUtils-Embed-" version ".tar.gz"))
        (sha256 (base32 "1w55q0wz6i4k9p500mc4wmr8jndnczb6qqrd2m1mrnx13v2mbr7h"))
      )
    )
    (build-system perl-build-system)
    (arguments '(
      #:tests? #f
    ))
    (home-page "https://metacpan.org/release/ExtUtils-Embed")
    (synopsis "Utilities for embedding Perl in C/C++ applications")
    (description "Utilities for embedding Perl in C/C++ applications")
    (license #f)
  )
)
