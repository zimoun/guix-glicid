(define-module (glicid packages rust)
 #:use-module (guix download)
 #:use-module (guix packages)
 #:use-module (guix build-system cargo)
 #:use-module (gnu packages crates-io)
 #:use-module (gnu packages crates-graphics)
 #:use-module ((guix licenses) #:prefix license:)
)

(define rust-clippy-0.0.302 rust-clippy-0.0)
(define rust-unicode-xid-0.0.4 rust-unicode-xid-0.0)

(define-public rust-sieve-0.1
  (package
    (name "rust-sieve")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sieve" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "141wa6assczj8asmjw8bca6zj94v3xi3614814sdwc7xp2m8kr0r"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/bemeurer/sieve")
    (synopsis "Fast segmented sieve of Erasthotenes implemented in Rust")
    (description "Fast segmented sieve of Erasthotenes implemented in Rust")
    (license license:bsd-3)))



(define-public rust-rlimit-0.4
  (package
    (name "rust-rlimit")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rlimit" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0dhm9jm1bvm4fk3839jw95gxs99yg0cwl9awwkyaclw3qdi2vc29"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-0.1) ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/Nugine/rlimit/")
    (synopsis "Resource limits")
    (description "Resource limits")
    (license license:expat)))

(define-public rust-pretty-assertions-1
  (package
    (name "rust-pretty-assertions")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "pretty_assertions" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0l2xpgqa1a73fkbacn0qxngixwmyp1fb90k496sql095nx4bbmbn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-ansi-term" ,rust-ansi-term-0.12)
         ("rust-ctor" ,rust-ctor-0.1)
         ("rust-diff" ,rust-diff-0.1)
         ("rust-output-vt100" ,rust-output-vt100-0.1))))
    (home-page "https://github.com/colin-kiegel/rust-pretty-assertions")
    (synopsis
      "Overwrite `assert_eq!` and `assert_ne!` with drop-in replacements, adding colorful diffs.")
    (description
      "Overwrite `assert_eq!` and `assert_ne!` with drop-in replacements, adding
colorful diffs.")
    (license (list license:expat license:asl2.0))))

(define-public rust-uu-yes-0.0.12
  (package
    (name "rust-uu-yes")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_yes" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fl8mdsv3v3zl4pv4d8pk573ylmlc6b9qz6dwb1661gdbmlk89nv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "yes ~ (uutils) repeatedly display a line with STRING (or 'y')")
    (description
      "yes ~ (uutils) repeatedly display a line with STRING (or 'y')")
    (license license:expat)))

(define-public rust-uu-whoami-0.0.12
  (package
    (name "rust-uu-whoami")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_whoami" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ykg4k1bnk26f816qbjbfhkhgljh9iffw3qld6qffmfah1398xy0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "whoami ~ (uutils) display user name of current effective user ID")
    (description
      "whoami ~ (uutils) display user name of current effective user ID")
    (license license:expat)))

(define-public rust-uu-who-0.0.12
  (package
    (name "rust-uu-who")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_who" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "14wilyf8qys4c7na5dfpyr5c14kcq1idznn4wcjd3ypw52q6hcli"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "who ~ (uutils) display information about currently logged-in users")
    (description
      "who ~ (uutils) display information about currently logged-in users")
    (license license:expat)))

(define-public rust-utf-8-0.7
  (package
    (name "rust-utf-8")
    (version "0.7.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "utf-8" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1a9ns3fvgird0snjkd3wbdhwd3zdpc2h5gpyybrfr6ra5pkqxk09"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/SimonSapin/rust-utf8")
    (synopsis "Incremental, zero-copy UTF-8 decoding with error handling")
    (description "Incremental, zero-copy UTF-8 decoding with error handling")
    (license (list license:expat license:asl2.0))))

(define-public rust-bytecount-0.6
  (package
    (name "rust-bytecount")
    (version "0.6.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bytecount" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0bklbbl5ml9ic18s9kn5iix1grrqc6sypz6hvfn8sjc6zhgv7zkj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-packed-simd-2" ,rust-packed-simd-2-0.3))))
    (home-page "https://github.com/llogiq/bytecount")
    (synopsis
      "count occurrences of a given byte, or the number of UTF-8 code points, in a byte slice, fast")
    (description
      "count occurrences of a given byte, or the number of UTF-8 code points, in a byte
slice, fast")
    (license (list license:asl2.0 license:expat))))

(define-public rust-uu-wc-0.0.12
  (package
    (name "rust-uu-wc")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_wc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0whss8lz770pkwdn7z4y8rh42r0mmvkx91d4yrwrhw7z32cfdzhd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bytecount" ,rust-bytecount-0.6)
         ("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-utf-8" ,rust-utf-8-0.7)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "wc ~ (uutils) display newline, word, and byte counts for input")
    (description
      "wc ~ (uutils) display newline, word, and byte counts for input")
    (license license:expat)))

(define-public rust-uu-users-0.0.12
  (package
    (name "rust-uu-users")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_users" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0d58v8rhh37x6qs1z8i183xm4m7hvfcabjpz70ab7hvsdaxb8da3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "users ~ (uutils) display names of currently logged-in users")
    (description "users ~ (uutils) display names of currently logged-in users")
    (license license:expat)))

(define-public rust-uu-uptime-0.0.12
  (package
    (name "rust-uu-uptime")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_uptime" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zh8m7ary132x5p2kra00ri0jyab8b5wgm126j1frbxn3riqsf3i"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chrono" ,rust-chrono-0.4)
         ("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "uptime ~ (uutils) display dynamic system information")
    (description "uptime ~ (uutils) display dynamic system information")
    (license license:expat)))

(define-public rust-uu-unlink-0.0.12
  (package
    (name "rust-uu-unlink")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_unlink" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ks54hh3691mycb6zzqak9s05a80l07mibmqvrismcy9b5kd0zyx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "unlink ~ (uutils) remove a (file system) link to FILE")
    (description "unlink ~ (uutils) remove a (file system) link to FILE")
    (license license:expat)))

(define-public rust-uu-uniq-0.0.12
  (package
    (name "rust-uu-uniq")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_uniq" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0rskys26z41h5jl0vy1aywajhll67p5drv2xifs08gnjnz443imq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-strum" ,rust-strum-0.21)
         ("rust-strum-macros" ,rust-strum-macros-0.21)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "uniq ~ (uutils) filter identical adjacent lines from input")
    (description "uniq ~ (uutils) filter identical adjacent lines from input")
    (license license:expat)))

(define-public rust-uu-unexpand-0.0.12
  (package
    (name "rust-uu-unexpand")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_unexpand" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qg2bjan33m4ar03z9yw4sivis8hz370shnj7mlcwqi4mxj5k9nv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "unexpand ~ (uutils) convert input spaces to tabs")
    (description "unexpand ~ (uutils) convert input spaces to tabs")
    (license license:expat)))

(define-public rust-uu-uname-0.0.12
  (package
    (name "rust-uu-uname")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_uname" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05xx2yzz4wvavvac842jr1i8q8xjbw5y4r2b2wwg5m83jxf42n4m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-platform-info" ,rust-platform-info-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "uname ~ (uutils) display system information")
    (description "uname ~ (uutils) display system information")
    (license license:expat)))

(define-public rust-uu-tty-0.0.12
  (package
    (name "rust-uu-tty")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_tty" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c44jqn56zmy58rc3rskl91cdbyhkm8g8zjs2rfc5ylybq4vif5j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "tty ~ (uutils) display the name of the terminal connected to standard input")
    (description
      "tty ~ (uutils) display the name of the terminal connected to standard input")
    (license license:expat)))

(define-public rust-uu-tsort-0.0.12
  (package
    (name "rust-uu-tsort")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_tsort" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1r2i7hjq7x2gf0ny171iix0r264lw90i2yic99savhi8rn2d19fv"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "tsort ~ (uutils) topologically sort input (partially ordered) pairs")
    (description
      "tsort ~ (uutils) topologically sort input (partially ordered) pairs")
    (license license:expat)))

(define-public rust-uu-truncate-0.0.12
  (package
    (name "rust-uu-truncate")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_truncate" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "177c7mv0xc3vkchli09pixvm52grv3fmqh6ja2pmb581mwy9888x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "truncate ~ (uutils) truncate (or extend) FILE to SIZE")
    (description "truncate ~ (uutils) truncate (or extend) FILE to SIZE")
    (license license:expat)))

(define-public rust-uu-true-0.0.12
  (package
    (name "rust-uu-true")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_true" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gg01lds5hflbc4hh3g75l3l2z8c4pc1bbbs78hp3dbchq4wmzwf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "true ~ (uutils) do nothing and succeed")
    (description "true ~ (uutils) do nothing and succeed")
    (license license:expat)))

(define-public rust-uu-tr-0.0.12
  (package
    (name "rust-uu-tr")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_tr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "099943mpvmwgpkxiwk1dcbsgqx1vxg0h783kslfv72gs50wvr9ix"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bit-set" ,rust-bit-set-0.5)
         ("rust-clap" ,rust-clap-2)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "tr ~ (uutils) translate characters within input and display")
    (description "tr ~ (uutils) translate characters within input and display")
    (license license:expat)))

(define-public rust-uu-touch-0.0.12
  (package
    (name "rust-uu-touch")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_touch" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zhk66ckdalrvafyij3akqffpi0lin1cig6i4n0dkrcqwzzzrqnc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-filetime" ,rust-filetime-0.2)
         ("rust-time" ,rust-time-0.1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "touch ~ (uutils) change FILE timestamps")
    (description "touch ~ (uutils) change FILE timestamps")
    (license license:expat)))

(define-public rust-uu-timeout-0.0.12
  (package
    (name "rust-uu-timeout")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_timeout" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0i7zn9qwc62c7yf8ynnyv32r533a6g654ji0ck16vk6nj4044gaz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "timeout ~ (uutils) run COMMAND with a DURATION time limit")
    (description "timeout ~ (uutils) run COMMAND with a DURATION time limit")
    (license license:expat)))

(define-public rust-uu-test-0.0.12
  (package
    (name "rust-uu-test")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_test" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1ggwfm3d67h5dbsf68dipj5hkqsdkbp9k0vjs465s73ad6l0gx9f"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "test ~ (uutils) evaluate comparison and file type expressions")
    (description
      "test ~ (uutils) evaluate comparison and file type expressions")
    (license license:expat)))

(define-public rust-retain-mut-0.1
  (package
    (name "rust-retain-mut")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "retain_mut" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18dvv6lmgw1xx29qjlhaazmhpvxvnc50iw1y40d3zqg195n2qmak"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/upsuper/retain_mut")
    (synopsis
      "Provide retain_mut method that has the same functionality as retain but gives mutable borrow to the predicate.")
    (description
      "Provide retain_mut method that has the same functionality as retain but gives
mutable borrow to the predicate.")
    (license license:expat)))

(define-public rust-uu-tee-0.0.12
  (package
    (name "rust-uu-tee")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_tee" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ii7xsmgyrps21azhg3b1dygvj4m3lns06j6bvys52ba84vpfmqi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-retain-mut" ,rust-retain-mut-0.1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "tee ~ (uutils) display input and copy to FILE")
    (description "tee ~ (uutils) display input and copy to FILE")
    (license license:expat)))

(define-public rust-uu-tail-0.0.12
  (package
    (name "rust-uu-tail")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_tail" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b80l1346xf3g8fycylafxgiqq2ygvkv42b31wmvrzx0mza3zsw3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "tail ~ (uutils) display the last lines of input")
    (description "tail ~ (uutils) display the last lines of input")
    (license license:expat)))

(define-public rust-uu-tac-0.0.12
  (package
    (name "rust-uu-tac")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_tac" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zks0dpaszl8i0br64d8n44l05mx1hl6s5gm87ys7afalpgdsjz1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-memmap2" ,rust-memmap2-0.5)
         ("rust-regex" ,rust-regex-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "tac ~ (uutils) concatenate and display input lines in reverse order")
    (description
      "tac ~ (uutils) concatenate and display input lines in reverse order")
    (license license:expat)))

(define-public rust-uu-sync-0.0.12
  (package
    (name "rust-uu-sync")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_sync" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vzjrnkxmh6v277vn5rr3m0q09cz8yvvdxh9yzqzvbgpsaz7rhw5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "sync ~ (uutils) synchronize cache writes to storage")
    (description "sync ~ (uutils) synchronize cache writes to storage")
    (license license:expat)))

(define-public rust-uu-sum-0.0.12
  (package
    (name "rust-uu-sum")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_sum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "055dv5rxl25f82b521p6kjwwqndahfzvd4fr811dz2s62c6n95ii"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "sum ~ (uutils) display checksum and block counts for input")
    (description "sum ~ (uutils) display checksum and block counts for input")
    (license license:expat)))

(define-public rust-cpp-synmap-0.3
  (package
    (name "rust-cpp-synmap")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_synmap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1dlh5r7i051j7p4v4mq0jwm9xf6qwn772lzy7zflx1z2vff4yzl9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cpp-syn" ,rust-cpp-syn-0.12)
         ("rust-cpp-synom" ,rust-cpp-synom-0.12)
         ("rust-memchr" ,rust-memchr-1))))
    (home-page "https://github.com/mystor/rust-cpp")
    (synopsis "Sourcemap and full crate parsing support for `cpp_syn`")
    (description "Sourcemap and full crate parsing support for `cpp_syn`")
    (license (list license:expat license:asl2.0))))

(define-public rust-cpp-synom-0.12
  (package
    (name "rust-cpp-synom")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_synom" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vbvqrbhkwqfl49g7iw0jsjd0nl3fy7i31f7a1hn8fr3jibdmj0z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-xid" ,rust-unicode-xid-0.0.4))))
    (home-page "https://github.com/mystor/cpp_syn")
    (synopsis "Stripped-down Nom parser used by cpp_syn")
    (description "Stripped-down Nom parser used by cpp_syn")
    (license (list license:expat license:asl2.0))))

(define-public rust-cpp-syn-0.12
  (package
    (name "rust-cpp-syn")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_syn" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0kk4y7ad8hajpxladn1y0fk3i9gmk1v0r9hjzs94v05kyndn9kd8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clippy" ,rust-clippy-0.0.302)
         ("rust-cpp-synom" ,rust-cpp-synom-0.12)
         ("rust-quote" ,rust-quote-0.3)
         ("rust-unicode-xid" ,rust-unicode-xid-0.0.4))))
    (home-page "https://github.com/mystor/cpp_syn")
    (synopsis "Internal rust-cpp nom parser for Rust source code")
    (description "Internal rust-cpp nom parser for Rust source code")
    (license (list license:expat license:asl2.0))))

(define-public rust-cpp-common-0.4
  (package
    (name "rust-cpp-common")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_common" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0a6n4x01q3wz4bp86n4dqyf142l4l9y38vjv5yhgyflllx4r3qvr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cpp-syn" ,rust-cpp-syn-0.12)
         ("rust-cpp-synom" ,rust-cpp-synom-0.12)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-quote" ,rust-quote-0.3))))
    (home-page "https://github.com/mystor/rust-cpp")
    (synopsis "Implementation details crate for the `cpp` crate")
    (description "Implementation details crate for the `cpp` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-cpp-build-0.4
  (package
    (name "rust-cpp-build")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_build" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0if4a6s7lbld96ay5dfb5hf5n6jy9xwjk1q94x4asclmw3kk2xf4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-cpp-common" ,rust-cpp-common-0.4)
         ("rust-cpp-syn" ,rust-cpp-syn-0.12)
         ("rust-cpp-synmap" ,rust-cpp-synmap-0.3)
         ("rust-cpp-synom" ,rust-cpp-synom-0.12)
         ("rust-lazy-static" ,rust-lazy-static-1))))
    (home-page "https://github.com/mystor/rust-cpp")
    (synopsis "Cargo build script for the `cpp` crate")
    (description "Cargo build script for the `cpp` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-if-rust-version-1
  (package
    (name "rust-if-rust-version")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "if_rust_version" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1v6mj3vqy5g0x7gpyg5kiivm42qqgras69cxb0hrg4w67qrwpns6"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/ogoffart/if_rust_version")
    (synopsis "Macro to enable or disable code depending on the rust version")
    (description
      "Macro to enable or disable code depending on the rust version")
    (license (list license:expat license:asl2.0))))

(define-public rust-cpp-common-0.5
  (package
    (name "rust-cpp-common")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_common" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10zj9z639f7j1ccycix8k73j0ic77cyznyb7062l50gywllasy6z"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/mystor/rust-cpp")
    (synopsis "Implementation details crate for the `cpp` crate")
    (description "Implementation details crate for the `cpp` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-cpp-macros-0.5
  (package
    (name "rust-cpp-macros")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp_macros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "09gwm3j61y2y5ma1s55qd12pl7rfjnjzyqxv9v4an44cc4ga54sg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-0.7)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-cpp-common" ,rust-cpp-common-0.5)
         ("rust-if-rust-version" ,rust-if-rust-version-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/mystor/rust-cpp")
    (synopsis "Procedural macro implementation for the `cpp` crate")
    (description "Procedural macro implementation for the `cpp` crate")
    (license (list license:expat license:asl2.0))))

(define-public rust-cpp-0.5
  (package
    (name "rust-cpp")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cpp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1s2hpy068k8461mdxpl4a7lm41ry9887zq3frg4xqj5y023a0xa8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cpp-macros" ,rust-cpp-macros-0.5))))
    (home-page "https://github.com/mystor/rust-cpp")
    (synopsis "Inline C++ code closures")
    (description "Inline C++ code closures")
    (license (list license:expat license:asl2.0))))

(define-public rust-uu-stdbuf-libstdbuf-0.0.12
  (package
    (name "rust-uu-stdbuf-libstdbuf")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_stdbuf_libstdbuf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17bjkrhvkrprv2kbgdh233iws7pvn72fhdqvy3sqi13ajbw95m8g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cpp" ,rust-cpp-0.5)
         ("rust-cpp-build" ,rust-cpp-build-0.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "stdbuf/libstdbuf ~ (uutils); dynamic library required for stdbuf")
    (description
      "stdbuf/libstdbuf ~ (uutils); dynamic library required for stdbuf")
    (license license:expat)))

(define-public rust-uu-stdbuf-0.0.12
  (package
    (name "rust-uu-stdbuf")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_stdbuf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "02vqfdf0lhs6yj6xx0mdfsrssdpzpgjbyww1js4kbw59i5kzcggy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-uu-stdbuf-libstdbuf" ,rust-uu-stdbuf-libstdbuf-0.0.12)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "stdbuf ~ (uutils) run COMMAND with modified standard stream buffering")
    (description
      "stdbuf ~ (uutils) run COMMAND with modified standard stream buffering")
    (license license:expat)))

(define-public rust-uu-stat-0.0.12
  (package
    (name "rust-uu-stat")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_stat" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "07cifs7ksbdq3zqqqd2bdj63n5758rrj47nx1x7zjk6965l8fsqa"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "stat ~ (uutils) display FILE status")
    (description "stat ~ (uutils) display FILE status")
    (license license:expat)))

(define-public rust-uu-split-0.0.12
  (package
    (name "rust-uu-split")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_split" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1d1723zx5cw8l294fgm3b21ggh8faj6i6lbhrylwvm5lh2bgviqw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "split ~ (uutils) split input into output files")
    (description "split ~ (uutils) split input into output files")
    (license license:expat)))

(define-public rust-ouroboros-macro-0.10
  (package
    (name "rust-ouroboros-macro")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ouroboros_macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0s6aic49lyclhas6bh1f84qfy31m333mcvnmn4v02v5rdrx8aqzl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-inflector" ,rust-inflector-0.11)
         ("rust-proc-macro-error" ,rust-proc-macro-error-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/joshua-maros/ouroboros")
    (synopsis "Proc macro for ouroboros crate.")
    (description "Proc macro for ouroboros crate.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ouroboros-0.10
  (package
    (name "rust-ouroboros")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ouroboros" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1phbp9wjp36bvkwlyvr2zznaack6xcvg0z1869r3i33iy5j6s8w4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aliasable" ,rust-aliasable-0.1)
         ("rust-ouroboros-macro" ,rust-ouroboros-macro-0.10)
         ("rust-stable-deref-trait" ,rust-stable-deref-trait-1))))
    (home-page "https://github.com/joshua-maros/ouroboros")
    (synopsis "Easy, safe self-referential struct generation.")
    (description "Easy, safe self-referential struct generation.")
    (license (list license:expat license:asl2.0))))

(define-public rust-compare-0.1
  (package
    (name "rust-compare")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "compare" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1lv84g7l04vc1g54z5sigz330xklhkljwl165vz7xi1fvga3608j"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/contain-rs/compare")
    (synopsis "Experimental comparators for collections to be generic over")
    (description "Experimental comparators for collections to be generic over")
    (license (list license:expat license:asl2.0))))

(define-public rust-binary-heap-plus-0.4
  (package
    (name "rust-binary-heap-plus")
    (version "0.4.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "binary-heap-plus" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mq6qs7lyckjjazj7apcndzhwhgz3r0nmrk1jf5137pzz0w8c1jg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-compare" ,rust-compare-0.1) ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/sekineh/binary-heap-plus-rs")
    (synopsis
      "Enhanced version of std::collections::BinaryHeap that supports max, min, and custom-order heaps.")
    (description
      "Enhanced version of std::collections::BinaryHeap that supports max, min, and
custom-order heaps.")
    (license license:expat)))

(define-public rust-uu-sort-0.0.12
  (package
    (name "rust-uu-sort")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_sort" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1g4v5dbykn0l5cfa2ddsnslr91i0v1kpj4266ys5iarpvj2k8hmi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-binary-heap-plus" ,rust-binary-heap-plus-0.4)
         ("rust-clap" ,rust-clap-2)
         ("rust-compare" ,rust-compare-0.1)
         ("rust-ctrlc" ,rust-ctrlc-3)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-itertools" ,rust-itertools-0.10)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-ouroboros" ,rust-ouroboros-0.10)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "sort ~ (uutils) sort input lines")
    (description "sort ~ (uutils) sort input lines")
    (license license:expat)))

(define-public rust-uu-sleep-0.0.12
  (package
    (name "rust-uu-sleep")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_sleep" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0j3x5bqk66xhrx3jw0w49lz7grml4hhvg5s93hnmgz0v6cas51n0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "sleep ~ (uutils) pause for DURATION")
    (description "sleep ~ (uutils) pause for DURATION")
    (license license:expat)))

(define-public rust-uu-shuf-0.0.12
  (package
    (name "rust-uu-shuf")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_shuf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0fgr70yvmirwg1nnjwc62vrb8x676r2w33q7jiwjpx0lffv8rj50"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-rand" ,rust-rand-0.5)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "shuf ~ (uutils) display random permutations of input lines")
    (description "shuf ~ (uutils) display random permutations of input lines")
    (license license:expat)))

(define-public rust-uu-shred-0.0.12
  (package
    (name "rust-uu-shred")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_shred" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zlynd918vqg6ag9zch2wxxxnlb14xhjzwwxwl1xggk2wm1gy7ym"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "shred ~ (uutils) hide former FILE contents with repeated overwrites")
    (description
      "shred ~ (uutils) hide former FILE contents with repeated overwrites")
    (license license:expat)))

(define-public rust-uu-seq-0.0.12
  (package
    (name "rust-uu-seq")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_seq" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0y3rpv14w8lm3vhwzv6wq3nlshhy17f457dzdc9w8z5yarhcm62g"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bigdecimal" ,rust-bigdecimal-0.3)
         ("rust-clap" ,rust-clap-2)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "seq ~ (uutils) display a sequence of numbers")
    (description "seq ~ (uutils) display a sequence of numbers")
    (license license:expat)))

(define-public rust-uu-runcon-0.0.12
  (package
    (name "rust-uu-runcon")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_runcon" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04p5y2gap5p0160j9rfnjz2kd35dsl2146wy62qdxlxvq3pqqdkh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-fts-sys" ,rust-fts-sys-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-selinux" ,rust-selinux-0.2)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "runcon ~ (uutils) run command with specified security context")
    (description
      "runcon ~ (uutils) run command with specified security context")
    (license license:expat)))

(define-public rust-uu-rmdir-0.0.12
  (package
    (name "rust-uu-rmdir")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_rmdir" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0b2kqmk6w6a5xh514ixwdyzg17b4izjnnia4l0jwmp93a1k5qwi6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "rmdir ~ (uutils) remove empty DIRECTORY")
    (description "rmdir ~ (uutils) remove empty DIRECTORY")
    (license license:expat)))

(define-public rust-uu-rm-0.0.12
  (package
    (name "rust-uu-rm")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_rm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1m0c3ic220bvgkr4fwvangja4zx2kca43xnksw4mkf8kd37llb89"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-remove-dir-all" ,rust-remove-dir-all-0.5)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "rm ~ (uutils) remove PATHNAME")
    (description "rm ~ (uutils) remove PATHNAME")
    (license license:expat)))

(define-public rust-uu-relpath-0.0.12
  (package
    (name "rust-uu-relpath")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_relpath" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1bcvbs2l7wgllvsgq3c205z6mshwmwsbmy4pl5lcvf8njl9gi1cp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "relpath ~ (uutils) display relative path of PATHNAME_TO from PATHNAME_FROM")
    (description
      "relpath ~ (uutils) display relative path of PATHNAME_TO from PATHNAME_FROM")
    (license license:expat)))

(define-public rust-uu-realpath-0.0.12
  (package
    (name "rust-uu-realpath")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_realpath" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1msgb9z1gn9pvmlx11lv40p53j8nnh9anrdw8ccckc38xhq82j3l"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "realpath ~ (uutils) display resolved absolute path of PATHNAME")
    (description
      "realpath ~ (uutils) display resolved absolute path of PATHNAME")
    (license license:expat)))

(define-public rust-uu-readlink-0.0.12
  (package
    (name "rust-uu-readlink")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_readlink" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "039qf6k707lfvvlggja4fwrivyh9g1ambryyj1mqyvn7ylvba7c2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "readlink ~ (uutils) display resolved path of PATHNAME")
    (description "readlink ~ (uutils) display resolved path of PATHNAME")
    (license license:expat)))

(define-public rust-uu-pwd-0.0.12
  (package
    (name "rust-uu-pwd")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_pwd" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "149w4ikd9gparqb57bgyd6i2ppvz4pbvm3dxsgv4v2kbgshyx4vr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "pwd ~ (uutils) display current working directory")
    (description "pwd ~ (uutils) display current working directory")
    (license license:expat)))

(define-public rust-uu-ptx-0.0.12
  (package
    (name "rust-uu-ptx")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_ptx" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0c3irsbcbwl61rkssbfcw7bbv1fxrgnr94b8hybw0r9dc9ydbpp7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-0.7)
         ("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex" ,rust-regex-1)
         ("rust-regex-syntax" ,rust-regex-syntax-0.6)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "ptx ~ (uutils) display a permuted index of input")
    (description "ptx ~ (uutils) display a permuted index of input")
    (license license:expat)))

(define-public rust-uu-printf-0.0.12
  (package
    (name "rust-uu-printf")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_printf" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04d5n0v5ca71wfiy4lz9zd8abm6585l36nvgfirhbq1py3ad6vlc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-itertools" ,rust-itertools-0.8)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "printf ~ (uutils) FORMAT and display ARGUMENTS")
    (description "printf ~ (uutils) FORMAT and display ARGUMENTS")
    (license license:expat)))

(define-public rust-uu-printenv-0.0.12
  (package
    (name "rust-uu-printenv")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_printenv" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10yn8gkq7q303j57vbn8f0j5gzl8xskn2iwc7c0ikl1c6qk13x78"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "printenv ~ (uutils) display value of environment VAR")
    (description "printenv ~ (uutils) display value of environment VAR")
    (license license:expat)))

(define-public rust-uu-pr-0.0.12
  (package
    (name "rust-uu-pr")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_pr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0djbn35agn8dm5zdqy2g96vqvnv0wvgjs1gxnili73fgw3xpm85m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chrono" ,rust-chrono-0.4)
         ("rust-clap" ,rust-clap-2)
         ("rust-getopts" ,rust-getopts-0.2)
         ("rust-itertools" ,rust-itertools-0.10)
         ("rust-quick-error" ,rust-quick-error-2)
         ("rust-regex" ,rust-regex-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "pr ~ (uutils) convert text files for printing")
    (description "pr ~ (uutils) convert text files for printing")
    (license license:expat)))

(define-public rust-uu-pinky-0.0.12
  (package
    (name "rust-uu-pinky")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_pinky" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1x7iib6n9rxhj3vza3542j6dzpkzzzarp92lsw3k1prk2155nlq1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "pinky ~ (uutils) display user information")
    (description "pinky ~ (uutils) display user information")
    (license license:expat)))

(define-public rust-uu-pathchk-0.0.12
  (package
    (name "rust-uu-pathchk")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_pathchk" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1j34ig3p8lnr4zd4j39hs75b2yx2fcf8s35vi3c36y6afkbc47wp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "pathchk ~ (uutils) diagnose invalid or non-portable PATHNAME")
    (description
      "pathchk ~ (uutils) diagnose invalid or non-portable PATHNAME")
    (license license:expat)))

(define-public rust-uu-paste-0.0.12
  (package
    (name "rust-uu-paste")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_paste" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mhppmw186xqdbrgqsal31rwr0ynn532a8wlm4p5b2nlqwrxgqy3"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "paste ~ (uutils) merge lines from inputs")
    (description "paste ~ (uutils) merge lines from inputs")
    (license license:expat)))

(define-public rust-uu-od-0.0.12
  (package
    (name "rust-uu-od")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_od" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1wxch9wsvcfj266qv5m507sy52wvm7rq5aba9nhgdqvkqslkmkw4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1)
         ("rust-clap" ,rust-clap-2)
         ("rust-half" ,rust-half-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "od ~ (uutils) display formatted representation of input")
    (description "od ~ (uutils) display formatted representation of input")
    (license license:expat)))

(define-public rust-uu-numfmt-0.0.12
  (package
    (name "rust-uu-numfmt")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_numfmt" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0alx0wpdkv5qkm8j769w947dcf1qnp4mqln0slknhw8cqabr2vjd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "numfmt ~ (uutils) reformat NUMBER")
    (description "numfmt ~ (uutils) reformat NUMBER")
    (license license:expat)))

(define-public rust-uu-nproc-0.0.12
  (package
    (name "rust-uu-nproc")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_nproc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1zgd47i0fqsslpjpxbqx182pl54iksv4qibm0p0yibl7f379m9m2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "nproc ~ (uutils) display the number of processing units available")
    (description
      "nproc ~ (uutils) display the number of processing units available")
    (license license:expat)))

(define-public rust-uu-nohup-0.0.12
  (package
    (name "rust-uu-nohup")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_nohup" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1j7llbz4jyiczhbpmjmxpnpd076950rf7lql6c6in4dz114q5ss5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "nohup ~ (uutils) run COMMAND, ignoring hangup signals")
    (description "nohup ~ (uutils) run COMMAND, ignoring hangup signals")
    (license license:expat)))

(define-public rust-uu-nl-0.0.12
  (package
    (name "rust-uu-nl")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_nl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1mlb4k5j1dky9bv0fkhfqaw5v0wyp8j4afbix6klzhxc7yqjqwg4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-aho-corasick" ,rust-aho-corasick-0.7)
         ("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex" ,rust-regex-1)
         ("rust-regex-syntax" ,rust-regex-syntax-0.6)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "nl ~ (uutils) display input with added line numbers")
    (description "nl ~ (uutils) display input with added line numbers")
    (license license:expat)))

(define-public rust-uu-nice-0.0.12
  (package
    (name "rust-uu-nice")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_nice" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "13j1jnb7y6af1kra15p3zc82jv04d0xvysmjh61flcjpfhf8aqid"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "nice ~ (uutils) run PROGRAM with modified scheduling priority")
    (description
      "nice ~ (uutils) run PROGRAM with modified scheduling priority")
    (license license:expat)))

(define-public rust-uu-mv-0.0.12
  (package
    (name "rust-uu-mv")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_mv" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06wqyfb64wqf3dwj48blivbm8y8gk8wshz9nj39m8h37qr0960wr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-fs-extra" ,rust-fs-extra-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "mv ~ (uutils) move (rename) SOURCE to DESTINATION")
    (description "mv ~ (uutils) move (rename) SOURCE to DESTINATION")
    (license license:expat)))

(define-public rust-uu-more-0.0.12
  (package
    (name "rust-uu-more")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_more" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1w9sksvgsr9d49snsvk3lfg6pfx6cz47arh6bzwiy432lksd53fd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-clap" ,rust-clap-2)
         ("rust-crossterm" ,rust-crossterm-0.20)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-redox-syscall" ,rust-redox-syscall-0.2)
         ("rust-redox-termios" ,rust-redox-termios-0.1)
         ("rust-unicode-segmentation" ,rust-unicode-segmentation-1)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "more ~ (uutils) input perusal filter")
    (description "more ~ (uutils) input perusal filter")
    (license license:expat)))

(define-public rust-uu-mktemp-0.0.12
  (package
    (name "rust-uu-mktemp")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_mktemp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18g55k5rcl1xvkm06s7r1g7vn9qqs3gk3m5brdkbxa34kkwdmazw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-rand" ,rust-rand-0.5)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "mktemp ~ (uutils) create and display a temporary file or directory from TEMPLATE")
    (description
      "mktemp ~ (uutils) create and display a temporary file or directory from TEMPLATE")
    (license license:expat)))

(define-public rust-uu-mknod-0.0.12
  (package
    (name "rust-uu-mknod")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_mknod" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hxgf6cp2dl4d8zbyhilhxvsssmwqfgwgprmk3h04zwgyk5vh2di"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "mknod ~ (uutils) create special file NAME of TYPE")
    (description "mknod ~ (uutils) create special file NAME of TYPE")
    (license license:expat)))

(define-public rust-uu-mkfifo-0.0.12
  (package
    (name "rust-uu-mkfifo")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_mkfifo" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1k2xw6m3g2dczb587sppanbfnq589znmqv115hpysqpym6a5cfgz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "mkfifo ~ (uutils) create FIFOs (named pipes)")
    (description "mkfifo ~ (uutils) create FIFOs (named pipes)")
    (license license:expat)))

(define-public rust-uu-mkdir-0.0.12
  (package
    (name "rust-uu-mkdir")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_mkdir" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0j2hxs6ags7ca104mcznw8qx695kxxycli2ypql666n0zkk1c4wf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "mkdir ~ (uutils) create DIRECTORY")
    (description "mkdir ~ (uutils) create DIRECTORY")
    (license license:expat)))

(define-public rust-termsize-0.1
  (package
    (name "rust-termsize")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "termsize" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05r5rlng0li8ggpgij8c3dl9p6hiywgdajzgscm383z9m0jdi1jy"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-termion" ,rust-termion-1)
         ("rust-winapi" ,rust-winapi-0.2))))
    (home-page "https://github.com/softprops/termsize")
    (synopsis "Retrieves terminal size")
    (description "Retrieves terminal size")
    (license license:expat)))

(define-public rust-uu-ls-0.0.12
  (package
    (name "rust-uu-ls")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_ls" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0w8mfjx3lfvilrr2pihxmmqkdfgb57ddhs916l8ryfxaym9sx3j2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-clap" ,rust-clap-2)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-lscolors" ,rust-lscolors-0.7)
         ("rust-number-prefix" ,rust-number-prefix-0.4)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-selinux" ,rust-selinux-0.2)
         ("rust-term-grid" ,rust-term-grid-0.1)
         ("rust-termsize" ,rust-termsize-0.1)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "ls ~ (uutils) display directory contents")
    (description "ls ~ (uutils) display directory contents")
    (license license:expat)))

(define-public rust-uu-logname-0.0.12
  (package
    (name "rust-uu-logname")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_logname" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "14cznsz6msgqi27vnv28am0zfq3kvlydx5wy155ns9449wrg607w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "logname ~ (uutils) display the login name of the current user")
    (description
      "logname ~ (uutils) display the login name of the current user")
    (license license:expat)))

(define-public rust-uu-ln-0.0.12
  (package
    (name "rust-uu-ln")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_ln" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17v6sy6v7rs0h3jkldn977g003vpv8v9w9id3i3arg111i0khvga"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "ln ~ (uutils) create a (file system) link to TARGET")
    (description "ln ~ (uutils) create a (file system) link to TARGET")
    (license license:expat)))

(define-public rust-uu-link-0.0.12
  (package
    (name "rust-uu-link")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_link" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1x3inyrdfqabi3k750rrp4bmpv5m2dxj6s2m8xbh66dq3pi8sndz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "link ~ (uutils) create a hard (file system) link to FILE")
    (description "link ~ (uutils) create a hard (file system) link to FILE")
    (license license:expat)))

(define-public rust-uu-kill-0.0.12
  (package
    (name "rust-uu-kill")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_kill" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0libp9g3b9wha092892n7vlz7rv317769dylh9abn2g7cl4n4bpi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "kill ~ (uutils) send a signal to a process")
    (description "kill ~ (uutils) send a signal to a process")
    (license license:expat)))

(define-public rust-uu-join-0.0.12
  (package
    (name "rust-uu-join")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_join" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0nj0jjrrf27c69scnj7rx6ia21nfwhcffzl89wkyig0170sn6n0y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "join ~ (uutils) merge lines from inputs with matching join fields")
    (description
      "join ~ (uutils) merge lines from inputs with matching join fields")
    (license license:expat)))

(define-public rust-file-diff-1
  (package
    (name "rust-file-diff")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "file_diff" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19a34rvbqg3b2my6ykax5n1qi2ahwbjacn9y2ji3h9gkp04ak9ri"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/ethanpailes/file_diff-rs")
    (synopsis "An atomic utility for diffing files in testing")
    (description "An atomic utility for diffing files in testing")
    (license license:bsd-3)))

(define-public rust-uu-install-0.0.12
  (package
    (name "rust-uu-install")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_install" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "045cynbkacbrw9awsk016wr834nx93l57jfwxvwmmixcvb6rk8cl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-file-diff" ,rust-file-diff-1)
         ("rust-filetime" ,rust-filetime-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "install ~ (uutils) copy files from SOURCE to DESTINATION (with specified attributes)")
    (description
      "install ~ (uutils) copy files from SOURCE to DESTINATION (with specified
attributes)")
    (license license:expat)))

(define-public rust-uu-id-0.0.12
  (package
    (name "rust-uu-id")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_id" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0p59wdnjvxfh3hwrhgk1l6zfg4jszy0mal10mdi5cxlfkc2aq5vf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-selinux" ,rust-selinux-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "id ~ (uutils) display user and group information for USER")
    (description "id ~ (uutils) display user and group information for USER")
    (license license:expat)))

(define-public rust-uu-hostname-0.0.12
  (package
    (name "rust-uu-hostname")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_hostname" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0rmslbq2csy7j4fqm1qmrzqsynzswsfsi7zm8lfh0fcva12cilv1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-hostname" ,rust-hostname-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "hostname ~ (uutils) display or set the host name of the current host")
    (description
      "hostname ~ (uutils) display or set the host name of the current host")
    (license license:expat)))

(define-public rust-uu-hostid-0.0.12
  (package
    (name "rust-uu-hostid")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_hostid" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0qm52r167kkjag20cd85dh5b79dmqgi0snvblm7q7594ghwm3g6y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "hostid ~ (uutils) display the numeric identifier of the current host")
    (description
      "hostid ~ (uutils) display the numeric identifier of the current host")
    (license license:expat)))

(define-public rust-uu-head-0.0.12
  (package
    (name "rust-uu-head")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_head" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0f2n76lx8dmhrv7vwrjgn94k6ikqmgnkarxzpg14zmw6kn16da0m"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "head ~ (uutils) display the first lines of input")
    (description "head ~ (uutils) display the first lines of input")
    (license license:expat)))

(define-public rust-sha3-0.6
  (package
    (name "rust-sha3")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "sha3" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "186nimd5rv3xfvsk3ravlqaaq1q5c5idmkq907398sm5nq2mjh16"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-block-buffer" ,rust-block-buffer-0.2)
         ("rust-byte-tools" ,rust-byte-tools-0.2)
         ("rust-digest" ,rust-digest-0.6)
         ("rust-generic-array" ,rust-generic-array-0.8))))
    (home-page "https://github.com/RustCrypto/hashes")
    (synopsis "SHA-3 (Keccak) hash function")
    (description "SHA-3 (Keccak) hash function")
    (license (list license:expat license:asl2.0))))

(define-public rust-blake2b-simd-0.5
  (package
    (name "rust-blake2b-simd")
    (version "0.5.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "blake2b_simd" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "11y5nm06lpypz65dbxgncs12ckx24i5i4a777ckfhfxd93ili9xg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arrayref" ,rust-arrayref-0.3)
         ("rust-arrayvec" ,rust-arrayvec-0.5)
         ("rust-constant-time-eq" ,rust-constant-time-eq-0.1))))
    (home-page "https://github.com/oconnor663/blake2_simd")
    (synopsis "a pure Rust BLAKE2b implementation with dynamic SIMD")
    (description "a pure Rust BLAKE2b implementation with dynamic SIMD")
    (license license:expat)))

(define-public rust-uu-hashsum-0.0.12
  (package
    (name "rust-uu-hashsum")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_hashsum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wpxb0wk993sw17grbwj3lgis4xgxd2as87ma6vc0m11r1n4yd15"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-blake2b-simd" ,rust-blake2b-simd-0.5)
         ("rust-clap" ,rust-clap-2)
         ("rust-digest" ,rust-digest-0.6)
         ("rust-hex" ,rust-hex-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-md5" ,rust-md5-0.3)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-regex" ,rust-regex-1)
         ("rust-regex-syntax" ,rust-regex-syntax-0.6)
         ("rust-sha1" ,rust-sha1-0.6)
         ("rust-sha2" ,rust-sha2-0.6)
         ("rust-sha3" ,rust-sha3-0.6)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "hashsum ~ (uutils) display or check input digests")
    (description "hashsum ~ (uutils) display or check input digests")
    (license license:expat)))

(define-public rust-uu-groups-0.0.12
  (package
    (name "rust-uu-groups")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_groups" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vvl0bznyqw0bnqyks3bv38v2xjbx6brv3lxpnlmvfqq2vq1hlh4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "groups ~ (uutils) display group memberships for USERNAME")
    (description "groups ~ (uutils) display group memberships for USERNAME")
    (license license:expat)))

(define-public rust-uu-fold-0.0.12
  (package
    (name "rust-uu-fold")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_fold" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18xlg36snrziijci11jny5xqiagyd9rz4sc4mk07cqgv303hk2p7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "fold ~ (uutils) wrap each line of input")
    (description "fold ~ (uutils) wrap each line of input")
    (license license:expat)))

(define-public rust-uu-fmt-0.0.12
  (package
    (name "rust-uu-fmt")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_fmt" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1i1b2iz66y4vh8j3xx89nw05mpxx7dfxkyd4ybsbfpk7qbp8xvsg"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "fmt ~ (uutils) reformat each paragraph of input")
    (description "fmt ~ (uutils) reformat each paragraph of input")
    (license license:expat)))

(define-public rust-uu-false-0.0.12
  (package
    (name "rust-uu-false")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_false" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0nshb9whl9rgr8rznh0fm5xmkz5hb3vyal342i877q6063apyb3k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "false ~ (uutils) do nothing and fail")
    (description "false ~ (uutils) do nothing and fail")
    (license license:expat)))

(define-public rust-smallvec-1
  (package
    (name "rust-smallvec")
    (version "1.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smallvec" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10zf4fn63p2d6sx8qap3jvyarcfw563308x3431hd4c34r35gpgj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-arbitrary" ,rust-arbitrary-1) ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/servo/rust-smallvec")
    (synopsis
      "'Small vector' optimization: store up to a small number of items on the stack")
    (description
      "'Small vector' optimization: store up to a small number of items on the stack")
    (license (list license:expat license:asl2.0))))

(define-public rust-coz-0.1
  (package
    (name "rust-coz")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "coz" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1qknvqva3kkf02pczbcy16yjann9ngl95irbw5cpsizmw8zmpxff"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2) ("rust-once-cell" ,rust-once-cell-1))))
    (home-page "https://github.com/plasma-umass/coz")
    (synopsis
      "Rust support for the `coz` Causal Profiler: https://github.com/plasma-umass/coz
")
    (description
      "Rust support for the `coz` Causal Profiler: https://github.com/plasma-umass/coz")
    (license license:expat )))

(define-public rust-uu-factor-0.0.12
  (package
    (name "rust-uu-factor")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_factor" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1xkky3jsingxlv274a2aapzfcb1yqygwkz1by8bv1446xzgxp6ar"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-coz" ,rust-coz-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-smallvec" ,rust-smallvec-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "factor ~ (uutils) display the prime factors of each NUMBER")
    (description "factor ~ (uutils) display the prime factors of each NUMBER")
    (license license:expat)))

(define-public rust-onig-sys-69
  (package
    (name "rust-onig-sys")
    (version "69.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "onig_sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1c639k5x7kp5m4vrvmvx4hqar8dyyiskknz6b07g2hd3byzi111q"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cc" ,rust-cc-1) ("rust-pkg-config" ,rust-pkg-config-0.3))))
    (home-page "http://github.com/iwillspeak/rust-onig")
    (synopsis
      "The `onig_sys` crate contains raw rust bindings to the
oniguruma library. This crate exposes a set of unsafe
functions which can then be used by other crates to
create safe wrappers around Oniguruma.

You probably don't want to link to this crate directly;
instead check out the `onig` crate.
")
    (description
      "The `onig_sys` crate contains raw rust bindings to the oniguruma library.  This
crate exposes a set of unsafe functions which can then be used by other crates
to create safe wrappers around Oniguruma.

You probably don't want to link to this crate directly; instead check out the
`onig` crate.")
    (license license:expat)))

(define-public rust-onig-4
  (package
    (name "rust-onig")
    (version "4.3.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "onig" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10xk5xfk3f3kq62s2sfaflsgr1v0v97xz6fl19gz9hmqn6rgq645"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-onig-sys" ,rust-onig-sys-69))))
    (home-page "http://github.com/iwillspeak/rust-onig")
    (synopsis
      "Rust-Onig is a set of Rust bindings for the
Oniguruma regular expression library. Oniguruma
is a modern regex library with support for
multiple character encodings and regex syntaxes.
")
    (description
      "Rust-Onig is a set of Rust bindings for the Oniguruma regular expression
library.  Oniguruma is a modern regex library with support for multiple
character encodings and regex syntaxes.")
    (license license:expat)))

(define-public rust-uu-expr-0.0.12
  (package
    (name "rust-uu-expr")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_expr" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18qy16fyazvmx4x0c80hfcskk09qbvv9rza44vkd00r2bzi2p87x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-num-bigint" ,rust-num-bigint-0.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-onig" ,rust-onig-4)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "expr ~ (uutils) display the value of EXPRESSION")
    (description "expr ~ (uutils) display the value of EXPRESSION")
    (license license:expat)))

(define-public rust-uu-expand-0.0.12
  (package
    (name "rust-uu-expand")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_expand" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0s5f7442lh6zxrl6gmn58kwz4z8jc40bvmxr4wbks2s3dksfv7q8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-unicode-width" ,rust-unicode-width-0.1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "expand ~ (uutils) convert input tabs to spaces")
    (description "expand ~ (uutils) convert input tabs to spaces")
    (license license:expat)))

(define-public rust-uu-env-0.0.12
  (package
    (name "rust-uu-env")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_env" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1n283bfdbac3ybws127yzhhx7vg2nb23ydjr88xgc21ja938lfcq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-rust-ini" ,rust-rust-ini-0.17)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "env ~ (uutils) set each NAME to VALUE in the environment and run COMMAND")
    (description
      "env ~ (uutils) set each NAME to VALUE in the environment and run COMMAND")
    (license license:expat)))

(define-public rust-uu-echo-0.0.12
  (package
    (name "rust-uu-echo")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_echo" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08dhg0rv7anaimnxyapjhj0mfpyfjkfwpjwhxmsfhk6gy2rrj5mi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "echo ~ (uutils) display TEXT")
    (description "echo ~ (uutils) display TEXT")
    (license license:expat)))

(define-public rust-uu-du-0.0.12
  (package
    (name "rust-uu-du")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_du" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pgfaprskz7hi0c7hdggzqfsbhja8m0yd79v6qmmhspi4i1246iw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chrono" ,rust-chrono-0.4)
         ("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "du ~ (uutils) display disk usage")
    (description "du ~ (uutils) display disk usage")
    (license license:expat)))

(define-public rust-uu-dirname-0.0.12
  (package
    (name "rust-uu-dirname")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_dirname" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1hgx8hh8r6w9d31qy3dwdkpylizpwqjwsphv4y6r5iqkrlkbzx6p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "dirname ~ (uutils) display parent directory of PATHNAME")
    (description "dirname ~ (uutils) display parent directory of PATHNAME")
    (license license:expat)))

(define-public rust-uu-dircolors-0.0.12
  (package
    (name "rust-uu-dircolors")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_dircolors" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pg20rrz44ly9jpc8z0prrsvrw4yinf6ird59c3p8z7q6wzpzaxn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "dircolors ~ (uutils) display commands to set LS_COLORS")
    (description "dircolors ~ (uutils) display commands to set LS_COLORS")
    (license license:expat)))

(define-public rust-number-prefix-0.4
  (package
    (name "rust-number-prefix")
    (version "0.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "number_prefix" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1wvh13wvlajqxkb1filsfzbrnq0vrmrw298v2j3sy82z1rm282w3"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/ogham/rust-number-prefix")
    (synopsis "Library for numeric prefixes (kilo, giga, kibi).")
    (description "Library for numeric prefixes (kilo, giga, kibi).")
    (license license:expat)))

(define-public rust-uu-df-0.0.12
  (package
    (name "rust-uu-df")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_df" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "18039w4296i5yc8ibarp8ndlzj4s44ax2w6i931if99w6rdlidbk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-number-prefix" ,rust-number-prefix-0.4)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "df ~ (uutils) display file system information")
    (description "df ~ (uutils) display file system information")
    (license license:expat)))

(define-public rust-gcd-2
  (package
    (name "rust-gcd")
    (version "2.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "gcd" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16m7mb5p5zwy836sclchmwrndnrjxz0qnbrvm0w9jy6anbd7hygk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-paste" ,rust-paste-1))))
    (home-page "https://github.com/frewsxcv/rust-gcd")
    (synopsis "Calculate the greatest common divisor")
    (description "Calculate the greatest common divisor")
    (license (list license:expat license:asl2.0))))

(define-public rust-uu-dd-0.0.12
  (package
    (name "rust-uu-dd")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_dd" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19sbma2pnvqr7q37hr7w2pmdvi17apwlbacnxz8nz3a4h76rrf95"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-byte-unit" ,rust-byte-unit-4)
         ("rust-clap" ,rust-clap-2)
         ("rust-gcd" ,rust-gcd-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-signal-hook" ,rust-signal-hook-0.3)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "dd ~ (uutils) copy and convert files")
    (description "dd ~ (uutils) copy and convert files")
    (license license:expat)))

(define-public rust-uu-date-0.0.12
  (package
    (name "rust-uu-date")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_date" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0pvdnz4k0x8dwakbibm8bnlcns7739c1haci4m8n71fsmib8v0h2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-chrono" ,rust-chrono-0.4)
         ("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "date ~ (uutils) display or set the current time")
    (description "date ~ (uutils) display or set the current time")
    (license license:expat)))

(define-public rust-uu-cut-0.0.12
  (package
    (name "rust-uu-cut")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_cut" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0vgz1cgfyl8zi8y5hpykl8sqmm6q6nkl07cgnn34q9s6vazvb5qr"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-bstr" ,rust-bstr-0.2)
         ("rust-clap" ,rust-clap-2)
         ("rust-memchr" ,rust-memchr-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "cut ~ (uutils) display byte/field columns of input lines")
    (description "cut ~ (uutils) display byte/field columns of input lines")
    (license license:expat)))

(define-public rust-uu-csplit-0.0.12
  (package
    (name "rust-uu-csplit")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_csplit" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "080z5k5kagvsixqqkfzz1mr508nb6xisxwmybpcmx6b73z3qkv25"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-regex" ,rust-regex-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "csplit ~ (uutils) Output pieces of FILE separated by PATTERN(s) to files 'xx00', 'xx01', ..., and output byte counts of each piece to standard output")
    (description
      "csplit ~ (uutils) Output pieces of FILE separated by PATTERN(s) to files 'xx00',
'xx01', ..., and output byte counts of each piece to standard output")
    (license license:expat)))

(define-public rust-ioctl-sys-0.6
  (package
    (name "rust-ioctl-sys")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ioctl-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0bhqal9zbala895b5iwvwfi8k13jbxb2dz19jmk8iwjqlvzryhhw"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/jmesmon/ioctl")
    (synopsis
      "IO Control for POSIX-and-beyond systems (core fn & macros, see `ioctls` for many ioctl definitions)")
    (description
      "IO Control for POSIX-and-beyond systems (core fn & macros, see `ioctls` for many
ioctl definitions)")
    (license (list license:expat license:asl2.0))))

(define-public rust-num-enum-derive-0.5
  (package
    (name "rust-num-enum-derive")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_enum_derive" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08dvxpa7l5hx5fcdr0bdv9bzajbcbxsbbnc6hl6zxmwhhiv2p68d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro-crate" ,rust-proc-macro-crate-1)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/illicitonion/num_enum")
    (synopsis
      "Internal implementation details for ::num_enum (Procedural macros to make inter-operation between primitives and enums easier)")
    (description
      "Internal implementation details for ::num_enum (Procedural macros to make
inter-operation between primitives and enums easier)")
    (license (list license:bsd-3 license:expat license:asl2.0))))

(define-public rust-num-enum-0.5
  (package
    (name "rust-num-enum")
    (version "0.5.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "num_enum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1b96nmbhn2gadlh4hna6mz6w892gzp1zic60q1s4akjy0nhkw3bj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-num-enum-derive" ,rust-num-enum-derive-0.5))))
    (home-page "https://github.com/illicitonion/num_enum")
    (synopsis
      "Procedural macros to make inter-operation between primitives and enums easier.")
    (description
      "Procedural macros to make inter-operation between primitives and enums easier.")
    (license (list license:bsd-3 license:expat license:asl2.0))))

(define-public rust-exacl-0.6
  (package
    (name "rust-exacl")
    (version "0.6.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "exacl" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0axsa6bkvyzzpz5r3wfjr7s6ja0dkx287kspp5jlis416wbvv6vn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.58)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-log" ,rust-log-0.4)
         ("rust-nix" ,rust-nix-0.21)
         ("rust-num-enum" ,rust-num-enum-0.5)
         ("rust-scopeguard" ,rust-scopeguard-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-uuid" ,rust-uuid-0.8))))
    (home-page "https://github.com/byllyfish/exacl")
    (synopsis
      "Manipulate file system access control lists (ACL) on macOS, Linux, and FreeBSD")
    (description
      "Manipulate file system access control lists (ACL) on macOS, Linux, and FreeBSD")
    (license license:expat)))

(define-public rust-uu-cp-0.0.12
  (package
    (name "rust-uu-cp")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_cp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1fy1ppb3ild3vlsn5il3fqy97hllcq405swwpp6fxd5649jfga23"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-exacl" ,rust-exacl-0.6)
         ("rust-filetime" ,rust-filetime-0.2)
         ("rust-ioctl-sys" ,rust-ioctl-sys-0.6)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-quick-error" ,rust-quick-error-1)
         ("rust-selinux" ,rust-selinux-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-winapi" ,rust-winapi-0.3)
         ("rust-xattr" ,rust-xattr-0.2))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "cp ~ (uutils) copy SOURCE to DESTINATION")
    (description "cp ~ (uutils) copy SOURCE to DESTINATION")
    (license license:expat)))

(define-public rust-uu-comm-0.0.12
  (package
    (name "rust-uu-comm")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_comm" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16ifqazjbgrs40mlf28wswhc7vcggsv0maa1k9s96dnyf1bzj4zd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "comm ~ (uutils) compare sorted inputs")
    (description "comm ~ (uutils) compare sorted inputs")
    (license license:expat)))

(define-public rust-uu-cksum-0.0.12
  (package
    (name "rust-uu-cksum")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_cksum" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "10vabph7hk1qgppzwnza2va6i7hbj9ch634lwqcf44m09zdw9bgq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "cksum ~ (uutils) display CRC and size of input")
    (description "cksum ~ (uutils) display CRC and size of input")
    (license license:expat)))

(define-public rust-uu-chroot-0.0.12
  (package
    (name "rust-uu-chroot")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_chroot" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1kgglqhc4j7aq84ixhhan09rn1d154a8j9idfy3p9f2q29lcpb9r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "chroot ~ (uutils) run COMMAND under a new root directory")
    (description "chroot ~ (uutils) run COMMAND under a new root directory")
    (license license:expat)))

(define-public rust-uu-chown-0.0.12
  (package
    (name "rust-uu-chown")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_chown" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01jgc6fy8iq5bdm2wn92g3yakxxr1sfg37cnfpskans35bv9iwvd"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "chown ~ (uutils) change the ownership of FILE")
    (description "chown ~ (uutils) change the ownership of FILE")
    (license license:expat)))

(define-public rust-uu-chmod-0.0.12
  (package
    (name "rust-uu-chmod")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_chmod" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "01ahjcn27yi2clxgpcbmy9lz8gzpj7ks1m2xci9nbn6hf047jlmm"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "chmod ~ (uutils) change mode of FILE")
    (description "chmod ~ (uutils) change mode of FILE")
    (license license:expat)))

(define-public rust-uu-chgrp-0.0.12
  (package
    (name "rust-uu-chgrp")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_chgrp" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "05j4w1jbxq8s07szrp4vwis93hnrg7zrnci51v4b44wx0f2gnyjl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "chgrp ~ (uutils) change the group ownership of FILE")
    (description "chgrp ~ (uutils) change the group ownership of FILE")
    (license license:expat)))

(define-public rust-fts-sys-0.2
  (package
    (name "rust-fts-sys")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fts-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1jfaclykcqf3cfillbdyv5142lal5w87pbhzkbj0w9qfb3qwj7nk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.59) ("rust-libc" ,rust-libc-0.2))))
    (home-page "https://github.com/koutheir/fts-sys")
    (synopsis "File hierarchy traversal functions (FTS)")
    (description "File hierarchy traversal functions (FTS)")
    (license license:expat)))

(define-public rust-uu-chcon-0.0.12
  (package
    (name "rust-uu-chcon")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_chcon" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "096kqqab3z8b72bhn5r809xwc4naad1a4v7nsj7acgim8zaz7li0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-fts-sys" ,rust-fts-sys-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-selinux" ,rust-selinux-0.2)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "chcon ~ (uutils) change file security context")
    (description "chcon ~ (uutils) change file security context")
    (license license:expat)))

(define-public rust-uu-cat-0.0.12
  (package
    (name "rust-uu-cat")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_cat" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0kyb1id4gykqfdcsrdg6s6m9slnnd1nhi95d2w8jf539p8jlgd0h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-clap" ,rust-clap-2)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-unix-socket" ,rust-unix-socket-0.5)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12)
         ("rust-winapi-util" ,rust-winapi-util-0.1))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "cat ~ (uutils) concatenate and display input")
    (description "cat ~ (uutils) concatenate and display input")
    (license license:expat)))

(define-public rust-uu-basenc-0.0.12
  (package
    (name "rust-uu-basenc")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_basenc" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0bicx6dm38nr1b8x0dhc2k3xdckx89rsbs24c7yiq4gjx7k1dpgp"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uu-base32" ,rust-uu-base32-0.0.12)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "basenc ~ (uutils) decode/encode input")
    (description "basenc ~ (uutils) decode/encode input")
    (license license:expat)))

(define-public rust-uu-basename-0.0.12
  (package
    (name "rust-uu-basename")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_basename" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1cliw5z7yjxzw7ri0dg0inpibaai2zdwmhw8cha465lmzh2l00hq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "basename ~ (uutils) display PATHNAME with leading directory components removed")
    (description
      "basename ~ (uutils) display PATHNAME with leading directory components removed")
    (license license:expat)))

(define-public rust-uu-base64-0.0.12
  (package
    (name "rust-uu-base64")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_base64" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "03frxjwsxf0d80xkzl05yj0v2ng31fjj01wkkr33kflkp0vrlqv0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uu-base32" ,rust-uu-base32-0.0.12)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "base64 ~ (uutils) decode/encode input (base64-encoding)")
    (description "base64 ~ (uutils) decode/encode input (base64-encoding)")
    (license license:expat)))

(define-public rust-uu-base32-0.0.12
  (package
    (name "rust-uu-base32")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_base32" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1x95hy4wdd5w8nh4jgaxzaba12a0b59hsl8yi6bif6ln4kfsq7jf"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "base32 ~ (uutils) decode/encode input (base32-encoding)")
    (description "base32 ~ (uutils) decode/encode input (base32-encoding)")
    (license license:expat)))

(define-public rust-uucore-procs-0.0.12
  (package
    (name "rust-uucore-procs")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uucore_procs" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0ap0xkah6ss7drzvgcma9m7li6wzw5js2rwb6gj5nvvqf5gyf174"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/uutils/uucore/uucore_procs")
    (synopsis "uutils ~ 'uucore' proc-macros")
    (description "uutils ~ 'uucore' proc-macros")
    (license license:expat)))

(define-public rust-z85-3
  (package
    (name "rust-z85")
    (version "3.0.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "z85" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0mpq5932l61kyjmx0knkyf3c045jk5m2fx2znrs0nd41vf9nx2dg"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/decafbad/z85")
    (synopsis
      "Rust implementation of ZeroMQ's Z85 encoding mechanism with padding.")
    (description
      "Rust implementation of ZeroMQ's Z85 encoding mechanism with padding.")
    (license (list license:expat license:asl2.0))))

(define-public rust-os-display-0.1
  (package
    (name "rust-os-display")
    (version "0.1.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "os_display" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0xfgfqvfg5nyidv5p85fb87l0mif1nniisxarw6npd4jv2x2jqks"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-unicode-width" ,rust-unicode-width-0.1))))
    (home-page "https://github.com/blyxxyz/os_display")
    (synopsis "Display strings in a safe platform-appropriate way")
    (description "Display strings in a safe platform-appropriate way")
    (license license:expat)))

(define-public rust-dns-lookup-1
  (package
    (name "rust-dns-lookup")
    (version "1.0.8")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "dns-lookup" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wk877zla9gdns5f1zgrxwzpi0abj2ld2n54a6dqsln4ab4szv2k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-cfg-if" ,rust-cfg-if-1)
         ("rust-clippy" ,rust-clippy-0.0.302)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-socket2" ,rust-socket2-0.4)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/keeperofdakeys/dns-lookup/")
    (synopsis
      "A simple dns resolving api, much like rust's unstable api. Also includes getaddrinfo and getnameinfo wrappers for libc variants.")
    (description
      "This package provides a simple dns resolving api, much like rust's unstable api.
 Also includes getaddrinfo and getnameinfo wrappers for libc variants.")
    (license (list license:expat license:asl2.0))))

(define-public rust-data-encoding-macro-internal-0.1
  (package
    (name "rust-data-encoding-macro-internal")
    (version "0.1.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "data-encoding-macro-internal" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1nrqy2c3azch285a9ya63qx43f25ldm58l0ans0fg5dav91fvfx5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-data-encoding" ,rust-data-encoding-2)
         ("rust-syn" ,rust-syn-1))))
    (home-page "https://github.com/ia0/data-encoding")
    (synopsis "Internal library for data-encoding-macro")
    (description "Internal library for data-encoding-macro")
    (license license:expat)))

(define-public rust-data-encoding-macro-0.1
  (package
    (name "rust-data-encoding-macro")
    (version "0.1.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "data-encoding-macro" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1jls0b9p4nsp5vcp2h53cc01m3drg8l4nh47idlzm27ys9y7p4l6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-data-encoding" ,rust-data-encoding-2)
         ("rust-data-encoding-macro-internal"
          ,rust-data-encoding-macro-internal-0.1))))
    (home-page "https://github.com/ia0/data-encoding")
    (synopsis "Macros for data-encoding")
    (description "Macros for data-encoding")
    (license license:expat)))

(define-public rust-uucore-0.0.12
  (package
    (name "rust-uucore")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uucore" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0wjf6jn70696gcw7pslx9hiq8jig2islr0n2i0mkrwd57myhaqnx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-data-encoding" ,rust-data-encoding-2)
         ("rust-data-encoding-macro" ,rust-data-encoding-macro-0.1)
         ("rust-dns-lookup" ,rust-dns-lookup-1)
         ("rust-dunce" ,rust-dunce-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-os-display" ,rust-os-display-0.1)
         ("rust-termion" ,rust-termion-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-time" ,rust-time-0.1)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-wild" ,rust-wild-2)
         ("rust-winapi" ,rust-winapi-0.3)
         ("rust-winapi-util" ,rust-winapi-util-0.1)
         ("rust-z85" ,rust-z85-3))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "uutils ~ 'core' uutils code library (cross-platform)")
    (description "uutils ~ 'core' uutils code library (cross-platform)")
    (license license:expat)))

(define-public rust-platform-info-0.2
  (package
    (name "rust-platform-info")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "platform-info" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "187jxpbpjy7mmf522s7p6i557vffcdgf6hx1brppwmixw16jqcw4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-libc" ,rust-libc-0.2) ("rust-winapi" ,rust-winapi-0.3))))
    (home-page "https://github.com/uutils/platform-info")
    (synopsis "A simple cross-platform interface to get info about a system")
    (description
      "This package provides a simple cross-platform interface to get info about a
system")
    (license license:expat)))

(define-public rust-uu-arch-0.0.12
  (package
    (name "rust-uu-arch")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "uu_arch" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "08x29iwy92f8z55ypgarf3sp2akssi22nlcnslaan5y41yxhi93s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-platform-info" ,rust-platform-info-0.2)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-uucore-procs" ,rust-uucore-procs-0.0.12))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis "arch ~ (uutils) display machine architecture")
    (description "arch ~ (uutils) display machine architecture")
    (license license:expat)))

(define-public rust-unicode-linebreak-0.1
  (package
    (name "rust-unicode-linebreak")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unicode-linebreak" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0grq6bsn967q4vpifld53s7a140nlmpq5vy8ghgr73f4n2mdqlis"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-regex" ,rust-regex-1))))
    (home-page "https://github.com/axelf4/unicode-linebreak")
    (synopsis "Implementation of the Unicode Line Breaking Algorithm")
    (description "Implementation of the Unicode Line Breaking Algorithm")
    (license license:asl2.0)))

(define-public rust-openblas-src-0.9
  (package
    (name "rust-openblas-src")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openblas-src" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0syy38a5bgv5mj6mb1n1zk1d6l5gqqrswvbmwkwx6h4z9wfrsql4"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/blas-lapack-rs/openblas-src")
    (synopsis "The package provides a source of BLAS and LAPACK via OpenBLAS.")
    (description
      "The package provides a source of BLAS and LAPACK via OpenBLAS.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-netlib-src-0.8
  (package
    (name "rust-netlib-src")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "netlib-src" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "04l2ggdaq0bjc64prsw2f8ddxn84m1rmpnkjb9nr0ijdpcv1zx1r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-cmake" ,rust-cmake-0.1))))
    (home-page "https://github.com/blas-lapack-rs/netlib-src")
    (synopsis "The package provides a source of BLAS and LAPACK via Netlib.")
    (description
      "The package provides a source of BLAS and LAPACK via Netlib.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-intel-mkl-tool-0.1
  (package
    (name "rust-intel-mkl-tool")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intel-mkl-tool" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1myyrxvmyij4c60w9x15npwzhlbjm8y8c94lvfsnrl5pbyakz8md"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-curl" ,rust-curl-0.4)
         ("rust-dirs" ,rust-dirs-2)
         ("rust-env-logger" ,rust-env-logger-0.7)
         ("rust-failure" ,rust-failure-0.1)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-structopt" ,rust-structopt-0.3)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-zstd" ,rust-zstd-0.5))))
    (home-page "https://github.com/rust-math/intel-mkl-src")
    (synopsis "CLI utility for redistributiing Intel(R) MKL")
    (description "CLI utility for redistributiing Intel(R) MKL")
    (license license:expat)))

(define-public rust-intel-mkl-src-0.5
  (package
    (name "rust-intel-mkl-src")
    (version "0.5.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intel-mkl-src" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "155q49a7nfbq1lllchsyx8jv2q2pijrjh1w08awvrbjyfcxb6q3j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-intel-mkl-tool" ,rust-intel-mkl-tool-0.1))))
    (home-page "https://github.com/rust-math/intel-mkl-src")
    (synopsis "Redistribution of Intel(R) MKL as a crate")
    (description "Redistribution of Intel(R) MKL as a crate")
    (license license:expat)))

(define-public rust-accelerate-src-0.3
  (package
    (name "rust-accelerate-src")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "accelerate-src" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "17fiqyq7f9k41pbsyrvk9pxyx9z6fw399wq036cvwkbmb14xcpj1"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/blas-lapack-rs/accelerate-src")
    (synopsis
      "The package provides a source of BLAS and LAPACK via the Accelerate framework.")
    (description
      "The package provides a source of BLAS and LAPACK via the Accelerate framework.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-blas-src-0.6
  (package
    (name "rust-blas-src")
    (version "0.6.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "blas-src" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0a134wadi4rslfqk4mafi6y7bbvacjh12x87621w4vyc3dni6px2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-accelerate-src" ,rust-accelerate-src-0.3)
         ("rust-intel-mkl-src" ,rust-intel-mkl-src-0.5)
         ("rust-netlib-src" ,rust-netlib-src-0.8)
         ("rust-openblas-src" ,rust-openblas-src-0.9))))
    (home-page "https://github.com/blas-lapack-rs/blas-src")
    (synopsis "The package provides a BLAS source of choice.")
    (description "The package provides a BLAS source of choice.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-ndarray-0.14
  (package
    (name "rust-ndarray")
    (version "0.14.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ndarray" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "011wqzmrd9gpfcfvy1xfbskqfiahn96pmi2d0r9x34d682amq3bc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-approx" ,rust-approx-0.4)
         ("rust-blas-src" ,rust-blas-src-0.6)
         ("rust-cblas-sys" ,rust-cblas-sys-0.1)
         ("rust-matrixmultiply" ,rust-matrixmultiply-0.2)
         ("rust-num-complex" ,rust-num-complex-0.3)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-rawpointer" ,rust-rawpointer-0.2)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/rust-ndarray/ndarray")
    (synopsis
      "An n-dimensional array for general elements and for numerics. Lightweight array views and slicing; views support chunking and splitting.")
    (description
      "An n-dimensional array for general elements and for numerics.  Lightweight array
views and slicing; views support chunking and splitting.")
    (license (list license:expat license:asl2.0))))

(define-public rust-smawk-0.3
  (package
    (name "rust-smawk")
    (version "0.3.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "smawk" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0hv0q1mw1r1brk7v3g4a80j162p7g1dri4bdidykrakzfqjd4ypn"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build? #t #:cargo-inputs (("rust-ndarray" ,rust-ndarray-0.14))))
    (home-page "https://github.com/mgeisler/smawk")
    (synopsis "Functions for finding row-minima in a totally monotone matrix.")
    (description
      "This package provides functions for finding row-minima in a totally monotone
matrix.")
    (license license:expat)))

(define-public rust-hyphenation-commons-0.8
  (package
    (name "rust-hyphenation-commons")
    (version "0.8.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyphenation_commons" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1gq59h9h8597k04yl53an0j56cvb0in98pxpp27dkiz5mnifgssz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-fst" ,rust-fst-0.4) ("rust-serde" ,rust-serde-1))))
    (home-page "https://github.com/tapeinosyne/hyphenation")
    (synopsis "Proemial code for the `hyphenation` library")
    (description "Proemial code for the `hyphenation` library")
    (license (list license:asl2.0 license:expat))))

(define-public rust-fst-0.4
  (package
    (name "rust-fst")
    (version "0.4.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "fst" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "06mnksicgv9rp8b7w0ykkshf355l05zym3ygm74qr5z30ndmpf3s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-utf8-ranges" ,rust-utf8-ranges-1))))
    (home-page "https://github.com/BurntSushi/fst")
    (synopsis
      "Use finite state transducers to compactly represents sets or maps of many
strings (> 1 billion is possible).
")
    (description
      "Use finite state transducers to compactly represents sets or maps of many
strings (> 1 billion is possible).")
    (license (list license:unlicense license:expat))))

(define-public rust-hyphenation-0.8
  (package
    (name "rust-hyphenation")
    (version "0.8.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "hyphenation" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "1w2hib167vpz7jbg3zs92ifihj4akirlhb5509aib1df8i6dvx5w"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bincode" ,rust-bincode-1)
         ("rust-bincode" ,rust-bincode-1)
         ("rust-fst" ,rust-fst-0.4)
         ("rust-fst" ,rust-fst-0.4)
         ("rust-hyphenation-commons" ,rust-hyphenation-commons-0.8)
         ("rust-hyphenation-commons" ,rust-hyphenation-commons-0.8)
         ("rust-pocket-resources" ,rust-pocket-resources-0.3)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-unicode-normalization" ,rust-unicode-normalization-0.1))))
    (home-page "https://github.com/tapeinosyne/hyphenation")
    (synopsis "Knuth-Liang hyphenation for a variety of languages")
    (description "Knuth-Liang hyphenation for a variety of languages")
    (license (list license:asl2.0 license:expat))))

(define-public rust-textwrap-0.14
  (package
    (name "rust-textwrap")
    (version "0.14.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "textwrap" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "106xjfzfpk3nj51fx9slf9kyir7xjwvpqm003v9ardgq5b8whrh0"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-hyphenation" ,rust-hyphenation-0.8)
         ("rust-smawk" ,rust-smawk-0.3)
         ("rust-terminal-size" ,rust-terminal-size-0.1)
         ("rust-unicode-linebreak" ,rust-unicode-linebreak-0.1)
         ("rust-unicode-width" ,rust-unicode-width-0.1))))
    (home-page "https://github.com/mgeisler/textwrap")
    (synopsis
      "Powerful library for word wrapping, indenting, and dedenting strings")
    (description
      "Powerful library for word wrapping, indenting, and dedenting strings")
    (license license:expat)))

(define-public rust-selinux-sys-0.5
  (package
    (name "rust-selinux-sys")
    (version "0.5.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "selinux-sys" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "19dighj7nf8gzy9bgh206znbh1p9gp56wrcwwklv9yiq2zwp9lqj"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bindgen" ,rust-bindgen-0.59)
         ("rust-cc" ,rust-cc-1)
         ("rust-dunce" ,rust-dunce-1)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/koutheir/selinux-sys")
    (synopsis "Flexible Mandatory Access Control (MAC) for Linux")
    (description "Flexible Mandatory Access Control (MAC) for Linux")
    (license license:expat)))

(define-public rust-reference-counted-singleton-0.1
  (package
    (name "rust-reference-counted-singleton")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "reference-counted-singleton" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "16is1ld71ara6wmxhsm80h3xbzr0dlai34cxqr5xrp9gm49m4i7g"))))
    (build-system cargo-build-system)
    (arguments `(#:skip-build? #t))
    (home-page "https://github.com/koutheir/reference-counted-singleton")
    (synopsis
      "Reference-counted singleton whose protected data can be recreated as needed")
    (description
      "Reference-counted singleton whose protected data can be recreated as needed")
    (license license:expat)))

(define-public rust-selinux-0.2
  (package
    (name "rust-selinux")
    (version "0.2.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "selinux" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0j0m5ll9qg9xf68pp5aid01r7b371bjdrr31gq21dsan8dmmsw89"))))
    (build-system cargo-build-system)
    (arguments
      `(#:skip-build?
        #t
        #:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-once-cell" ,rust-once-cell-1)
         ("rust-reference-counted-singleton"
          ,rust-reference-counted-singleton-0.1)
         ("rust-selinux-sys" ,rust-selinux-sys-0.5)
         ("rust-thiserror" ,rust-thiserror-1))))
    (home-page "https://github.com/koutheir/selinux")
    (synopsis "Flexible Mandatory Access Control for Linux")
    (description "Flexible Mandatory Access Control for Linux")
    (license license:expat)))

(define-public rust-coreutils-0.0.12
  (package
    (name "rust-coreutils")
    (version "0.0.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "coreutils" version))
        (file-name (string-append name "-" version ".tar.gz"))
        (sha256
          (base32 "0zjlbchnm9jlrh4grxhwhd3vdh3qy20l2yi41avz3avgl5vbpykl"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-clap" ,rust-clap-2)
         ("rust-lazy-static" ,rust-lazy-static-1)
         ("rust-selinux" ,rust-selinux-0.2)
         ("rust-textwrap" ,rust-textwrap-0.14)
         ("rust-uu-arch" ,rust-uu-arch-0.0.12)
         ("rust-uu-base32" ,rust-uu-base32-0.0.12)
         ("rust-uu-base64" ,rust-uu-base64-0.0.12)
         ("rust-uu-basename" ,rust-uu-basename-0.0.12)
         ("rust-uu-basenc" ,rust-uu-basenc-0.0.12)
         ("rust-uu-cat" ,rust-uu-cat-0.0.12)
         ("rust-uu-chcon" ,rust-uu-chcon-0.0.12)
         ("rust-uu-chgrp" ,rust-uu-chgrp-0.0.12)
         ("rust-uu-chmod" ,rust-uu-chmod-0.0.12)
         ("rust-uu-chown" ,rust-uu-chown-0.0.12)
         ("rust-uu-chroot" ,rust-uu-chroot-0.0.12)
         ("rust-uu-cksum" ,rust-uu-cksum-0.0.12)
         ("rust-uu-comm" ,rust-uu-comm-0.0.12)
         ("rust-uu-cp" ,rust-uu-cp-0.0.12)
         ("rust-uu-csplit" ,rust-uu-csplit-0.0.12)
         ("rust-uu-cut" ,rust-uu-cut-0.0.12)
         ("rust-uu-date" ,rust-uu-date-0.0.12)
         ("rust-uu-dd" ,rust-uu-dd-0.0.12)
         ("rust-uu-df" ,rust-uu-df-0.0.12)
         ("rust-uu-dircolors" ,rust-uu-dircolors-0.0.12)
         ("rust-uu-dirname" ,rust-uu-dirname-0.0.12)
         ("rust-uu-du" ,rust-uu-du-0.0.12)
         ("rust-uu-echo" ,rust-uu-echo-0.0.12)
         ("rust-uu-env" ,rust-uu-env-0.0.12)
         ("rust-uu-expand" ,rust-uu-expand-0.0.12)
         ("rust-uu-expr" ,rust-uu-expr-0.0.12)
         ("rust-uu-factor" ,rust-uu-factor-0.0.12)
         ("rust-uu-false" ,rust-uu-false-0.0.12)
         ("rust-uu-fmt" ,rust-uu-fmt-0.0.12)
         ("rust-uu-fold" ,rust-uu-fold-0.0.12)
         ("rust-uu-groups" ,rust-uu-groups-0.0.12)
         ("rust-uu-hashsum" ,rust-uu-hashsum-0.0.12)
         ("rust-uu-head" ,rust-uu-head-0.0.12)
         ("rust-uu-hostid" ,rust-uu-hostid-0.0.12)
         ("rust-uu-hostname" ,rust-uu-hostname-0.0.12)
         ("rust-uu-id" ,rust-uu-id-0.0.12)
         ("rust-uu-install" ,rust-uu-install-0.0.12)
         ("rust-uu-join" ,rust-uu-join-0.0.12)
         ("rust-uu-kill" ,rust-uu-kill-0.0.12)
         ("rust-uu-link" ,rust-uu-link-0.0.12)
         ("rust-uu-ln" ,rust-uu-ln-0.0.12)
         ("rust-uu-logname" ,rust-uu-logname-0.0.12)
         ("rust-uu-ls" ,rust-uu-ls-0.0.12)
         ("rust-uu-mkdir" ,rust-uu-mkdir-0.0.12)
         ("rust-uu-mkfifo" ,rust-uu-mkfifo-0.0.12)
         ("rust-uu-mknod" ,rust-uu-mknod-0.0.12)
         ("rust-uu-mktemp" ,rust-uu-mktemp-0.0.12)
         ("rust-uu-more" ,rust-uu-more-0.0.12)
         ("rust-uu-mv" ,rust-uu-mv-0.0.12)
         ("rust-uu-nice" ,rust-uu-nice-0.0.12)
         ("rust-uu-nl" ,rust-uu-nl-0.0.12)
         ("rust-uu-nohup" ,rust-uu-nohup-0.0.12)
         ("rust-uu-nproc" ,rust-uu-nproc-0.0.12)
         ("rust-uu-numfmt" ,rust-uu-numfmt-0.0.12)
         ("rust-uu-od" ,rust-uu-od-0.0.12)
         ("rust-uu-paste" ,rust-uu-paste-0.0.12)
         ("rust-uu-pathchk" ,rust-uu-pathchk-0.0.12)
         ("rust-uu-pinky" ,rust-uu-pinky-0.0.12)
         ("rust-uu-pr" ,rust-uu-pr-0.0.12)
         ("rust-uu-printenv" ,rust-uu-printenv-0.0.12)
         ("rust-uu-printf" ,rust-uu-printf-0.0.12)
         ("rust-uu-ptx" ,rust-uu-ptx-0.0.12)
         ("rust-uu-pwd" ,rust-uu-pwd-0.0.12)
         ("rust-uu-readlink" ,rust-uu-readlink-0.0.12)
         ("rust-uu-realpath" ,rust-uu-realpath-0.0.12)
         ("rust-uu-relpath" ,rust-uu-relpath-0.0.12)
         ("rust-uu-rm" ,rust-uu-rm-0.0.12)
         ("rust-uu-rmdir" ,rust-uu-rmdir-0.0.12)
         ("rust-uu-runcon" ,rust-uu-runcon-0.0.12)
         ("rust-uu-seq" ,rust-uu-seq-0.0.12)
         ("rust-uu-shred" ,rust-uu-shred-0.0.12)
         ("rust-uu-shuf" ,rust-uu-shuf-0.0.12)
         ("rust-uu-sleep" ,rust-uu-sleep-0.0.12)
         ("rust-uu-sort" ,rust-uu-sort-0.0.12)
         ("rust-uu-split" ,rust-uu-split-0.0.12)
         ("rust-uu-stat" ,rust-uu-stat-0.0.12)
         ("rust-uu-stdbuf" ,rust-uu-stdbuf-0.0.12)
         ("rust-uu-sum" ,rust-uu-sum-0.0.12)
         ("rust-uu-sync" ,rust-uu-sync-0.0.12)
         ("rust-uu-tac" ,rust-uu-tac-0.0.12)
         ("rust-uu-tail" ,rust-uu-tail-0.0.12)
         ("rust-uu-tee" ,rust-uu-tee-0.0.12)
         ("rust-uu-test" ,rust-uu-test-0.0.12)
         ("rust-uu-timeout" ,rust-uu-timeout-0.0.12)
         ("rust-uu-touch" ,rust-uu-touch-0.0.12)
         ("rust-uu-tr" ,rust-uu-tr-0.0.12)
         ("rust-uu-true" ,rust-uu-true-0.0.12)
         ("rust-uu-truncate" ,rust-uu-truncate-0.0.12)
         ("rust-uu-tsort" ,rust-uu-tsort-0.0.12)
         ("rust-uu-tty" ,rust-uu-tty-0.0.12)
         ("rust-uu-uname" ,rust-uu-uname-0.0.12)
         ("rust-uu-unexpand" ,rust-uu-unexpand-0.0.12)
         ("rust-uu-uniq" ,rust-uu-uniq-0.0.12)
         ("rust-uu-unlink" ,rust-uu-unlink-0.0.12)
         ("rust-uu-uptime" ,rust-uu-uptime-0.0.12)
         ("rust-uu-users" ,rust-uu-users-0.0.12)
         ("rust-uu-wc" ,rust-uu-wc-0.0.12)
         ("rust-uu-who" ,rust-uu-who-0.0.12)
         ("rust-uu-whoami" ,rust-uu-whoami-0.0.12)
         ("rust-uu-yes" ,rust-uu-yes-0.0.12)
         ("rust-uucore" ,rust-uucore-0.0.12)
;         ("rust-sieve", rust-sieve-0.1)
)
        #:tests? #f
        #:cargo-development-inputs
        (("rust-atty" ,rust-atty-0.2)
         ("rust-chrono" ,rust-chrono-0.4)
         ("rust-conv" ,rust-conv-0.3)
         ("rust-filetime" ,rust-filetime-0.2)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-nix" ,rust-nix-0.23)
         ("rust-pretty-assertions" ,rust-pretty-assertions-1)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-regex" ,rust-regex-1)
         ("rust-rlimit" ,rust-rlimit-0.4)
         ("rust-sha1" ,rust-sha1-0.6)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-time" ,rust-time-0.1)
         ("rust-unindent" ,rust-unindent-0.1)
         ("rust-unix-socket" ,rust-unix-socket-0.5)
         ("rust-users" ,rust-users-0.10)
         ("rust-uucore" ,rust-uucore-0.0.12)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "https://github.com/uutils/coreutils")
    (synopsis
      "coreutils ~ GNU coreutils (updated); implemented as universal (cross-platform) utils, written in Rust")
    (description
      "coreutils ~ GNU coreutils (updated); implemented as universal (cross-platform)
utils, written in Rust")
    (license license:expat)))

rust-coreutils-0.0.12