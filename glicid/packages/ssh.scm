(define-module (glicid packages ssh)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages ssh)
  #:use-module (gnu packages hurd)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages linux)
)
(define-public libcbor-0.8.0
  (package
    (name "libcbor")
    (version "0.8.0")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append "http://github.com/PJK/libcbor/archive/refs/tags/v" version ".tar.gz"))
        (sha256 (base32 "12vw46ahhsc3ydnivkv5lvk1cn4sjjmcr639cjcl99d4dqb9g031"))
      )
    )
    (build-system cmake-build-system)
    (arguments
      `(#:configure-flags (list
         "-DCMAKE_CXX_FLAGS=-fPIE"
         "-DCMAKE_C_FLAGS=-fPIE"
       ))
    )
    (synopsis "libfido2")
    (description "Todo")
    (home-page "toto")
    (license license:gpl2+)
  )
)

(define-public libcbor-0.9.0
  (package
    (inherit libcbor-0.8.0)
    (version "0.9.0")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append "http://github.com/PJK/libcbor/archive/refs/tags/v" version ".tar.gz"))
        (sha256 (base32 "1l8m7h84zwlx088gkz6gvg2fq11g0p3q6lblwba8c01y6gwy90fs"))
      )
    )
  )
)

(define-public libcbor-latest libcbor-0.9.0)

(define-public libfido2-1.8.0
  (package
    (name "libfido2")
    (version "1.8.0")
    (source 
      (origin
        (method url-fetch)
        (uri (string-append "https://developers.yubico.com/libfido2/Releases/libfido2-" version ".tar.gz"))
        (sha256 (base32 "07gxyy5yzgfh5hg7q9fr77z5mkj0xjvd5ya7p5f5kar4iwc92hjm"))
      )
    )
    (build-system cmake-build-system)
    (arguments '(
      #:tests? #f
    ))
    (inputs `(
      ("libcbor",libcbor-latest)
      ("openssl",openssl)
      ("zlib", zlib)
      ("pkg-config", pkg-config)
      ("eudev", eudev)
    ))
    (synopsis "libfido2")
    (description "Todo")
    (home-page "toto")
    (license license:gpl2+)
  )
)

(define-public libfido2-1.9.0
  (package
    (inherit libfido2-1.8.0)
    (version "1.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (string-append "https://developers.yubico.com/libfido2/Releases/libfido2-" version ".tar.gz"))
        (sha256 (base32 "1ln2b0zfvl35r03kn2k1i2aw1k3ypvivc71xmp4dzlin6ypy6fds"))
      )
    )
  )
)

(define libfido2-latest libfido2-1.9.0) 

(define-public openssh-with-fido2
  (package
    (inherit openssh)
    (name "openssh-with-fido2")
    (arguments `(
      #:test-target "tests"
      ;; Otherwise, the test scripts try to use a nonexistent directory and
      ;; fail.
      #:make-flags '("REGRESSTMP=\"$${BUILDDIR}/regress\"")
      #:configure-flags `(
        "--sysconfdir=/etc/ssh"
        ;; Default value of 'PATH' used by sshd.
        "--with-default-path=/run/current-system/profile/bin"
        ;; configure needs to find krb5-config.
        ,(string-append "--with-kerberos5=" (assoc-ref %build-inputs "mit-krb5") "/bin")
        ;; libedit is needed for sftp completion.
        "--with-libedit"
        ;; for u2f
        "--with-security-key-builtin"
        ;; Enable PAM support in sshd.
        ,,@(if (hurd-target?)
          '()
          '("--with-pam")
        )
        ;; "make install" runs "install -s" by default,
        ;; which doesn't work for cross-compiled binaries
        ;; because it invokes 'strip' instead of
        ;; 'TRIPLET-strip'.Work around this.
        ,,@(if (%current-target-system)
          '("--disable-strip")
          '()
        )
      )
      #:phases
        (modify-phases %standard-phases
          (add-after 'configure 'reset-/var/empty
            (lambda* (#:key outputs #:allow-other-keys)
              (let ((out (assoc-ref outputs "out")))
                (substitute*
                  "Makefile"
                  (("PRIVSEP_PATH=/var/empty") (string-append "PRIVSEP_PATH=" out "/var/empty"))
                )
                #t
              )
            )
          )
          (add-before 'check 'patch-tests
            (lambda _
              (substitute* "regress/test-exec.sh" (("/bin/sh") (which "sh")))
              ;; Remove 't-exec' regress target which requires user 'sshd'.
              (substitute*
                (list "Makefile" "regress/Makefile")
                (("^(tests:.*) t-exec(.*)" all pre post) (string-append pre post))
              )
              #t
            )
          )
          (replace 'install
            (lambda*
              (
                #:key outputs (make-flags '())
                #:allow-other-keys
              )
              ;; Install without host keys and system configuration files.
              (apply invoke "make" "install-nosysconf" make-flags)
              (install-file "contrib/ssh-copy-id" (string-append (assoc-ref outputs "out") "/bin/"))
              (chmod (string-append (assoc-ref outputs "out") "/bin/ssh-copy-id") #o555)
              (install-file "contrib/ssh-copy-id.1" (string-append (assoc-ref outputs "out") "/share/man/man1/"))
              #t
            )
          )
        )
    ))
    (inputs `(
      ("libfido2", libfido2-latest)
      ("libcbor", libcbor-latest)
      ,@(package-inputs openssh)
    ))
  )
)
