(define-module (glicid packages sssd)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module ((gnu packages sssd) #:prefix gnu:)  
)

(define-public sssd-upstream-orig
  (package
    (inherit gnu:sssd)
    (name "sssd-upstream")
    (version "1_16_5")
    (source
     (origin
        (inherit (package-source gnu:sssd))
        (method git-fetch)
        (uri (git-reference
          (url "https://github.com/SSSD/sssd")
          (commit (string-append "sssd-" version))
        ))
        (file-name (git-file-name name version))
        (sha256 (base32 "0zbs04lkjbp7y92anmafl7gzamcnq1f147p13hc4byyvjk9rg6f7"))
      )
    )
 )
)
