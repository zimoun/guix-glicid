(define-module (glicid packages virtualization)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages virtualization)
  #:use-module (gnu packages storage)
;  #:use-module (glicid packages storage)
)

(define-public qemu-with-rbd
  (package
    (inherit qemu) 
    (name "qemu-with-rbd")
    (arguments
      (substitute-keyword-arguments `(
        #:tests? #f
        ,@(package-arguments qemu)
      ))
    ) 
    (inputs `(
      ("ceph:lib", ceph "lib")
      ,@(package-inputs qemu)
    ))
  )
)

(define-public qemu-minimal-with-rbd
  (package
    (inherit qemu-minimal) 
    (name "qemu-minimal-with-rbd")
    (arguments
      (substitute-keyword-arguments `(
        #:tests? #f
        ,@(package-arguments qemu-minimal)
      ))
    ) 
    (inputs `(
      ("ceph:lib", ceph "lib" )
      ,@(package-inputs qemu-minimal)
    ))
  )
)
