(define-module (glicid services file-systems)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (gnu packages file-systems)
  #:export (
    %default-autofs-conf
    autofs-configuration
    autofs-configuration?
    autofs-service
    autofs-service-type
  )
)

(define %default-autofs-conf
  (plain-file "autofs" "
  # Empty file as we do nothing by default
  ")
)

(define-record-type* <autofs-configuration>
  autofs-configuration make-autofs-configuration
  autofs-configuration?
  (autofs autofs-configuration-autofs
    (default autofs)
  )
  (config-file autofs-config-file
    (default %default-autofs-conf)
  )
  (log-file autofs-log-file
    (default "/var/log/autofs.log")
  )
)

(define autofs-service
  (match-lambda
    (($ <autofs-configuration> autofs config-file log-file)
      (list
        (shepherd-service
          (provision '(autofs) )
          (documentation "Run autofs.")
          (requirement '(user-processes))
          (respawn? #t)
          (start #~(make-forkexec-constructor
            (list
              #$(file-append autofs "/sbin/automount")
              "-d" "-f"
            )
            #:log-file #$log-file
          ))
          (stop #~(make-kill-destructor))
        )
      )
    )
  )
)

(define autofs-service-type
  (service-type (name 'autofs)
    (extensions 
      (list (
        service-extension
        shepherd-root-service-type
        autofs-service
      ))
    )
    (description "Run autofs")
  )
)

