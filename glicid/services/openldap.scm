(define-module (glicid services openldap)
  #:use-module (gnu packages openldap)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #: export (
    openldap-configuration
    openldap-configuration?
    openldap-shepherd-service
    openldap-service-type
  )
)

(define-record-type* <openldap-configuration>
  openldap-configuration make-openldap-configuration
  openldap-configuration?
  (openldap openldap-configuration-openldap
    (default openldap)
  )
  (uri openldap-configuration-uri
    (default "ldapi:// ldap://")
  )
  (logflags openldap-configuration-logflags
    (default "0")
  )
  (pid-file openldap-configuration-pid-file
    (default "/var/run/openldap/slapd.pid")
  )
  (config-file openldap-configuration-config-file
    (default (file-append openldap "/etc/openldap/slapd.conf"))
  )
  (log-file openldap-configuration-log-file
    (default "/var/log/slapd.log")
  )
)

(define openldap-shepherd-service
  (match-lambda
    (($ <openldap-configuration> openldap uri logflags pid-file config-file log-file)
      (list
        (shepherd-service
          (provision '(slapd) )
          (documentation "Run openldap.")
          (requirement '(user-processes))
          (respawn? #t)
          (start #~(make-forkexec-constructor
            (list
              #$(file-append openldap "/libexec/slapd")
              "-h" #$uri
              "-d" #$logflags
              "-f" #$config-file
            )
            #:pid-file #$pid-file 
            #:log-file #$log-file
          ))
          (stop #~(make-kill-destructor))
        )
      )
    )
  )
)


(define %openldap-activation
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))
        (mkdir-p "/var/run/openldap")
        (mkdir-p "/var/lib/ldap")
        #t
    )
  )
)

(define openldap-service-type
  (service-type (name 'slapd)
    (extensions 
      (list
        (service-extension shepherd-root-service-type openldap-shepherd-service)
        (service-extension activation-service-type (const %openldap-activation))
      )
    )
    (description "Run @uref{https://www.openldap.org, Openldap} community developped LDAP software.")
  )
)
