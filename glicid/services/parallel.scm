(define-module (glicid services parallel)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (gnu packages parallel)
  #:export (
    munged-configuration
    munged-configuration?
    munged-service
    munged-service-type
    slurmdbd-configuration
    slurmdbd-configuration?
    slurmdbd-service
    slurmdbd-service-type
    slurmctld-configuration
    slurmctld-configuration?
    slurmctld-service
    slurmctld-service-type
  )
)

;
; Munged
;
(define-record-type* <munged-configuration>
  munged-configuration make-munged-configuration
  munged-configuration?
  (munge munge-configuration-munge
    (default munge)
  )
  (key-file munged-key-file
    (default (file-append munge "/etc/munge/munged.key"))
  )
  (log-file munged-log-file
    (default "/var/log/munged.log")
  )
)

(define munged-service
  (match-lambda
    (($ <munged-configuration> munge key-file log-file)
      (list
        (shepherd-service
          (provision '(munged) )
          (documentation "Run munged.")
          (requirement '(user-processes))
          (respawn? #t)
          (start #~(make-forkexec-constructor
            (list
              #$(file-append munge "/sbin/munged")
              "-F"
              "-f" ; until we fix the permissions
              "--key-file" #$key-file
            )
            #:log-file #$log-file
          ))
          (stop #~(make-kill-destructor))
        )
      )
    )
  )
)

(define %munged-activation
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))
        (mkdir-p "/var/run/munge")
        (mkdir-p "/var/lib/munge")
        #t
    )
  )
)

(define munged-service-type
  (service-type (name 'munged)
    (extensions 
      (list
        (service-extension shepherd-root-service-type munged-service)
        (service-extension activation-service-type (const %munged-activation))
      )
    )
    (description "Run munged")
  )
)

;
; slurmdbd
;
(define-record-type* <slurmdbd-configuration>
  slurmdbd-configuration make-slurmdbd-configuration
  slurmdbd-configuration?
  (slurm slurm-configuration-slurm
    (default slurm)
  )
  (slurmdbd-conf slurmdbd-slurmdbd-conf
    (default (file-append slurm "/etc/slurm/slurmdbd.conf"))
  )
  (log-file slurmdbd-log-file
    (default "/var/log/slurmdbd.log")
  )
)

(define slurmdbd-service
  (match-lambda
    (($ <slurmdbd-configuration> slurm slurmdbd-conf log-file)
      (list
        (shepherd-service
          (provision '(slurmdbd) )
          (documentation "Run slurmdbd.")
          (requirement '(user-processes))
          (respawn? #t)
          (start #~(make-forkexec-constructor
            (list
              #$(file-append slurm "/sbin/slurmdbd")
              "-D"
            )
            #:log-file #$log-file
          ))
          (stop #~(make-kill-destructor))
        )
      )
    )
  )
)

(define %slurmdbd-activation
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))
        (mkdir-p "/var/log/slurm")
        (mkdir-p "/var/lib/slurm-archives")
        (mkdir-p "/var/lib/slurm")
        (chmod "0600" "/etc/slurm/slurmdbd.conf")
        #t
    )
  )
)

(define slurmdbd-service-type
  (service-type (name 'slurmdbd)
    (extensions 
      (list
        (service-extension shepherd-root-service-type slurmdbd-service)
        (service-extension activation-service-type (const %slurmdbd-activation))
      )
    )
    (description "Run slurmdbd")
  )
)

;
; slurmctld
;
(define-record-type* <slurmctld-configuration>
  slurmctld-configuration make-slurmctld-configuration
  slurmctld-configuration?
  (slurm slurm-configuration-slurm
    (default slurm)
  )
  (slurmctld-conf slurmctld-slurmctld-conf
    (default (file-append slurm "/etc/slurm/slurmctld.conf"))
  )
  (log-file slurmctld-log-file
    (default "/var/log/slurmctld.log")
  )
)

(define slurmctld-service
  (match-lambda
    (($ <slurmctld-configuration> slurm slurmctld-conf log-file)
      (list
        (shepherd-service
          (provision '(slurmctld) )
          (documentation "Run slurmctld.")
          (requirement '(user-processes))
          (respawn? #t)
          (start #~(make-forkexec-constructor
            (list
              #$(file-append slurm "/sbin/slurmctld")
              "-D" "-R"
              "-f" #$slurmctld-conf
            )
            #:log-file #$log-file
          ))
          (stop #~(make-kill-destructor))
        )
      )
    )
  )
)

(define %slurmctld-activation
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))
        (mkdir-p "/var/log/slurm")
        (mkdir-p "/var/lib/slurm-archives")
        (mkdir-p "/var/lib/slurm")
        #t
    )
  )
)

(define slurmctld-service-type
  (service-type (name 'slurmctld)
    (extensions 
      (list
        (service-extension shepherd-root-service-type slurmctld-service)
        (service-extension activation-service-type (const %slurmctld-activation))
      )
    )
    (description "Run slurmctld")
  )
)

