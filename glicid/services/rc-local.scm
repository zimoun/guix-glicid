(define-module (glicid services rc-local)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (guix)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (gnu packages bash)
  #:export (
    %default-rc-local-conf
    rc-local-configuration
    rc-local-configuration?
    rc-local-service
    rc-local-service-type
  )
)

(define %default-rc-local-conf
  (plain-file "rc-local" "
  # Empty file as we do nothing by default
  ")
)

(define-record-type* <rc-local-configuration>
  rc-local-configuration make-rc-local-configuration
  rc-local-configuration?
  (config-file rc-local-config-file
    (default %default-rc-local-conf)
  )
  (log-file rc-local-log-file
    (default "/var/log/rc-local.log")
  )
)

(define rc-local-service
  (match-lambda
    (($ <rc-local-configuration> config-file log-file)
      (list
        (shepherd-service
          (provision '(rc-local) )
          (documentation "Run rc-local.")
          (requirement '(user-processes))
          (respawn? #f)
          (one-shot? #t)
          (start #~(make-forkexec-constructor
            (list
              #$(file-append bash "/bin/bash")
              "-l"
              #$config-file
            )
            #:log-file #$log-file
          ))
          (stop #~(make-kill-destructor))
        )
      )
    )
  )
)

(define rc-local-service-type
  (service-type (name 'rc-local)
    (extensions 
      (list (
        service-extension
        shepherd-root-service-type
        rc-local-service
      ))
    )
    (description "Run a script in a rc-local like form")
  )
)

