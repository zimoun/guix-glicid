(define-module (glicid utils)
  #:use-module (guix packages)
  #:use-module (guix transformations)
  #:use-module (guix utils)
  #:use-module (gnu packages gcc)
  #:use-module (glicid packages gcc)
  #:use-module (gnu packages commencement)
  #:use-module (gnu packages)
  #:export (latest-version)
  #:export (gcc11-instead-of-gcc)
  #:export (transform-package)
  #:export (instead-of)
)
;(define (latest-version v1 v2) (if (string> (package-version v1) (package-version v2)) v1 v2))

(define (latest-version v1 v2)

 (case (version-compare (package-version v1) (package-version v2))
                            ((>) v1)
                            ((=) v1)
                            ((<) v2)
 )
)

(define gcc11-instead-of-gcc
  (package-input-rewriting `(
    (,gcc-toolchain . ,gcc-toolchain-11)
    (,gfortran-toolchain . ,gfortran-toolchain-11)
  ))
)

(define (transform-package original-package suffix)
  (package
    (inherit original-package)
    (name (string-append (package-name original-package) "-" suffix ))
  )
)

(define (instead-of package-a-spec package-b) 
  (package-input-rewriting/spec `(
    (,package-a-spec . ,(const package-b)) 
  ))
)
